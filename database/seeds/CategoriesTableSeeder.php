<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Chicken',
                'type' => 'Chicken',
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-09',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Lamb',
                'type' => 'Lamb',
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-09',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Beef',
                'type' => 'Beef',
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-09',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Mutton',
                'type' => 'All cameras',
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-09',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Marinated',
                'type' => 'All powerbanks',
                'created_at' => '2021-02-09',
                'updated_at' => '2021-02-09',
            ),
            
        ));
        
        
    }
}