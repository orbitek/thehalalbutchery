<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// SELECT categories.id, categories.name AS Category_name,products.id AS Product_id, products.name AS product_name, variations.id AS Variation_id, variations.variation, variations.price
// FROM ((categories
// INNER JOIN products ON products.category_id = categories.id)
// INNER JOIN variations ON variations.product_id = products.id)
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
