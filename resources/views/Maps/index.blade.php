<!DOCTYPE html>
<html>
  <head>
    <title>Add Map</title>

    <!-- [START maps_add_map_resources_style] -->
    <link rel="stylesheet" type="text/css" href="./style.css" />
    <!-- [END maps_add_map_resources_style] -->
    <script src="./index.js"></script>
    <style type="text/css">
      #map {
  height: 400px;
  /* The height is 400 pixels */
  width: 100%;
  /* The width is the width of the web page */
}
    </style>
  </head>
  <body>
    <!-- [START maps_add_map_heading] -->
    <h3>My Google Maps Demo</h3>
    <!-- [END maps_add_map_heading] -->
    <!-- [START maps_add_map_div] -->
    <!--The div element for the map -->
    <div id="map"></div>
    <!-- [END maps_add_map_div] -->

    <!-- Async script executes immediately and must be after any DOM elements used in callback. -->
    <script
      src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap&libraries=&v=weekly"
      async
    ></script>
    <script type="text/javascript">
      function initMap(): void {
  // [START maps_add_map_instantiate_map]
  // The location of Uluru
  const uluru = { lat: -25.344, lng: 131.036 };
  // The map, centered at Uluru
  const map = new google.maps.Map(
    document.getElementById("map") as HTMLElement,
    {
      zoom: 4,
      center: uluru,
    }
  );
  // [END maps_add_map_instantiate_map]

  // [START maps_add_map_instantiate_marker]
  // The marker, positioned at Uluru
  const marker = new google.maps.Marker({
    position: uluru,
    map: map,
  });
  // [END maps_add_map_instantiate_marker]
}
// [END maps_add_map]
export { initMap };
    </script>
    <script type="text/javascript">
      function initMap() {
  // [START maps_add_map_instantiate_map]
  // The location of Uluru
  const uluru = { lat: -25.344, lng: 131.036 };
  // The map, centered at Uluru
  const map = new google.maps.Map(document.getElementById("map"), {
    zoom: 4,
    center: uluru,
  });
  // [END maps_add_map_instantiate_map]
  // [START maps_add_map_instantiate_marker]
  // The marker, positioned at Uluru
  const marker = new google.maps.Marker({
    position: uluru,
    map: map,
  });
  // [END maps_add_map_instantiate_marker]
}
    </script>
  </body>
</html>