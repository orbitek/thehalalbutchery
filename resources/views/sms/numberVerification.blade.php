@extends('store.storeLayout')
@section('content')
<script src="{{asset('public/js/lib/jquery.js')}}"></script>
<script src="{{asset('public/js/dist/jquery.validate.js')}}"></script>

<style>
label.error {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
  padding:1px 20px 1px 20px;
}
#loginForm{
        margin-left: auto;
    margin-right: auto;
}

</style>
    <!-- SECTION -->
<div class="section steps">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row" style="display: flex;flex-wrap: wrap; background:#ffffffe0;; border-radius:150px 0px 150px 0px;">
       <div class="col-md-6" style="background: white;padding-left: 0px;padding-right: 0px;background: black;border-radius: 150px 0px 0px 0px;"> 
            <img src="{{asset('public/images/slides/main.png')}}" alt="Premium BBQ Box" style="width: 100%; border-radius: 150px 0px 0px 0px; ">
         </div>

        <div class="col-md-6">

         <form method="post" id="varifyNumberForm" action="{{route('sendVarificationCode')}}" >
            {{csrf_field()}}
            <div class="" >
                <!-- Billing Details -->
                <div class="billing-details">
                    <div class="section-title">
                        <h3 class="title" style="font-size: 33px;color: #2b2d42;">Varify Number</h3>
                    </div>
                    <div class="form-group">
                        <input class="input" type="tel" maxlength="12" minlength="11" name="tel" id="tel" placeholder="Mobile">
                        <span style="float: left;margin: 15px;color: #7b7b7b;font-size: 12px;">E.g. "44711122233"</span>
                    </div>
                    
                    <br>
                        <input type="submit"  name="signin" class="primary-btn order-submit" style="width: 75%;margin-bottom:22px ;" value="Varify">

                </form>
                
                </div>
                    
                </div>
                <!-- /Billing Details -->
            </div>

        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>

<!-- /SECTION -->
@endsection
