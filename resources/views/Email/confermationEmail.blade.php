<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>THB</title>

	<style>
	.invoice-box {
		max-width: 800px;
		margin: auto;
		padding: 30px;
		border: 1px solid #eee;
		box-shadow: 0 0 10px rgba(0, 0, 0, 0.64);
		font-size: 16px;
		line-height: 24px;
		color: #000;
	}

	.invoice-box table {
		width: 100%;
		line-height: inherit;
		text-align: left;
	}

	.invoice-box table td {
		padding: 5px;
		vertical-align: top;
	}
	.invoice-box table tr th {
		padding: 5px;
		vertical-align: top;
	}

	.invoice-box table tr td:nth-child(2) {
		text-align: right;
	}

	.invoice-box table tr.top table td {
		padding-bottom: 20px;
	}

	.invoice-box table tr.top table td.title {
		font-size: 45px;
		line-height: 45px;
		color: #333;
	}

	.invoice-box table tr.information table td {
		padding-bottom: 40px;
	}

	.invoice-box table tr.heading td {
		background: #eee;
		border-bottom: 1px solid #ddd;
		font-weight: bold;
	}

	.invoice-box table tr.details td {
		padding-bottom: 20px;
	}

	.invoice-box table tr.item td {
		border-bottom: 1px solid #757575;
	}

	.invoice-box table tr.item.last td {
		border-bottom: none;
	}

	.invoice-box table tr.total td:nth-child(2) {
		border-top: 2px solid #eee;
		font-weight: bold;
	}

	@media only screen and (max-width: 600px) {
		.invoice-box table tr.top table td {
			width: 100%;
			display: block;
			text-align: center;
		}

		.invoice-box table tr.information table td {
			width: 100%;
			display: block;
			text-align: center;
		}
	}

	/** RTL **/
	.rtl {
		direction: rtl;
		font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
	}

	.rtl table {
		text-align: right;
	}

	.rtl table tr td:nth-child(2) {
		text-align: center;
	}
	td{
		text-align: center;
		color: black;
		font-family: "Times New Roman", Times, serif;
	}
</style>
</head>

<body>
	<div class="invoice-box">
		<table cellpadding="0" cellspacing="0" style="border-spacing: 0px;">
			<tr class="top">
				<td colspan="4">
					<table>
						<tr>
							<td class="title">
								<img src="{{asset('public/img/thb_black_logo.jpg')}}" style="width: 100%; max-width: 200px;  " />
							</td>
						</tr>
						<tr><td >
							<h1>Thank you for your order,<br><h2> {{$invoice->user->full_name}}!</h2></h1>
						</td></tr>
						<tr><td>Thank you for placing your order with The Halal Butchery, we will contact you as soon as your order is on its way!
						</td></tr>
						<tr><td><h1>Order Summary</h1></td></tr>

						<tr>
							<table style="border-spacing: 0px;">
								<tr>
									<th style="border: 1px solid black;text-align: left; font-size: 1vu;">DELIVERY ADDRESS</th>
									
								</tr>
								<tr>
									<td style="border: 1px solid black;text-align: left;">
									
										{{$invoice->user->full_name}}<br />
										{{$invoice->user->phone}}<br />
										{{$invoice->user->email}}<br />

									{{$invoice->Place}}<br />
									{{$invoice->city}}, {{$invoice->postcode}}
									
									</td>
									
								</tr>
								<br>
								<tr>
									<th style="border: 1px solid black;text-align: right;">SUMMARY</th>
								</tr>
								<tr>
									<td style="border: 1px solid black;text-align: right;">Order #: {{$invoice->id}}<br />
									Order Date: {{$invoice->created_at}}<br />
									Order Total:£ {{ number_format((float)($invoice->total_price), 2, '.', '') }}<br>
									Payment Method:  {{ ($invoice->payment_method == 2)? "Cash On Delivery":"Card Payment" }}<br>
									@if($invoice->shipping_time != "-" )
									<?php $dt = explode("T", $invoice->shipping_time); ?>
									Expected Time: {{$dt[0]}} / {{ $dt[1]}}
									@endif
									</td>
								</tr>
							</table>
							<table style="border-spacing:0px; margin-top: 5px;">
								<tr><td style="border: 1px solid black;text-align: left;">
									<strong>Note:</strong><span> {{$invoice->notes}} </span>
								</td></tr>
							</table>
						</tr>
					</table>
				</td>
			</tr>

			<tr>
			<table style="border-spacing: 0px;    color: black;">
			<tr class="heading"  style="border-bottom: 1px solid black;background: #e0e0e0; ">
				<th>Item</th>
				<th>Size</th>
				<th>Qty</th>
				<th style="text-align: center;">Price</th>

			</tr>
			@foreach($invoice->invoice_detail as $prod)

			<tr class="item" style="border-bottom: 1px solid black">
				<td style="text-align: left;">{{$prod->product->name}} <span style="font-size:10px;" >{{ $prod->product->tag }}</span>

					@if($prod->invoice_question)
					@foreach($prod->invoice_question as $q)
					<p><strong>{{$q->question}}</strong></p>
					<p>{{$q->option}}</p>

					@endforeach
					@endif

				</td>
				<td style="text-align: left;">{{$prod->size}}</td>
				<td style="text-align: left">{{$prod->qty}}</td>

				<td style="text-align: center;"><span style="text-decoration: line-through;font-size: 12px;display: none" >£ {{ round((($prod->total_price)+($prod->total_price)*(20/100)),2) }}</span>   £{{  number_format((float)($prod->total_price), 2, '.', '') }}</td>

			</tr>

			@endforeach
			<tr><td  style="text-align: left"><strong>Sub-Total</strong></td>
				<td></td> <td></td>
				<td><strong><span style="text-decoration: line-through;font-size: 12px;display: none" >£ {{ round((($invoice->price)+($invoice->price)*(20/100)),2) }}</span> £{{ number_format((float)($invoice->price),2,'.','')}}</strong></td>
			</tr>
			<tr style="border-bottom: 1px solid black"><td  style="text-align: left"><strong>Delivery ({{$invoice->shipping->type}})</strong></td>
				<td></td> <td></td>
				<td><strong>£{{$invoice->shipping->price}}</strong></td>
			</tr>
			<tr>
				<td style="text-align: left"><strong>Promo Discount</strong></td> <td></td><td></td><td><strong>£{{number_format((float)($invoice->discount),2,'.','') }}</strong></td>
			</tr>
			<tr style="display: none">
				<td style="text-align: left"><strong>20% Offer Discount</strong></td> <td></td><td></td><td><strong>£{{round((($invoice->total_price)*(20/100)),2) }}</strong></td>
			</tr>

			<tr>
				<td style="text-align: left"><strong>Order Total</strong></td> <td></td><td></td><td><strong>£{{ number_format((float)($invoice->total_price),2,'.','') }}</strong></td>
			</tr>
		</table>
		<table style="background: black; color: white;BORDER-RADIUS: 50PX;">
			<tr>
				<td> <p style="color: white;">We are social! Join Us.</p>
				</td>
			</tr>
				<tr>
					<td>
						<a href="instagram.com/thehalalbutcheryuk/"> <img src="{{asset('public/img/insta.png')}}" style="width: 100%; max-width: 50px;  " /></a>
						<a href="https://www.facebook.com/search/top?q=thehalalbutchery">	<img src="{{asset('public/img/fb.png')}}" style="width: 100%; max-width: 50px;  " /></a>
				
						<a href="https://www.tiktok.com/@thehalalbutchery"><img src="{{asset('public/img/tt.png')}}" style="width: 100%; max-width: 50px;  " /></a>
						<a href="https://twitter.com/halalbutcheryuk"><img src="{{asset('public/img/tw.png')}}" style="width: 100%; max-width: 50px;  " /></a>
					</td>
				</tr>
				<tr>
					<td><a href="https://thehalalbutchery.com/"> <p style="color: white;">The Halal Butchery</p></a>
				</td>
			</tr>
		</table>
	</tr>
		</table>
	</div>
</body>
</html>
