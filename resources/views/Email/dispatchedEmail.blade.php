<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>THB</title>

	<style>
	.invoice-box {
		max-width: 800px;
		margin: auto;
		padding: 30px;
		border: 1px solid #eee;
		box-shadow: 0 0 10px rgba(0, 0, 0, 0.64);
		font-size: 16px;
		line-height: 24px;
		color: #000;
	}

	.invoice-box table {
		width: 100%;
		line-height: inherit;
		text-align: left;
	}

	.invoice-box table td {
		padding: 5px;
		vertical-align: top;
	}
	.invoice-box table tr th {
		padding: 5px;
		vertical-align: top;
	}

	.invoice-box table tr td:nth-child(2) {
		text-align: right;
	}

	.invoice-box table tr.top table td {
		padding-bottom: 20px;
	}

	.invoice-box table tr.top table td.title {
		font-size: 45px;
		line-height: 45px;
		color: #333;
	}

	.invoice-box table tr.information table td {
		padding-bottom: 40px;
	}

	.invoice-box table tr.heading td {
		background: #eee;
		border-bottom: 1px solid #ddd;
		font-weight: bold;
	}

	.invoice-box table tr.details td {
		padding-bottom: 20px;
	}

	.invoice-box table tr.item td {
		border-bottom: 1px solid #757575;
	}

	.invoice-box table tr.item.last td {
		border-bottom: none;
	}

	.invoice-box table tr.total td:nth-child(2) {
		border-top: 2px solid #eee;
		font-weight: bold;
	}

	@media only screen and (max-width: 600px) {
		.invoice-box table tr.top table td {
			width: 100%;
			display: block;
			text-align: center;
		}

		.invoice-box table tr.information table td {
			width: 100%;
			display: block;
			text-align: center;
		}
	}

	/** RTL **/
	.rtl {
		direction: rtl;
		font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
	}

	.rtl table {
		text-align: right;
	}

	.rtl table tr td:nth-child(2) {
		text-align: center;
	}
	td{
		text-align: center;
		color: black;
		font-family: "Times New Roman", Times, serif;
	}
</style>
</head>

<body>
	<div class="invoice-box">
		<table cellpadding="0" cellspacing="0" style="border-spacing: 0px;">
			<tr class="top">
				<td colspan="4">
					<table>
						<tr>
							<td class="title">
								<img src="{{asset('public/img/thb_black_logo.jpg')}}" style="width: 100%; max-width: 200px;  " />
							</td>
						</tr>
						<tr><td >
							<h1>Congragulations, Your order has been dispatched!<br></h1>
						</td></tr>
						<tr><td>Congratulations, your order has been dispatched! Have your apron ready and an eye out for your THB driver who will be arriving with your goods!
						</td></tr>

						
					</table>
				</td>
			</tr>

			<tr>
			
		<table><tr><td>We are social!Join Us.
				</td></tr>
				<tr><td><a href="instagram.com/thehalalbutcheryuk/"> <img src="{{asset('public/img/insta.png')}}" style="width: 100%; max-width: 50px;  " /></a>
				<a href="https://www.facebook.com/search/top?q=thehalalbutchery">	<img src="{{asset('public/img/fb.png')}}" style="width: 100%; max-width: 50px;  " /></a>
				
				<a href="https://www.tiktok.com/@thehalalbutchery"><img src="{{asset('public/img/tt.png')}}" style="width: 100%; max-width: 50px;  " /></a>
				<a href="https://twitter.com/halalbutcheryuk"><img src="{{asset('public/img/tw.png')}}" style="width: 100%; max-width: 50px;  " /></a></td></tr>
</table>
	</tr>
		</table>
	</div>
</body>
</html>
