@extends('admin_panel.adminLayout') @section('content')
<script src="{{asset('js/lib/jquery.js')}}"></script>
<script src="{{asset('js/dist/jquery.validate.js')}}"></script>
<style>label.error {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
  padding:1px 20px 1px 20px;
}</style>
<div class="content-wrapper">
          <div class="row">
            <div class="col-md-12 d-flex align-items-stretch grid-margin">
              <div class="row flex-grow">
                <div class="col-12">
                  <div class="card">
                    <div class="card-body">
                    <a href="{{route('Postcode.PostcodeManagement')}}"> < Back to List</a>
                    <br><br>
                      <h4 class="card-title">Edit Postcode</h4>
                      <br>
                      <form class="forms-sample" method="post"  id="postcode_form" action="{{route('Postcode.postcodeUpdate',$postcode->id)}}">
                      {{csrf_field()}}
                        <div class="form-group">
                          <label for="exampleInputEmail1">Postcode</label>
                          <input type="text" class="form-control" id="postcode" name="postcode" value="{{$postcode->postcode}}">
                        </div>

                        <div class="form-group">
                          <label for="exampleInputPassword1">Area</label>
                          <input type="text" class="form-control" id="area" name="area" value=" {{$postcode->area}}">
                        </div>
                        
                         <div class="form-group">
                            <label for="exampleInputPassword2" >City</label>
                                <input type="text" class="form-control" name="city" id="city" value="Manchester" placeholder="Enter City" readonly>
                        </div>

                           <div class="form-group">
                            <label for="exampleInputPassword2" >Assign To</label>
                                <select type="text" class="form-control" name="butcher" id="butcher" value="Manchester" >
                                  <option>Select Butcher</option>
                                  @foreach($butchers as $butcher)
                                  <option value="{{$butcher->id}}">{{$butcher->name}}</option>
                                  @endforeach
                          </div>
                          
                          <br>
                                             
                        <input  type="submit" name="updateButton"  class="btn btn-success mr-2" id="updateButton" value="UPDATE" />
                      
                      </form>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
    
    
    <!--JQUERY Validation-->
<script>
	
	$(document).ready(function() {
		// validate the comment form when it is submitted
		//$("#commentForm").validate();

		// validate signup form on keyup and submit
		$("#cat_form").validate({
			rules: {
				Name: "required",
				Type: "required",
				
				
				
			},
			messages: {
				Name: "Category Name is Required",
				Type: "Category Type is Required",
                	
			}
		});

		
	});
	</script>
<!--/JQUERY Validation-->
@endsection