@extends('admin_panel.adminLayout') @section('content')
<script src="{{asset('js/lib/jquery.js')}}"></script>
<script src="{{asset('js/dist/jquery.validate.js')}}"></script>
<style>label.error {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
  padding:1px 20px 1px 20px;
}
.card {
    margin-top: 10px;
}
</style>



<div class="content-wrapper">
    <div class="row">
        <div class="col-12 stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Add Promo</h4>
                    <form class="forms-sample" method="post" id="question_form" action="{{route('promo.store')}}">
                        {{csrf_field()}}

                        <div class="form-group row">
                            
                            <div class="col-md-3">
                                <input type="text" class="form-control" name="title" id="title" placeholder="Enter Title">
                            </div>
                            <div class="col-md-3">
                                <select name="type" class="form-control" id="type">
                                    @foreach($types as $type)
                                    <option value="{{$type->id}}">{{$type->type}}</option>
                                    @endforeach
                                </select>
                            </div>
                             <div class="col-md-3">
                                <input type="text" class="form-control" name="value" id="value" placeholder="Enter value">
                            </div>
                             <div class="col-md-3">
                                <input type="text" class="form-control" name="min_price" id="min_price" placeholder="Enter Min Price">
                            </div>
                            
                        </div>

                        <div class="form-group row">
                            
                        <div class="col-md-3">
                                <input type="text" class="form-control" name="limit" id="limit" placeholder="Enter Limit">
                            </div>
                             <div class="col-md-3">
                                <input type="date" class="form-control" name="start_date" id="start_date" placeholder="Enter Start Date">
                            </div>
                             <div class="col-md-3">
                                <input type="date" class="form-control" name="end_date" id="end_date" placeholder="Enter End Date">
                            </div>
                        </div>

                        <div class="form-group row">
                        <div class="col-md-12">
                                <input type="text" class="form-control" name="description" id="description" placeholder="Enter Description">
                            </div>
                        </div>

                       
                        <button type="submit" class="btn btn-success mr-2" style="float: right;">Submit</button>
                    </form>
                    @if($errors->any())
                    <ul>
                        @foreach($errors->all() as $err)
                        <tr>
                            <td>
                                <li>{{$err}}</li>
                            </td>
                        </tr>
                        @endforeach
                    </ul>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Promos Table</h4>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>

                       
                                <tr>
                                    <th>
                                      Status  
                                    </th>
                                    <th >
                                        Promo
                                    </th>
                                    <th >
                                        Price
                                    </th>
                                    
                                    <th >
                                        Min Price
                                    </th>
                                    <th >
                                        Limit
                                    </th>
                                    <th >
                                        Description
                                    </th>
                                    <th>
                                        Start Date
                                    </th>
                                    <th>
                                        End Date
                                    </th>
                                    <th>Actions</th>
                                    
                                   
                                </tr>
                            </thead>
                            <tbody>
                               @foreach($promos as $promo)
                                <tr>
                                    <td>
                                       {{$promo->status}}
                                    </td>
                                    
                                    <td>
                                       {{$promo->title}}
                                    </td>
                                    <td>
                                       {{$promo->value}}
                                    </td>
                                    <td>
                                       {{$promo->min_price}}
                                    </td>
                                    <td>
                                       {{$promo->limit}}
                                    </td>
                                    <td>
                                       {{$promo->description}}
                                    </td>
                                    <td>
                                       {{$promo->start_date}}
                                    </td>
                                    <td>
                                       {{$promo->end_date}}
                                    </td>
                                    
                                    <td>
                                        <a class="btn btn-warning" data-toggle="modal"  data-variation="" data-price="" data-id="" >Edit</a>
                                   &nbsp
                                        <a href="" class="btn btn-danger">Delete</a>
                                    </td>
                                </tr>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Button trigger modal -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Variation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form class="forms-sample" method="post" action="">
            {{csrf_field()}}

            <input type="hidden" name="id" id="id">
          <div class="form-group">
            <label for="recipient-name"  class="col-form-label">Variation</label>
            <input type="text" class="form-control" name="variationE" id="variationE">
          </div>
          <div class="form-group">
            <label for="message-text"  class="col-form-label">Price</label>
            <input class="form-control" name="priceE" id="priceE"></input>
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection
@section('content_js')
    <script type="text/javascript">
        $( ".btn-warning" ).click(function() {
            var button = $(event.relatedTarget);
            var id =$(this).data('id');
            var price =$(this).data('price');
            var variation =$(this).data('variation');

            $('#id').val(id);
            $('#priceE').val(price);
            $('#variationE').val(variation);
           
            $('#exampleModal').modal('toggle');
        });
    </script>
@endsection