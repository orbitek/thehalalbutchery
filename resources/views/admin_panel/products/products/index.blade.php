@extends('admin_panel.adminLayout') @section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Products Table <a class="btn btn-lg btn-success" style="float:right;color:white" href="{{route('admin.products.create')}}">+ Add Product</a></h4>
                    <br><br>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>
                                        Images
                                    </th>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Category
                                    </th>
                                    <th>
                                        Active
                                    </th>
                                    <th>
                                        Variartions
                                    </th>
                                    <th>
                                        Delete
                                    </th>
                                    <th>
                                        Price
                                    </th>
                                    <th>
                                        Description
                                    </th>
                                    <th>
                                        Category
                                    </th>
                                    
                                    <th>
                                        Update
                                    </th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($prdlist as $prd)
                                <tr>
                                    <td>
                                        <img src="../public/uploads/products/{{$prd->id}}/{{$prd->image_name}}" style="width:100px;height:100px;border-radius:10%;" alt="">
                                    </td>
                                    <td>
                                       <a href="{{route('admin.products.edit', ['id' => $prd->id])}}" style="width:250px; overflow: hidden;" class="btn btn-warning">{{$prd->name}}</a>
                                    </td>
                                    <td>
                                       <strong> {{$prd->category->name}}</strong>
                                    </td>
                                    <td>
                                         <a href="https://thehalalbutchery.com/admin_panel/products/inStock/{{$prd->id}}/{{$prd->active}}" class="btn {{ ($prd->active==1)?'btn-success':'btn-danger' }}" value="{{$prd->id}}" style="width: 40px; border-radius: 40px;  height: 40px;" onclick="stock({{ $prd->id }},{{$prd->active}})">{{ ($prd->active==1)?'I':'O' }}</a>
                                    </td>

                                    <td>
                                         <a href="https://thehalalbutchery.com/admin_panel/products/variation/{{$prd->id}}" class="btn btn-primary"  style="width: 40px; border-radius: 40px;  height: 40px;" >V</a>
                                    </td>
                                    <td><a href="{{route('admin.products.delete', ['id' => $prd->id])}}"class="btn btn-danger">Delete</a></td>
                                    <td>
                                        {{$prd->price}}
                                    </td>
                                    <td>
                                        {{$prd->description}}
                                    </td>
                                    <td>
                                        {{$prd->category->name}}
                                    </td>
                                    
                                    <td><a href="{{route('admin.products.edit', ['id' => $prd->id])}}" class="btn btn-warning">Edit</a> </td>
                                     
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('content_js')
<script type="text/javascript">
function stock(id, val)
{
    $.post("route('admin.inStock')",
    {
      id: id,
      val: val
    },
    function(data,status){
      alert("Data: " + data + "\nStatus: " + status);
    });
}
    </script>

@endsection

