<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="mobile-web-app-capable" content="yes">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="{{asset('public/js/lib/jquery.js')}}"></script>
    <script src="{{asset('public/js/dist/jquery.validate.js')}}"></script>
    <title>Admin Login</title>
</head>
<style>
    @import url(https://fonts.googleapis.com/css?family=Roboto:300);

.login-page {
  width: 40%;
  padding: 8% 0 0;
  margin: auto;
}

.form input {
 height: 40px;
    padding: 0px 15px;
    border-radius: 40px;
    border: 1px solid #E4E7ED;
    background-color: gray;
    color: white;
}
.form button {
  font-family: "Roboto", sans-serif;
  text-transform: uppercase;
  outline: 0;
  background: #4CAF50;
  width: 100%;
  border: 0;
  padding: 15px;
  color: #FFFFFF;
  font-size: 14px;
  -webkit-transition: all 0.3 ease;
  transition: all 0.3 ease;
  cursor: pointer;
}
.form button:hover,.form button:active,.form button:focus {
  background: #43A047;
}
.form .message {
  margin: 15px 0 0;
  color: #b3b3b3;
  font-size: 12px;
}
.form .message a {
  color: #4CAF50;
  text-decoration: none;
}
.form .register-form {
  display: none;
}
.container {
  position: relative;
  z-index: 1;
  max-width: 50%;
  margin: 0 auto;
}
.container:before, .container:after {
  content: "";
  display: block;
  clear: both;
}
.container .info {
  margin: 50px auto;
  text-align: center;
}
.container .info h1 {
  margin: 0 0 15px;
  padding: 0;
  font-size: 36px;
  font-weight: 300;
  color: #1a1a1a;
}
.container .info span {
  color: #4d4d4d;
  font-size: 12px;
}
.container .info span a {
  color: #000000;
  text-decoration: none;
}
.container .info span .fa {
  color: #EF3B3A;
}
body {
  background: black; /* fallback for old browsers */
  
  font-family: "Roboto", sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;      
}
    
label.error {
  color: #a94442;
  background-color: #f2dede;
  
}
</style>
<body>

    <div class="login-page">
      <div class="container">
        <img src="{{asset('public/img/THB-Logo.png')}}" alt="" style="margin-left: 20px; max-width: 170px;">
        <div class="form" >
            <form class="login-form" id="loginForm" method="post">
            {{csrf_field()}}
                <input type="text" name="Username" id="Username" placeholder="Username" /><br><br>
                <input type="password" name="Password" id="Password" placeholder="Password"  /><br><br>
                <input type="submit" name="loginButton" id="loginButton" value="LOGIN" style="background-color: #ad9802; margin-left: 50px" />
            </form>
        </div>
      </div>
    </div>
</body>
</html>


<!--JQUERY Validation-->
<script>
	
	$(document).ready(function() {
		// validate the comment form when it is submitted
		//$("#commentForm").validate();

		// validate signup form on keyup and submit
		$("#loginForm").validate({
			rules: {
				Username: "required",
                Password: "required"

				
				
			},
			messages: {
				Username: "No Input Entered",
				Password: "No Input Entered",
                
				
				
			}
		});

		
	});
	</script>
<!--/JQUERY Validation-->
