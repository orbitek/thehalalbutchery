@extends('admin_panel.adminLayout') @section('content')
<script src="{{asset('public/js/lib/jquery.js')}}"></script>
<script src="{{asset('public/js/dist/jquery.validate.js')}}"></script>
<script src="{{asset('public/js/dist/additional-methods.js')}}"></script>

<style>label.error {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
  padding:1px 20px 1px 20px;
}
.form-control {
  border: groove;
}
</style>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12 d-flex align-items-stretch grid-margin">
            <div class="row flex-grow">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <a style="color:green;" href="{{route('admin.adminManagement')}}">
                                < Back to List</a>
                                    <br>
                                    <br>
                                    <h4 >Create Admin</h4>
                                    <br>
                            <form id="forms-sample" method="POST" action="{{route('admin.adminStore')}}">
                                @csrf

                                <div class="form-group row">
                                    <label for="username" class="col-md-4 col-form-label text-md-right">User Name</label>

                                    <div class="col-md-8">
                                        <input id="username" type="text" class="form-control" name="username" placeholder="User Name" required autocomplete="username" autofocus>
                                      
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                                    <div class="col-md-8">
                                        <input id="name" type="text" class="form-control" name="name" placeholder="Name" required autocomplete="name" autofocus>
                                      
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="address" class="col-md-4 col-form-label text-md-right">Address</label>

                                    <div class="col-md-8">
                                        <input id="address" type="text" class="form-control" name="address" placeholder="Address" required autocomplete="address" autofocus>
                                      
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="city" class="col-md-4 col-form-label text-md-right">City</label>

                                    <div class="col-md-8">
                                        <input id="city" type="text" class="form-control" name="city" placeholder="City" required autocomplete="city" autofocus>
                                      
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="postcode" class="col-md-4 col-form-label text-md-right">Postcode</label>

                                    <div class="col-md-8">
                                        <input id="postcode" type="text" class="form-control" name="postcode" placeholder="Post Code" required autocomplete="postcode" autofocus>
                                       
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="phone" class="col-md-4 col-form-label text-md-right">Phone/Mobile</label>

                                    <div class="col-md-8">
                                        <input id="phone" type="text" class="form-control" name="phone" placeholder="Phone" required autocomplete="phone" autofocus>
                                       
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label for="approvallevels" class="col-md-4 col-form-label text-md-right">{{('Roles')}} </label>

                                    <div class="col-md-8">
                                         <select class="form-control" style="margin-top:5px" name="role[]" id="role" style="margin-top:5px">
                                       <option value="" Selected disabled>{{('Select')}} {{('Role')}} </option>
                                            @if($data['role'])
                                                @foreach($data['role'] as $role)
                                                <option value="{{$role['id']}}">{{(app()->getLocale() == 'en'?$role['name']:$role['namej'])}} </option>
                                                
                                                @endforeach
                                            @endif
                                    </select>
                                    </div>
                                </div>

                                 <div class="form-group row" style="display: none">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{('User Id')}}</label>

                                    <div class="col-md-8">
                                        <input id="user_id" type="text" class="form-control" name="id" value="-1" style="" autocomplete="off">

                                       
                                    </div>
                                </div>
                              
                               
                              
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{('Close')}}</button>
                    <div class="form-group row mb-0">
                            <div class="col-md-10">
                                <button type="submit" id="savebtn" class="btn btn-primary">
                                    {{('Save')}}
                                </button>
                            </div>
                        </div>
                    </div>
                                
                    </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
    
<script>
 
  function fileChange(e) {
 
     document.getElementById('inp_img').value = '';
 
     for (var i = 0; i < e.target.files.length; i++) { 
     
        var file = e.target.files[i];
 
        if (file.type == "image/jpeg" || file.type == "image/png") {
 
           var reader = new FileReader();  
           reader.onload = function(readerEvent) {
 
              var image = new Image();
              image.onload = function(imageEvent) { 
 
                 var max_size = 600;
                 var w = image.width;
                 var h = image.height;
                   
                 if (w > h) {  if (w > max_size) { h*=max_size/w; w=max_size; }
                 } else     {  if (h > max_size) { w*=max_size/h; h=max_size; } }
               
                 var canvas = document.createElement('canvas');
                 canvas.width = w;
                 canvas.height = h;
                 canvas.getContext('2d').drawImage(image, 0, 0, w, h);
                 if (file.type == "image/jpeg") {
                    var dataURL = canvas.toDataURL("image/jpeg", 1.0);
                 } else {
                    var dataURL = canvas.toDataURL("image/png");    
                 }
                 document.getElementById('inp_img').value += dataURL + '|';
              }
              image.src = readerEvent.target.result;
           }
           reader.readAsDataURL(file);
           
            readURL(this);

        } else {
           document.getElementById('inp_files').value = ''; 
           alert('Please only select images in JPG or PNG format.');   
           return false;
        }
     }
 
  }
 
  document.getElementById('inp_files').addEventListener('change', fileChange, false); 
         
</script>    
    
<script>
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imageHolder').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    
    
    
  
function onReadyColorList(arrayOfColor){
    var addedColor = document.querySelector("#color_list").value;
    var arrayOfColor = addedColor.split(',');
    for(var i =0 ; i< arrayOfColor.length; i++){
        newColor = `<div style="height:25px;display:inline-block;margin:5px;width:25px!important;background-color:${arrayOfColor[i]}"></div>`;
        document.querySelector("#colors").innerHTML += newColor;
    }
}

function addColor(){
    var pickedColor = document.querySelector("#picker").value;
    newColor = `<div style="height:25px;display:inline-block;margin:5px;width:25px!important;background-color:${pickedColor}"></div>`;
    var addedColor = document.querySelector("#color_list").value;
    var arrayOfColor = [];
    if (addedColor != ""){  
        arrayOfColor = addedColor.split(',');
        if(!arrayOfColor.includes(pickedColor)){
            arrayOfColor.push(pickedColor);
            document.querySelector("#color_list").value = arrayOfColor.join(',');
            document.querySelector("#colors").innerHTML += newColor;
        }
    }
    else{
        arrayOfColor.push(pickedColor);
        document.querySelector("#colors").innerHTML += newColor;
        document.querySelector("#color_list").value = pickedColor;
    }   
       // console.log(addedColor);          
}


</script>
    
    
<!--JQUERY Validation-->
<script>
	
	$(document).ready(function() {
		
        
        
		$("#product_form").validate({
            
			rules: {
              
                Name: "required",
                inp_files: "required",
                
                Description: "required",
                Category: "required",
                Price: {
					required: true,
					number: true
				},
                Discounted_Price: {
					required: true,
					number: true
				},
                colors: "required",
                Tags: "required"
                
                
				
				
				
				
			},
			messages: {
				
				Name: "No Name is Entered",
                inp_files:  "ERRRERRR",
                Description: "No Description is Entered",
                Category: "No Category is Selected",
                
				Price: {
					required: "No Price is Entered",
					number: "Invalid Price"
				},
                Discounted_Price: {
					required: "No Price is Entered",
					number: "Invalid Price"
				},
                colors: "No Color is Selected",
                Tags: "No Tags is Selected",
				
				
			}
            
            
            
		});
        
                

		
	});
    
  
    
    
	</script>
<!--/JQUERY Validation-->    
@endsection
