@extends('admin_panel.adminLayout') @section('content')
<style>label.error {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
  padding:1px 20px 1px 20px;
}
.form-control {
  border: groove;
}</style>
<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12 d-flex align-items-stretch grid-margin">
            <div class="row flex-grow">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <a href="{{route('admin.adminManagement')}}">
                                < Back to List</a>
                                    <br>
                                      <br>
                                  
                                    <h4 >Edit</h4>
                                      <br>
                                     <form id="forms-sample" method="POST" action="{{route('admin.adminUpdate',$data['user']->id)}}">
                                @csrf

                                <div class="form-group row">
                                    <label for="username" class="col-md-4 col-form-label text-md-right">User Name</label>

                                    <div class="col-md-8">
                                        <input id="username" type="text" class="form-control" value="{{$data['user']->username}}" name="username" placeholder="User Name" required autocomplete="username" autofocus>
                                      
                                    </div>
                                </div>

                                @if(session()->get('admin')->id == $data['user']->id )
                                <div class="form-group row">
                                    <label for="Password" class="col-md-4 col-form-label text-md-right">Password</label>

                                    <div class="col-md-8">
                                        <input id="password" type="password" class="form-control" value="{{$data['user']->password}}" name="password" placeholder="User Name" required autocomplete="password" autofocus>
                                      
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="Password" class="col-md-4 col-form-label text-md-right"></label>

                                    <div class="col-md-8">
                                        <input type="checkbox" onclick="showPassword()">&nbsp&nbsp&nbsp<label style="margin-top: 3px">Show Password</label>
                                      
                                    </div>
                                </div>
                                

                                <div class="form-group row">
                                    <label for="confirm_password" class="col-md-4 col-form-label text-md-right">Confirm Password</label>

                                    <div class="col-md-8">
                                        <input id="confirm_password" type="password" class="form-control" value="{{$data['user']->password}}" name="confirm_password" placeholder="Confirm Password" required autocomplete="confirm_password" autofocus>
                                      <span style="font-size: 12px" id='message'></span>
                                    </div>
                                </div>
                                @endif
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">Name</label>

                                    <div class="col-md-8">
                                        <input id="name" type="text" class="form-control"  value="{{$data['user']->name}}" name="name" placeholder="Name" required autocomplete="name" autofocus>
                                      
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="address" class="col-md-4 col-form-label text-md-right">Address</label>

                                    <div class="col-md-8">
                                        <input id="address" type="text" class="form-control"  value="{{$data['user']->address}}" name="address" placeholder="Address" required autocomplete="address" autofocus>
                                      
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="city" class="col-md-4 col-form-label text-md-right">City</label>

                                    <div class="col-md-8">
                                        <input id="city" type="text" class="form-control"  value="{{$data['user']->city}}" name="city" placeholder="City" required autocomplete="city" autofocus>
                                      
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="postcode" class="col-md-4 col-form-label text-md-right">Postcode</label>

                                    <div class="col-md-8">
                                        <input id="postcode" type="text" class="form-control"  value="{{$data['user']->postcode}}" name="postcode" placeholder="Post Code" required autocomplete="postcode" autofocus>
                                       
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="phone" class="col-md-4 col-form-label text-md-right">Phone/Mobile</label>

                                    <div class="col-md-8">
                                        <input id="phone" type="text" class="form-control"  value="{{$data['user']->phone}}" name="phone" placeholder="Phone" required autocomplete="phone" autofocus>
                                       
                                    </div>
                                </div>
                                @hasRole(['Admin'])
                                <div class="form-group row">
                                    <label for="approvallevels" class="col-md-4 col-form-label text-md-right">{{('Roles')}} </label>

                                    <div class="col-md-8">
                                         <select class="form-control" style="margin-top:5px" name="role[]" id="role" style="margin-top:5px">
                                     
                                            @if($data['role'])
                                                @foreach($data['role'] as $role)
                                                <option value="{{$role['id']}}" {{( $data['user_role']==$role['id']? 'selected':'')}}>{{$role['name']}} </option>
                                                
                                                @endforeach
                                            @endif
                                    </select>
                                    </div>
                                </div>
                                @endhasRole

                                 <div class="form-group row" style="display: none">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{('User Id')}}</label>

                                    <div class="col-md-8">
                                        <input id="user_id" type="text" class="form-control" name="id" value="-1" style="" autocomplete="off">

                                       
                                    </div>
                                </div>
                              
                               
                              
                    <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{('Close')}}</button>
                    <div class="form-group row mb-0">
                            <div class="col-md-10">
                                <button type="submit" id="savebtn" class="btn btn-primary">
                                    {{('Save')}}
                                </button>
                            </div>
                        </div>
                    </div>
                                
                    </form>
                                   
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
    
<script>
 
  function fileChange(e) {
 
     document.getElementById('inp_img').value = '';
 
     for (var i = 0; i < e.target.files.length; i++) { 
     
        var file = e.target.files[i];
 
        if (file.type == "image/jpeg" || file.type == "image/png") {
 
           var reader = new FileReader();  
           reader.onload = function(readerEvent) {
 
              var image = new Image();
              image.onload = function(imageEvent) { 
 
                 var max_size = 600;
                 var w = image.width;
                 var h = image.height;
                   
                 if (w > h) {  if (w > max_size) { h*=max_size/w; w=max_size; }
                 } else     {  if (h > max_size) { w*=max_size/h; h=max_size; } }
               
                 var canvas = document.createElement('canvas');
                 canvas.width = w;
                 canvas.height = h;
                 canvas.getContext('2d').drawImage(image, 0, 0, w, h);
                 if (file.type == "image/jpeg") {
                    var dataURL = canvas.toDataURL("image/jpeg", 1.0);
                 } else {
                    var dataURL = canvas.toDataURL("image/png");    
                 }
                 document.getElementById('inp_img').value += dataURL + '|';
              }
              image.src = readerEvent.target.result;
           }
           reader.readAsDataURL(file);
           
            readURL(this);

        } else {
           document.getElementById('inp_files').value = ''; 
           alert('Please only select images in JPG or PNG format.');   
           return false;
        }
     }
 
  }
 
  document.getElementById('inp_files').addEventListener('change', fileChange, false); 
         
</script>     
<script>
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#imageHolder').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
   
$( document ).ready(function() {
    var addedColor = document.querySelector("#color_list").value;
    var arrayOfColor = addedColor.split(',');
    
    
    
    //console.log(addedColor);
    onReadyColorList(arrayOfColor);       
});
function onReadyColorList(arrayOfColor){
    var addedColor = document.querySelector("#color_list").value;
    var arrayOfColor = addedColor.split(',');
    for(var i =0 ; i< arrayOfColor.length; i++){
        newColor = `<div style="height:25px;display:inline-block;margin:5px;width:25px!important;background-color:${arrayOfColor[i]}"></div>`;
        document.querySelector("#colors").innerHTML += newColor;
    }
}
function addColor(){
    var pickedColor = document.querySelector("#picker").value;
    newColor = `<div style="height:25px;display:inline-block;margin:5px;width:25px!important;background-color:${pickedColor}"></div>`;
    var addedColor = document.querySelector("#color_list").value;
    //console.log(addedColor);
    if (addColor != null){  
        var arrayOfColor = addedColor.split(',');
        if(!arrayOfColor.includes(pickedColor)){
            arrayOfColor.push(pickedColor);
            document.querySelector("#color_list").value = arrayOfColor.join(',');
            document.querySelector("#colors").innerHTML += newColor;
        }
        
        console.log(addedColor);
       
        
    } 
}

$('#password, #confirm_password').on('keyup', function () {
  if ($('#password').val() == $('#confirm_password').val()) {
    $('#message').html('Matching').css('color', 'green');
    $('#password, #confirm_password').css('border-color','green');
    $('#savebtn').prop('disabled','false');
  } else 
    $('#message').html('Not Matching').css('color', 'red');
    $('#password, #confirm_password').css('border-color','red');
    $('#savebtn').prop('disabled','true');
});

function showPassword() {
  var x = document.getElementById("password");
  var y = document.getElementById("confirm_password");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }

  if (y.type === "password") {
    y.type = "text";
  } else {
    y.type = "password";
  }
}

</script>
    

@endsection
