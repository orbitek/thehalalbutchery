<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>The Halal Butchery</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="{{ asset('vendors/css/vendor.bundle.addons.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <!-- endinject -->
    <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <link rel="shortcut icon" href="{{ asset('img/THB-fav.png') }}" />
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <!-- Yajra Datatables -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
    <!-- Multi Select -->
    <link rel="stylesheet"
        href="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1569006288/BBBootstrap/choices.min.css?version=7.0.0">
    <link rel="stylesheet" type="text/css"
        href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://res.cloudinary.com/dxfq3iotg/raw/upload/v1569006273/BBBootstrap/choices.min.js?version=7.0.0">
    </script>
    <!-- <script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js">
    </script> -->

    <style type="text/css">
        .mt-100 {
            margin-top: 100px
        }

        .active {
            font-weight: bold;
            font-size: 16px !important;
        }

        .active a i span {
            font-size: 16px;
        }

        .menu-title,
        .profile-name,
        .designation,
        .sidebar .nav .nav-item .nav-link .menu-title {
            color: white;
        }
    </style>
    <!-- End Multi Select -->


</head>

<body>

    <div class="container-scroller">
        <!-- partial:partials/_navbar.html -->
        <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row"
            style="background: linear-gradient(120deg, #000000, #ad9802);">
            <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center"
                style="background-color: black">
                <a class="navbar-brand brand-logo" href="{{ route('admin.dashboard') }}">
                    <div style="color: #ad9802;font-weight: 900;text-decoration: underline;">BUTCHERY</div>
                </a>
                <a class="navbar-brand brand-logo-mini" href="{{ route('admin.dashboard') }}">
                    <div style="color: #ad9802;font-weight: 900;text-decoration: underline;">BUTCHERY</div>
                </a>
            </div>
            <!--   <div class="row" style="margin-right: 10px;">
                <span class="profile-text col-md-6">Hi, {{ session()->get('admin')->name }}</span>
                <a class="col-md-3 dropdown-item" href="{{ route('admin.adminEdit', session()->get('admin')->id) }}">
                                My Profile
                            </a>
                            <a class="dropdown-item col-md-3" href="{{ route('admin.logout') }}">
                                Sign Out
                            </a>
            </div> -->

            <div class="navbar-menu-wrapper d-flex align-items-center">

                <ul class="navbar-nav navbar-nav-right">
                    <li class="nav-item dropdown d-none d-xl-inline-block">
                        <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown"
                            aria-expanded="false">
                            <span class="profile-text">{{ session()->get('admin')->name }}</span>
                            <img class="img-xs rounded-circle" src="{{ asset('images/faces/face1.jpg') }}"
                                alt="Profile image">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                            <br>
                            <br>
                            <a class="dropdown-item" href="{{ route('admin.adminEdit', session()->get('admin')->id) }}">
                                My Profile
                            </a>
                            <br>
                            <br>
                            <a class="dropdown-item" href="{{ route('admin.logout') }}">
                                Sign Out
                            </a>
                        </div>


                    </li>
                </ul>
                <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button"
                    data-toggle="offcanvas">
                    <span class="mdi mdi-menu"></span>
                </button>
            </div>
        </nav>
        <!-- partial -->
        <div class="container-fluid page-body-wrapper" style="padding-left: 0px">
            <!-- partial:partials/_sidebar.html -->
            <nav class="sidebar sidebar-offcanvas" id="sidebar" style="background-color: black">
                <ul class="nav">
                    <li class="nav-item nav-profile">
                        <div class="nav-link">
                            <div class="user-wrapper">
                                <div class="profile-image">
                                    <img src="{{ asset('images/faces/face1.jpg') }}" alt="profile image">
                                </div>
                                <div class="text-wrapper">
                                    <p class="profile-name">{{ session()->get('admin')->name }}</p>
                                    <div>
                                        <small class="designation text-muted">Admin</small>
                                        <span class="status-indicator online"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="nav-item {{ Route::is('admin.dashboard') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.dashboard') }}">
                            <i class="menu-icon mdi mdi-television"></i>
                            <span class="menu-title">Dashboard</span>
                        </a>
                    </li>
                    <li class="nav-item {{ Route::is('admin.dashboard.newOrders') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.dashboard.newOrders') }}">
                            <i class="menu-icon mdi mdi-new-box"></i>
                            <span class="menu-title">New Orders</span>
                        </a>
                    </li>
                    <li class="nav-item {{ Route::is('admin.dashboard.processingOrders') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.dashboard.processingOrders') }}">
                            <i class="menu-icon mdi mdi-autorenew"></i>
                            <span class="menu-title">Processing Orders</span>
                        </a>
                    </li>
                    <li class="nav-item {{ Route::is('admin.dashboard.dispatchedOrders') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.dashboard.dispatchedOrders') }}">
                            <i class="menu-icon mdi mdi-truck-delivery"></i>
                            <span class="menu-title">Dispatched Orders</span>
                        </a>
                    </li>
                    <li class="nav-item {{ Route::is('admin.dashboard.deliverdOrders') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ route('admin.dashboard.deliverdOrders') }}">
                            <i class="menu-icon mdi mdi-check-circle"></i>
                            <span class="menu-title">Deliverd Orders</span>
                        </a>
                    </li>
                    @hasRole(['Admin'])
                        <li class="nav-item {{ Route::is('admin.products') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('admin.products') }}">
                                <i class="menu-icon mdi mdi-cart-outline"></i>
                                <span class="menu-title">Products</span>
                            </a>
                        </li>
                        <li class="nav-item {{ Route::is('admin.question') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('admin.question') }}">
                                <i class="menu-icon mdi mdi-paw"></i>
                                <span class="menu-title">Questions</span>
                            </a>
                        </li>
                    @endhasRole
                    @hasRole(['Admin'])
                        <li class="nav-item {{ Route::is('admin.categories') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('admin.categories') }}">
                                <i class="menu-icon mdi mdi-view-grid"></i>
                                <span class="menu-title">Categories</span>
                            </a>
                        </li>
                    @endhasRole

                    <li class="nav-item" hidden>
                        <a class="nav-link" href="{{ route('admin.orderManagement') }}">
                            <i class="menu-icon mdi mdi-content-paste"></i>
                            <span class="menu-title">Order Management</span>
                        </a>
                    </li>
                    @hasRole(['Admin'])
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('admin.adminManagement') }}">
                                <i class="menu-icon mdi mdi-account"></i>
                                <span class="menu-title">User Management</span>
                            </a>
                        </li>
                    @endhasRole
                    @hasRole(['Admin'])
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('Postcode.PostcodeManagement') }}">
                                <i class="menu-icon mdi mdi-map-marker-multiple"></i>
                                <span class="menu-title">PostCode Management</span>
                            </a>
                        </li>
                    @endhasRole

                    @hasRole(['Admin'])
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('promo.promoManagement') }}">
                                <i class="menu-icon mdi  mdi-parking"></i>
                                <span class="menu-title">PromoCode Management</span>
                            </a>
                        </li>
                    @endhasRole
                    @hasRole(['Admin'])
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('blog.index') }}">
                                <i class="menu-icon mdi  mdi-beats"></i>
                                <span class="menu-title">Blog Management</span>
                            </a>
                        </li>
                    @endhasRole

                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('admin.adminEdit', session()->get('admin')->id) }}">
                            <i class="menu-icon mdi mdi-account-circle"></i>
                            <span class="menu-title">Profile</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('admin.logout') }}">
                            <i class="menu-icon mdi mdi-logout"></i>
                            <span class="menu-title">Logout</span>
                        </a>
                    </li>
                </ul>
            </nav>
            <!-- partial -->
            <div class="main-panel">
                <audio id="notif" src="{{ asset('sound/notification.mp3') }}"></audio>
                @yield('content')
                <!-- content-wrapper ends -->
                <!-- partial:partials/_footer.html -->
                <footer class="footer">

                </footer>
                <!-- partial -->
            </div>
            <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>
    <!-- plugins:js -->
    <script src="{{ asset('vendors/js/vendor.bundle.base.js') }}"></script>
    <script src="{{ asset('vendors/js/vendor.bundle.addons.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="{{ asset('js/off-canvas.js') }}"></script>
    <script src="{{ asset('js/misc.js') }}"></script>

    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script> -->
    <!-- endinject -->
    <!-- Custom js for this page-->

    <script src="{{ asset('js/dashboard.js') }}"></script>
    <script src="{{ asset('js/chart.js') }}"></script>

    <!--    Jquery Validation-->
    <!--  <script src="{{ asset('js/lib/jquery.js') }}"></script> -->
    <script src="{{ asset('js/dist/jquery.validate.js') }}"></script>


    <!-- End custom js for this page-->
    <script>
        // Enable pusher logging - don't include this in production
        //Pusher.logToConsole = true;

        var pusher = new Pusher('b7ae93e2f766d6ecb986', {
            cluster: 'eu'
        });

        var channel = pusher.subscribe('THB');
        channel.bind('new_order', function(data) {
            notification_audio();

            swal({
                title: "New Order!",
                text: "You Recieved A New Order!",
                type: "success"
            }).then(function() {
                $('#order_table').DataTable().ajax.reload();
            });


        });

        function notification_audio() {
            document.getElementById('notif').play();
        }
    </script>
    @yield('content_js')
</body>

</html>
