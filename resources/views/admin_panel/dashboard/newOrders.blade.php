@extends('admin_panel.adminLayout')

@section('content')
<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Orders</h4>
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped" id="order_table">
                            <thead>
                                <tr>
                                    <th>
                                        Time
                                    </th>
                                    <th>
                                       Order No
                                    </th>
                                    <th>
                                        Name
                                    </th>
                                    <th>
                                        Postcode
                                    </th>
                                    <th>
                                        Shipping
                                    </th>
                                    <th>Expected Time</th>
                                    <th>
                                        Status
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('content_js')
<script type="text/javascript">
    orders();
    function orders(){
        var order_table = $('#order_table').DataTable({
            "order": [[ 1, "DESC" ]],
            "lengthMenu": [[ 50, -1], [ 50, "All"]],
            processing:true,
            serverside:true,
            retrieve: true,
            responsive: true,
            destroy:true,
            ajax:{
                url:"{{ route('invoice.dashboard_ajax','New')}}",
            },
            columns:[
            {   
                data: 'created_at',
                name: 'created_at'
            },

             {
                data: 'id',
                name: 'id'
             },

             {
                data: 'name',
                name: 'user.name'
             },

             {
                data: 'postcode',
                name: 'user.addresses.postcode'
             },
             {
                data: 'shipping',
                name: 'shipping.type'
             },
             {
                data: 'shipping_time',
                name: 'shipping_time'
                
             },
                          {
                data: 'status',
                name: 'status'
             },
             {
                data: 'action',
                name: 'action'
             },
             {
                data: 'paid',
                name: 'paid'
             },

            ],
            "columnDefs": [
            {
                "targets": [8],
                "visible": false,
            },
            
            ],
            'rowCallback': function(row, data, index){
            if(data.status == 'Placed' && data.paid == 1){
                //console.log(data.status); 
                $(row).css('background-color', 'green');
            }
            else if(data.status == 'Placed' && data.paid == 0){
                $(row).css('background-color', 'yellow');
            }
        }
        });
    }
</script>
@endsection