<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>THB</title>

	<style>
	.invoice-box {
		width:100%;
		margin: auto;
		padding: 30px;
		border: 1px solid #eee;
		box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
		font-size: 24px;
		line-height: 24px;
		font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
		color: #555;
		 margin: 0px;
        padding: 0px;
        font-weight: 900;
	}

@media only screen and (max-width: 1000px) {
  .invoice-box {
		width: 100%;
		margin: auto;
		padding: 30px;
		border: 1px solid #eee;
		box-shadow: 0 0 10px rgba(0, 0, 0, 0.15);
		font-size: 24px;
		line-height: 24px;
		color: #555;
		 margin: 0px;
        padding: 0px;
        font-weight: 900;
}
	.invoice-box table {
		width: 100%;
		line-height: inherit;
		text-align: left;
	}

	.invoice-box table td {
		padding: 25px;
		vertical-align: top;
	}

	.invoice-box table tr td:nth-child(2) {
		text-align: right;
	}

	.invoice-box table tr.top table td {
		padding-bottom: 20px;
	}

	.invoice-box table tr.top table td.title {
		font-size: 45px;
		line-height: 45px;
		color: #333;
	}

	.invoice-box table tr.information table td {
		padding-bottom: 40px;
	}

	.invoice-box table tr.heading td {
		background: #eee;
		border-bottom: 1px solid #ddd;
		font-weight: bold;
	}

	.invoice-box table tr.details td {
		padding-bottom: 20px;
	}

	.invoice-box table tr.item td {
		border-bottom: 1px solid #757575;
	}

	.invoice-box table tr.item.last td {
		border-bottom: none;
	}

	.invoice-box table tr.total td:nth-child(2) {
		border-top: 2px solid #eee;
		font-weight: bold;
	}

	@media only screen and (max-width: 600px) {
		.invoice-box table tr.top table td {
			width: 100%;
			display: block;
			text-align: center;
		}

		.invoice-box table tr.information table td {
			width: 100%;
			display: block;
			text-align: center;
		}
	}

	/** RTL **/
	.rtl {
		direction: rtl;
		font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
	}

	.rtl table {
		text-align: right;
	}

	.rtl table tr td:nth-child(2) {
		text-align: center;
	}
</style>
</head>

<body class="invoice-box" id="invoice-box" style="line-height: normal">
	<div >
		<table cellpadding="0" cellspacing="0" style="width:100%" border="1">
			<tr class="top">
				<td colspan="4">
					<table style="width:100%">
						<tr>
							<td class="title">
								<img src="{{asset('public/img/thb_black_logo.jpg')}}" style="width: 100%; max-width: 200px;" />
							</td>

							<td  style="color: black;text-align: end;line-height: normal;padding: 25px;">
								Invoice #: {{$invoice->id}}<br />
								Created: {{$invoice->created_at}}<br />
								Shipping: {{$invoice->shipping->type}}<br>

							</td>
						</tr>
					</table>
				</td>
			</tr>

			<tr class="information">
				<td colspan="4">
					<table style="width:100%">
						<tr>
							<td  style="color: black; padding: 25px;">
								{{$invoice->Place}}<br />
								{{$invoice->postcode}}
							</td>

							<td  style="color: black;text-align: end;padding: 25px;">
								{{$invoice->user->full_name}}<br />
								{{$invoice->user->phone}}<br />
								{{$invoice->user->email}}<br />

							</td>
						</tr>
					</table>
				</td>
			</tr>


			<tr class="heading"  style="color: black;">
				<td style="padding: 25px;">Item</td>
				<td style="padding: 25px;">Size</td>
				<td style="padding: 25px;">Qty</td>
				<td style="padding: 25px;">Price</td>

			</tr>
			@foreach($invoice->invoice_detail as $prod)

			<tr class="item"  style="color: black;">
				<td style="padding: 25px;">{{$prod->product->name}}

					@if($prod->invoice_question)
					@foreach($prod->invoice_question as $q)
					<p><strong>{{$q->question}}</strong></p>
					<p>{{$q->option}}</p>

					@endforeach
					@endif

				</td>
				<td style="padding: 25px;">{{$prod->size}}</td>
				<td style="padding: 25px;">{{$prod->qty}}</td>

				<td style="padding: 25px;">£{{$prod->total_price}}</td>

			</tr>

			@endforeach
			<tr  style="color: black;"><td style="padding: 25px;"><strong>Delivery Charges</strong></td><td></td> <td></td><td style="padding: 25px;"><strong>£{{$invoice->shipping->price}}</strong></td></tr>
			<tr  style="color: black;"><td style="padding: 25px;"><strong>Total</strong></td> <td></td><td></td><td style="padding: 25px;"><strong>£{{$invoice->total_price}}</strong></td></tr>

		</table>
	</div>
	 <!-- jQuery -->
  	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

  	<!-- printThis -->
	<script src="{{asset('public/print/printThis.js')}}"></script>
	
		<script type="text/javascript">

		function prints(){
			$("#invoice-box").printThis({
				 importCSS: false,
			});
			
		}
		function redirect(){
			window.location = "{{route('admin.dashboard')}}";
		}
		prints();
	</script>
</body>
</html>
