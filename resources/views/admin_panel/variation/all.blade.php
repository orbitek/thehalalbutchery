@extends('admin_panel.adminLayout') @section('content')
<script src="{{asset('js/lib/jquery.js')}}"></script>
<script src="{{asset('js/dist/jquery.validate.js')}}"></script>
<style>label.error {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
  padding:1px 20px 1px 20px;
}</style>



<div class="content-wrapper">
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Variation Table</h4>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th> Product</th>
                                    <th style="width: 80%;">
                                        variation
                                    </th>
                                    <th style="width: 80%;">
                                        Price
                                    </th>
                                    <th>Actions</th>
                                    
                                   
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($vlist as $var)
                                <tr>
                                    <td>
                                      {{ \App\Product::where(['id' => $var->product_id])->pluck('name')->first() }}
                                    </td>
                                    <td>
                                        {{$var->variation}}
                                    </td>
                                    <td>
                                       <input type="text" name="price" class="updatePrice" value="{{$var->price}}" id="{{$var->id}}"> 
                                    </td>
                                    
                                    <td>
                                        <a class="btn btn-warning" data-toggle="modal"  data-variation="{{$var->variation}}" data-price="{{$var->price}}" data-id="{{$var->id}}" >Edit</a>
                                   &nbsp
                                        <a href="{{route('admin.variation.delete', ['id' => $var->id])}}" class="btn btn-danger">Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Button trigger modal -->

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Update Variation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form class="forms-sample" method="post" action="{{route('admin.updateVariation')}}">
            {{csrf_field()}}

            <input type="hidden" name="id" id="id">
          <div class="form-group">
            <label for="recipient-name"  class="col-form-label">Variation</label>
            <input type="text" class="form-control" name="variationE" id="variationE">
          </div>
          <div class="form-group">
            <label for="message-text"  class="col-form-label">Price</label>
            <input class="form-control" name="priceE" id="priceE"></input>
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
      </form>
    </div>
  </div>
</div>
@endsection
@section('content_js')
    <script type="text/javascript">
        $( ".btn-warning" ).click(function() {
            var button = $(event.relatedTarget);
            var id =$(this).data('id');
            var price =$(this).data('price');
            var variation =$(this).data('variation');

            $('#id').val(id);
            $('#priceE').val(price);
            $('#variationE').val(variation);
           
            $('#exampleModal').modal('toggle');
        });

        $('select').on('change', function() {
  			alert( this.value );
});

     $('.updatePrice').on('blur', function() {
    		var id = $(this).attr('id');
    		var price = $(this).val();
    		var url = "{{route('ajaxUpdateVariation')}}";
    		var token=$("input[name=_token]").val();
    		$.ajax({
		   		type:'post',
		   		url:url,
		   		dataType: "JSON",
		   		async: false,
		   		data:{id:id, price:price, _token: token},
		   		success:function(msg)
		   		{
		   			document.getElementById("individualPrice_"+order_serial).innerHTML=" £"+(x*product_price).toFixed(2);
		   			document.getElementById("order_total").innerHTML = (msg[2].toFixed(2));
		   			$('#sub_total').val(msg[2].toFixed(2));

		   			shipping_prices();
		   		}
		   	});

		});

    </script>

@endsection