<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>The Halal Butchery {{ $title }}</title>
    <!-- Hotjar -->
    <!-- Hotjar Tracking Code for www.thehalalbutchery.com -->
    <script>
        (function(h, o, t, j, a, r) {
            h.hj = h.hj || function() {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {
                hjid: 2585988,
                hjsv: 6
            };
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>
    <!-- /Hotjar -->
    <link rel="shortcut icon" href="{{ asset('img/THB-fav.png') }}" />
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}" />


    <!-- Slick -->
    <link type="text/css" rel="stylesheet" href="{{ asset('public/css/slick.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('public/css/slick-theme.css') }}" />

    <!-- nouislider -->
    <link type="text/css" rel="stylesheet" href="{{ asset('public/css/nouislider.min.css') }}" />

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="{{ asset('public/css/font-awesome.min.css') }}">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="{{ asset('public/css/style2.css') }}" />
    <link type="text/css" rel="stylesheet" href="{{ asset('public/css/rewamp.css') }}?v=2.6" />

    <!-- Postcoder to complete address -->
    <link rel="stylesheet" href="{{ asset('public/css/postcoder-autocomplete.css') }}" />

    <!-- Swall Plugins -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.16.7/dist/sweetalert2.all.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />

    <!-- jquery -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!-- <script async src="https://www.googletagmanager.com/gtag/js?id=G-1DKP1G0WZY"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-1DKP1G0WZY');
</script> -->

    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-MR0ZNVZ5G8"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-MR0ZNVZ5G8');
    </script>



    <style type="text/css">
        .carousel-item {
            background: black;
        }

        .carousel-inner img {
            /*-webkit-mask-image: linear-gradient(to left, rgba(0,0,0,1), rgba(0,0,0,0.1));*/
        }

        .carousel-control-next:focus,
        .carousel-control-next:hover,
        .carousel-control-prev:focus,
        .carousel-control-prev:hover {
            opacity: 0.5;
        }

        .carousel-control-next,
        .carousel-control-next,
        .carousel-control-prev,
        .carousel-control-prev {
            opacity: 0.5;
        }

        .carousel-control-next,
        .carousel-control-prev {
            width: 5%;
        }
    </style>

    <!-- Popup style -->
    <style>
        body {
            font-family: Arial, Helvetica, sans-serif;
        }

        * {
            box-sizing: border-box;
        }

        /* Button used to open the contact form - fixed at the bottom of the page */
        .socials-button {
            display: none;
            padding: 16px 20px;
            border: none;
            cursor: pointer;
            opacity: 0.8;
            position: fixed;
            bottom: 23px;
            right: 28px;
            width: auto;
            z-index: 999;
        }

        /* The popup form - hidden by default */
        .socials-popup {
            display: none;
            position: fixed;
            bottom: 20%;
            width: 80%;
            right: 10%;
            border: 3px solid #f1f1f1;
            z-index: 9;
        }

        /* Add styles to the form container */
        .form-container {
            max-width: 300px;
            padding: 10px;
            background-color: white;
        }

        /* Full-width input fields */
        .form-container input[type=text],
        .form-container input[type=password] {
            width: 100%;
            padding: 15px;
            margin: 5px 0 22px 0;
            border: none;
            background: #f1f1f1;
        }

        /* When the inputs get focus, do something */
        .form-container input[type=text]:focus,
        .form-container input[type=password]:focus {
            background-color: #ddd;
            outline: none;
        }

        /* Set a style for the submit/login button */
        .form-container .btn {
            background-color: #04AA6D;
            color: white;
            padding: 16px 20px;
            border: none;
            cursor: pointer;
            width: 100%;
            margin-bottom: 10px;
            opacity: 0.8;
        }

        /* Add a red background color to the cancel button */
        .form-container .cancel {
            background-color: red;
        }

        /* Add some hover effects to buttons */
        .form-container .btn:hover,
        .socials-button:hover {
            opacity: 1;
        }
    </style>
    <!-- End Popup style -->
    <style>
        /*Right Side Buttons Start*/
        .sticky-container {
            padding: 0px;
            margin: 0px;
            position: fixed;
            right: -119px;
            top: 130px;
            width: 160px;
            z-index: 9999;
        }

        .sticky_social li {
            list-style-type: none;
            border-radius: 10px;
            background-color: #000000;
            color: #efefef;
            height: 43px;
            padding: 0px;
            margin: 0px 0px 1px 0px;
            -webkit-transition: all 0.25s ease-in-out;
            -moz-transition: all 0.25s ease-in-out;
            -o-transition: all 0.25s ease-in-out;
            transition: all 0.25s ease-in-out;
            cursor: pointer;
            filter: url("data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\'><filter id=\'grayscale\'><feColorMatrix type=\'matrix\' values=\'0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0\'/></filter></svg>#grayscale");
            filter: gray;
            -webkit-filter: grayscale(100%);
        }

        .sticky_social li:hover {
            margin-left: -115px;
            filter: url("data:image/svg+xml;utf8,<svg xmlns=\'http://www.w3.org/2000/svg\'><filter id=\'grayscale\'><feColorMatrix type=\'matrix\' values=\'1 0 0 0 0, 0 1 0 0 0, 0 0 1 0 0, 0 0 0 1 0\'/></filter></svg>#grayscale");
            -webkit-filter: grayscale(0%);
        }

        .sticky_social li img {
            float: left;
            margin: 5px 5px;
            margin-right: 10px;
        }

        .sticky_social li p {
            float: left;
            padding: 0px;
            margin: 0px;
            color: white;
            text-transform: uppercase;
            line-height: 43px;
        }

        /*Right Side Buttons End*/
    </style>
</head>

<body>
    <!--Right Side Buttons Start-->
    <div class="sticky-container" style="display: none;">
        <ul class="sticky_social">
            <li>
                <a href="https://www.instagram.com/thehalalbutcheryuk/">
                    <img width="32" height="32" title="THB Instagram" alt="THB Instagram"
                        src="{{ asset('public/images/socials/Icon-ins.png') }}" />
                    <p>Instagram</p>
                </a>
            </li>
            <li>
                <a href="https://www.facebook.com/thehalalbutcheryuk">
                    <img width="32" height="32" title="THB FaceBook" alt="THB FaceBook"
                        src="{{ asset('public/images/socials/Icon-fb.png') }}" />
                    <p>Facebook</p>
                </a>
            </li>
            <li>
                <a href="https://twitter.com/halalbutcheryuk?s=11">
                    <img width="32" height="32" title="THB Twitter" alt="THB Twitter"
                        src="{{ asset('public/images/socials/Icon-twi.png') }}" />
                    <p>Twitter</p>
                </a>
            </li>
            <li>
                <a href="https://vm.tiktok.com/ZMd3fC2To/">
                    <img width="32" height="32" title="THB TikTok" alt="THB TikTok"
                        src="{{ asset('public/images/socials/Icon-tik.png') }}" />
                    <p>TikTok</p>
                </a>
            </li>
            <li>
                <a href="https://wa.me/message/RWVS64NEC6GTB1">
                    <img width="32" height="32" title="THB Whatsapp" alt="THB Whatsapp"
                        src="{{ asset('public/images/socials/Icon-wapp.png') }}" />
                    <p>Whatsapp</p>
                </a>
            </li>

        </ul>
    </div>
    <!--Right Side Buttons End-->
    <!-- HEADER -->
    <header class="mainHeader">

        <!-- TOP HEADER -->
        <div id="top-header" style="padding:0px; background:#BEA004; ">
            <div class="container" style="min-width: 90%;text-align: center;font-size: 18px;font-weight: 500;">
                <span>Order <span style="color: white">Over £40 </span> FOR FREE SAME DAY ANY TIME DELIVERY</span>

            </div>
            <!-- /TOP HEADER -->

            <!-- MAIN HEADER -->
            <div id="header">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row header-row">

                        <!-- Tag Line -->
                        <div class="col-md-3 col-lg-4">
                            <p
                                style=" color: white;font-size: 1.5vw;font-family: Montserrat;font-weight: 900;padding-left: 0px;">
                                THE 3HR MEAT DELIVERY SERVICE</p>
                        </div>
                        <!-- /Tag Line -->
                        <!-- LOGO -->
                        <div class="col-md-2 col-lg-2">
                            <div class="header-logo">
                                <a href="{{ route('user.home') }}" class="logo">
                                    <img src="{{ asset('public/img/THB-Logo.png') }}"
                                        style="max-width: 168px;width: 100%;" alt="">
                                </a>
                            </div>
                        </div>
                        <!-- /LOGO -->

                        <!-- SEARCH BAR -->
                        <div class="col-xs-3 col-md-3 col-xs-3">
                            <div class="">
                                <form action="{{ route('user.search') }}" method="get">
                                    <div class=" input-icons tutuclass ">
                                        <input class="input input-field" id="tutu"
                                            style="border-radius: 40px;width: 100%; " name="n"
                                            placeholder="Search">
                                        <i class="fa fa-search icon " style="color: #BEA004;font-size: 18px;"
                                            aria-hidden="true"></i>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- /SEARCH BAR -->


                        <!-- Modal -->
                        {{-- <div class="modal" id="mymodal">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content" style="background:#ffffffb8">
                                    <div class="modal-header">
                                        <h3>Login</h3>
                                        <button id="closemodal" type="button" class="close"
                                            data-dismiss="mymodal">
                                            &times;</button>
                                    </div>

                                    <div class="modal-body">
                                        <form method="post" action="{{ route('user.login') }}" id="modalLoginForm">
                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                <input class="input" type="email" id="email" name="email"
                                                    class="form-control"
                                                    style="border: 1px solid #c1c7d3; border-radius: 40px"
                                                    placeholder="Email" required>
                                            </div>
                                            <div class="form-group">
                                                <input class="input" type="password" name="password"
                                                    class="form-control" id="pass"
                                                    style="border: 1px solid #c1c7d3; border-radius: 40px"
                                                    placeholder="Password" required>
                                            </div>


                                            <div class="modal-footer">
                                                <button type="button" class="btn order-submit" data-dismiss="modal"
                                                    style="margin: 0px 2.5px 2.5px; padding: px 25px">Close</button>
                                                <input type="submit" class="primary-btn order-submit"
                                                    style="font-size: 14px; margin: 0px 2.5px 2.5px; padding: 0px 25px"
                                                    value="Login">
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div> --}}





                        {{-- // const mymodal = new bootstrap.Modal('#mymodal');
                        // window.addEventListener('DOMContentLoaded', () => {
                        // mymodal.show();
                        // });



                        // $(document).ready(function() {

                        // $("#mymodal").show();
                        // if () {

                        // }
                        // }); --}}



                        {{-- $(document).ready(function() {
                        $("#myModal").show();
                        }); --}}







                        <!-- ACCOUNT -->
                        <div class="col-xs-3 col-md-3">

                            <div class="header-ctn">
                                <!-- Account -->
                                @if (session()->has('user'))
                                    <div class="dropdown col-md-4 col-lg-4" style="padding-left: 0px;">
                                        <a class=" dropbtn " id="custom_shopping_cart"
                                            href="{{ route('editProfile') }}">
                                            <i class="fa fa-user " matBadge="15"
                                                style="color: white;font-size: 32px;"></i>
                                            <span>{{ session()->get('user')->full_name }}</span>
                                        </a>
                                        <div class="dropdown-content">
                                            <a href="{{ route('editProfile') }}"><i class="fa fa-user"
                                                    aria-hidden="true"></i></a>
                                            <a href="{{ route('user.history') }}"><i class="fa fa-history"
                                                    aria-hidden="true"></i></a>
                                            <a href="{{ route('user.logout') }}"> <i class="fa fa-sign-out"></i></a>
                                        </div>
                                    </div>
                                @else
                                    <div class="dropdown col-md-4 col-lg-4" style="padding-left: 0px;">
                                        <a class="dropdown-toggle " id="custom_shopping_cart"
                                            href="{{ route('user.login') }}">
                                            <i class="fa fa-user " matBadge="15"
                                                style="color: white;font-size: 32px;"></i>
                                            <span>Account</span>
                                        </a>
                                    </div>
                                @endif

                                <!-- Cart -->
                                <div class="dropdown col-md-3 col-lg-3">
                                    <a class="dropdown-toggle " id="custom_shopping_cart"
                                        href="{{ route('user.cart') }}">
                                        <i class="fa fa-shopping-cart " matBadge="15"><span
                                                class='badge badge-warning' id='lblCartCount'>
                                                {{ Session::get('orderCounter') ? Session::get('orderCounter') : '0' }}
                                            </span></i>
                                        <span>Cart</span>
                                    </a>

                                </div>
                                <!-- /Cart -->

                                <!-- Menu Toogle -->
                                <div class="menu-toggle pull-right col-xs-1 col-md-1" style="padding-right: 0px;">
                                    <a href="#">
                                        <i class="fa fa-bars"></i>
                                        <span>Menu</span>
                                    </a>
                                </div>
                                <!-- /Menu Toogle -->
                            </div>

                        </div>
                        <!-- /ACCOUNT -->
                    </div>
                    <!-- row -->
                </div>
                <!-- container -->
            </div>
            <!-- /MAIN HEADER -->
    </header>
    <!-- /HEADER -->

    <!-- Mobile Header -->
    <!-- HEADER -->
    <header class="mobileMainHeader">



        <!-- MAIN HEADER -->
        <div id="header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row header-row">

                    <!-- Menu Toogle -->
                    <div class="header-ctn col-sm-1 col-xs-1">
                        <div class="menu-toggle " style="padding-right: 0px;">
                            <a href="#">
                                <i class="fa fa-bars"></i>

                            </a>
                        </div>
                    </div>
                    <!-- /Menu Toogle -->

                    <!-- SEARCH BAR -->
                    <div class="col-sm-3 col-xs-3">
                        <div class="">

                            <i class="fa fa-search icon search-display" style="color: white;font-size: 32px;"
                                aria-hidden="true"></i>

                        </div>
                    </div>
                    <!-- /SEARCH BAR -->

                    <!-- LOGO -->
                    <div class="col-sm-4 col-xs-4">
                        <div class="header-logo">
                            <a href="{{ route('user.home') }}" class="logo">
                                <img src="{{ asset('public/img/THB-Logo.png') }}"
                                    style="max-width: 70px;width: 100%;" alt="">
                            </a>
                        </div>
                    </div>
                    <!-- /LOGO -->



                    <!-- ACCOUNT -->

                    @if (session()->has('user'))
                        <div class="dropdown col-sm-2 col-xs-2" style="padding-left: 0px;text-align: right;">
                            <a class=" dropbtn " id="custom_shopping_cart" href="{{ route('editProfile') }}">
                                <i class="fa fa-user " matBadge="15" style="color: white;font-size: 32px;"></i>

                            </a>
                            <div class="dropdown-content">
                                <a href="{{ route('editProfile') }}"><i class="fa fa-user"
                                        aria-hidden="true"></i></a>
                                <a href="{{ route('user.history') }}"><i class="fa fa-history"
                                        aria-hidden="true"></i></a>
                                <a href="{{ route('user.logout') }}"> <i class="fa fa-sign-out"></i></a>
                            </div>
                        </div>
                    @else
                        <div class="dropdown col-sm-2 col-xs-2" style="padding-left: 0px;">
                            <a class="dropbtn " id="custom_shopping_cart" href="{{ route('user.login') }}">
                                <i class="fa fa-user " matBadge="15" style="color: white;font-size: 32px;"></i>

                            </a>
                        </div>
                    @endif


                    <!-- Cart -->
                    <div class="dropdown col-sm-2 col-xs-2" style=" padding-right: 0px;text-align: right;">
                        <a class="dropdown-toggle " id="custom_shopping_cart" href="{{ route('user.cart') }}">
                            <i class="fa fa-shopping-cart " matBadge="15"
                                style="font-size: 32px; color: white; margin: 0 AUTO;"><span
                                    class='badge badge-warning' id='lblCartCount'>
                                    {{ Session::get('orderCounter') ? Session::get('orderCounter') : '0' }} </span></i>

                        </a>

                    </div>
                    <!-- /Cart -->





                    <!-- /ACCOUNT -->

                    <!-- Hidden Search bar -->
                    <div class="col-sm-12 col-xs-12 search-bar" style="display: none;">
                        <div class="">
                            <form action="{{ route('user.search') }}" method="get">
                                <div class=" input-icons tutuclass ">
                                    <input class="input input-field search-field" id="tutu"
                                        style="border-radius: 40px;width: 100%; " name="n">
                                    <i class="fa fa-search icon " style="color: #BEA004;font-size: 18px;"
                                        aria-hidden="true"></i>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- Hidden Search bar -->
                </div>
                <!-- row -->
            </div>
            <!-- container -->
        </div>
        <!-- /MAIN HEADER -->
        <!-- TOP HEADER -->
        <div id="top-header" style="padding:0px; background:white; ">
            <div class="container" style="min-width: 90%;text-align: center;font-size: 18px;font-weight: 500;">
                <span style="color:#BEA004;">The 3HR Meat delivery Service</span>

            </div>
            <!-- /TOP HEADER -->
    </header>
    <!-- /HEADER -->
    <!-- /Mobile Header -->

    <!-- NAVIGATION -->
    <nav id="navigation">
        <!-- container -->
        <div class="container" style="min-width: 90%">
            <!-- responsive-nav -->
            <div id="responsive-nav" style="overflow: auto;padding-top: 0px;">
                <!-- NAV -->
                <ul class="navbar" style="margin-bottom: 0px">
                    <li style="background: none; float: right; "><span class="nav-tog ">X</span>
                    <li id="nav-logo" class="mobile-logo"
                        style="width: 170px;height: 170px; display: none;margin-left: 15px;">

                        <div class="header-logo">
                            <a href="{{ route('user.home') }}" class="logo">
                                <img src="{{ asset('public/img/THBN.png') }}" style="width: 170px;height: 170px;"
                                    alt="">
                            </a>
                        </div>

                    </li>
                    <a href="{{ route('user.home') }}" class="{{ Route::is('user.home') ? 'active' : '' }} ">
                        <li><span> Home</span></li>
                    </a>
                    @if (Route::is('user.search'))
                        @foreach ($cat->where('status', 1)->sortBy('position') as $c)
                            <a href="{{ route('user.search.cat', ['id' => $c->id]) }}"
                                class=" nav-item {{ $c->id == $a ? 'active' : '' }}">
                                <li style="color:;"><span> {{ $c->name }}</span></li>
                            </a>
                        @endforeach
                        <a href="search" class=" nav-item {{ $a == -1 ? 'active' : '' }}">
                            <li><span> Browse All</span></li>
                        </a>
                    @else
                        @foreach ($cat->where('status', 1)->sortBy('position') as $c)
                            <!-- style="background-image:url('https://thehalalbutchery.com/li/{{ $c->id }}.png'); -->
                        <a href="{{ route('user.search.cat', ['id' => $c->id]) }}"><li style="background-image:url('https://thehalalbutchery.com/li/{{ $c->id }}.png');"><span>{{ $c->name }}</span> </li></a>
@endforeach
                       <a href="{{ route('user.search') }}"> <li ><span> Browse All</span></li></a>
                    @endif
                    
                </ul>
                <!-- /NAV -->
            </div>
            <!-- /responsive-nav -->
        </div>
        <!-- /container -->
    </nav>
    <!-- /NAVIGATION -->

    <!-- SECTION -->

    <div class="section" style="padding: 0px">
        <!-- container -->
        <div class="container" style="padding: 0px; min-width: 100%;">
            @if (session('status'))
                <script type="text/javascript">
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-midle',
                        showConfirmButton: true,
                        timer: 4000,
                        timerProgressBar: true,
                        customClass: 'swal-narrow',
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'success',
                        title: "{{ session('status') }}"
                    })
                </script>
            @endif

            @if (session('warning'))
                <script type="text/javascript">
                    const Toast = Swal.mixin({
                        toast: true,

                        position: 'top-midle',
                        showConfirmButton: true,
                        timer: 4000,
                        timerProgressBar: true,
                        customClass: 'swal-narrow',

                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'warning',
                        title: "{{ session('warning') }}"
                    })
                </script>
            @endif
            @if (session('error'))
                <script type="text/javascript">
                    const Toast = Swal.mixin({
                        toast: true,

                        position: 'top-midle',
                        showConfirmButton: true,
                        timer: 4000,
                        timerProgressBar: true,
                        customClass: 'swal-narrow',
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'error',
                        title: "{{ session('error') }}"
                    })
                </script>
        </div>
        @endif
        <!-- If store is closed -->
        @if (isset($closed))
            @if ($closed == 1)
                <script type="text/javascript">
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-middle',
                        showConfirmButton: true,
                        timer: 4000,
                        timerProgressBar: true,
                        customClass: 'swal-narrow',
                        didOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'warning',
                        title: "{{ 'Store is now closed! Any orders placed after 7pm (4pm - Sunday) will be processed the following day from 10am!' }}"
                        // title: "{{ 'Eid Mubarak! The Halal Butchery will be closed on Monday 2nd May. Any orders placed after 4pm - Sunday 1st May will be processed and dispatched on Tuesday 3rd May.' }}"
                    })
                </script>
            @endif
        @endif
        <!-- row -->
        @if (Route::is('user.home'))
            <!-- Slider Stat -->
            <div id="demo" class="carousel slide carousel-fade" data-ride="carousel">
                <ul class="carousel-indicators" style="display: none;">
                    <li data-target="#demo" data-slide-to="0" class="active"></li>
                    <li data-target="#demo" data-slide-to="1"></li>
                </ul>
                <div class="carousel-inner">

                    {{-- <div class="carousel-item active">
                    <a href="{{route('user.search')}}">
                        <img src="{{asset('images/slides/HFFMCR1.png')}}" alt="Mixed Kebab Box" style="width: 100%; ">
                    </a>  
                </div> --}}

                    <div class="carousel-item active">
                        <img src="{{ asset('public/images/slides/main.png') }}" alt="Mixed Kebab Box"
                            style="width: 100%; ">
                        <div class="carousel-caption fadeInLeft" style="width:100%;top:20%">
                            <p style="text-align: left;letter-spacing: 5PX;font-size: 2vw;color: white;">Welcoming
                                students back to Manchester with 20% off</p>
                            <h2 style="text-align: left;letter-spacing: 5px;font-size: 4vw;"><span
                                    style="color: white">USE CODE: </span><span style="color: #ad9802">SEPT20</span>
                            </h2>


                        </div>
                    </div>


                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#demo" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>
                </div>

                <?php
                session_start();
                $showPopup = !isset($_SESSION['show_popup']) || $_SESSION['show_popup'];
                $_SESSION['show_popup'] = false;
                ?>

                @if ($showPopup)

                    {{-- @if ($myModal) --}}
                    @if (!session()->has('modal'))
                        <div id="myModal" class="modal" style="background: #00000087">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Join the THB</h5>
                                        <button type="button" class="close" data-dismiss="modal"
                                            onclick="hideModal()" style="color: white">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="backgroundimg">
                                            <img src="{{ asset('public/images/slides/main.png') }}" class="modalbg">
                                        </div>
                                        <h3 style="text-align: center;">Subscribe</h3>
                                        <p style="text-align: center"> to our mailing list to get the latest updates
                                            straight in
                                            your
                                            inbox.</p>
                                        <form method="Post" action="{{ route('subscribeNow') }}"
                                            style="flex-direction: column; display:flex; text-align:center">
                                            {{ csrf_field() }}
                                            <div class="form-group">
                                                <input class="input" type="email" name="email" required
                                                    placeholder="Enter Your Email">
                                            </div>
                                            <button type="submit" class="btn btn-primary"
                                                style="background-color: #BEA004; border:1px solid #BEA004; border-radius: 30px; font-size: 14px; font-weight: 400; width:14rem; margin:auto; display:block">Join</button>
                                            {{-- <button onclick="hideModal()" type="button" class="btn btn-secondary"
                                            style="border:1px solid; border-radius: 30px">Close</button> --}}
                                            <a href="javascript:hideModal()"
                                                style="text-decoration: underline; padding:1rem"> No
                                                Thanks, I am not interested</a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <style>
                            .modal-content {
                                height: 46.5rem;
                                width: 100%;
                            }

                            .modal-header {
                                background-color: rgba(0, 0, 0, 0.8);
                                padding: 16px 16px;
                                color: #FFF;
                                border-bottom: 2px solid #337AB7;
                            }

                            .modalbg {
                                height: 100%;
                                width: 100%;
                                padding-bottom: 2.5rem;
                            }

                            .modal-title {
                                width: 100%;
                                font-size: 2.5rem;
                                color: white;
                                text-align: center;
                                align-items: center;
                                justify-content: center;
                            }
                        </style>

                        <script>
                            $(document).ready(function() {
                                $('#myModal').show();
                            });

                            function hideModal() {
                                $("#myModal").hide();
                            }

                            // if (#subscribeemail != "") {
                            function subcribe() {
                                alert("Subscribed succesfully");
                                hideModal();
                            }
                            // }
                        </script>
                        {{-- {{ session()->put('modal', 'shown') }} --}}
                    @endif
                    {{-- @endif --}}
                @endif

                <!-- Slider End -->
                <!-- About Us -->

                <div class="row"
                    style="padding-left: 5%; background: none; margin-bottom: 20px; align-items: center;display:none;">
                    <div class="col-md-3" style="text-align:center;">
                        <img src="{{ asset('public/img/THBN.png') }}" id="nav-logo" class="about-logo"
                            style="width: 100%;" alt="THB">
                    </div>
                    <div class="col-md-9" style="text-align: left;padding: 2%;font-size: 18px !important;">
                        <div>
                            <h4 style="font-size: 40px;">
                                ABOUT THB
                            </h4>
                            <hr style="width:30%; float: left;"><br>
                        </div>
                        <div style="min-height: 180px;     display: inline-block;">
                            <p style="width:90%;text-size: 18px; margin-top: 2%;">The Halal Butchery is an online
                                meat
                                store and more. To fulfil expanding customer expectations and the evolution of
                                consumer
                                culture, The Halal Butchery has incorporated technology with the meat industry. As a
                                result, the maximum convenience of ordering products in the contemporary world is
                                now
                                available to you wherever you are, anytime you want.<br>
                                Need your meat by this evening? … Not a problem.
                                <br>
                            </p>
                        </div>
                        <br>
                        <br>
                        <div style="">
                            <form action="{{ route('aboutUs') }}">
                                <button class="white_color carousel_order_now" type="submit">Read More</button>
                            </form>
                        </div>
                    </div>


                </div>

                <!-- /row -->
                <!-- /About Us -->
            </div>




    </div>
    <!-- /container -->
    </div>
    <!-- SECTION -->
    <!-- Section -->
    <div class="section steps-back">
        <div class="container" style="text-align: center;">
            <h1
                style="margin-top: 20px;color: #BEA004;font-size: 5rem; text-shadow: 2px 2px #000000;margin-bottom: 30px;">
                LATEST PROMOTIONS</h1>


            <div class="row Promo_section" style="margin-bottom: 25px;">
                <div class="col-md-6 promo-detaild">
                    <h1 style="color:white;">BBQ SAVER BOX</h1>
                    <div class="div10"></div>
                    <p
                        style="color:white;font-size: 19px;text-decoration: line-through;color: #636363; text-decoration-color: red;">
                        Was £ 57.59</p>
                    <div class="div10"></div>
                    <p style="color:white;font-size: 36px;">Now Only</p>
                    <div class="div10"></div>
                    <h5 style="color:red;font-size: 36px;">£ 47.99</h5>
                    <div class="div10"></div>
                    <form action="{{ env('APP_URL') }}/productName/bbq-saver-box">
                        <button class="white_color carousel_order_now" type="submit" style=" ">Shop
                            Now</button>
                    </form>
                </div>

                <div class="col-md-6"
                    style="background: black;padding-left: 0px;padding-right: 0px;border-radius: 0px 40px 40px 0px;">
                    <img src="{{ asset('public/images/offers/BBQS.png') }}" class="promo-imaged"
                        alt="Premium BBQ Box" style=" ">
                </div>

            </div>


            <div class="row promo_section" style="margin-bottom: 25px;">
                <div class="col-md-6"
                    style="background: url()black;padding-left: 0px;padding-right: 0px;border-radius: 40px 0px 0px 40px;">
                    <img src="{{ asset('public/images/offers/BBQB.png') }}" class="promo-image"
                        alt="Premium BBQ Box" style="">
                </div>
                <div class="col-md-6 promo-detail" style="">
                    <h1 style="color:white;">THE THB BBQ BOX</h1>
                    <div class="div10"></div>
                    <p
                        style="color:white;font-size: 19px;    text-decoration: line-through;color: #636363; text-decoration-color: red;">
                        Was £94.99</p>
                    <div class="div10"></div>
                    <p style="color:white;font-size: 36px;">Now Only</p>
                    <div class="div10"></div>
                    <h5 style="color:red;font-size: 36px;">£ 83.99</h5>
                    <div class="div10"></div>
                    <form action="{{ env('APP_URL') }}/productName/the-thb-bbq-box-feeds-6-people">
                        <button class="white_color carousel_order_now" type="submit" style="  ">Shop
                            Now</button>
                    </form>
                </div>

            </div>

            <div class="row Promo_section">
                <div class="col-md-6 promo-detaild">
                    <h1 style="color:white;">KING OF THE GRILL BOX</h1>
                    <div class="div10"></div>
                    <p
                        style="color:white;font-size: 19px;    text-decoration: line-through;color: #636363; text-decoration-color: red;">
                        Was £132.99</p>
                    <div class="div10"></div>
                    <p style="color:white;font-size: 36px;">Now Only</p>
                    <div class="div10"></div>
                    <h5 style="color:red;font-size: 36px;">£ 119.99</h5>
                    <div class="div10"></div>
                    <form action="{{ env('APP_URL') }}/productName/king-of-the-grill-box">
                        <button class="white_color carousel_order_now" type="submit" style=" ">Shop
                            Now</button>
                    </form>

                </div>

                <div class="col-md-6"
                    style="background: black;padding-left: 0px;padding-right: 0px;border-radius: 0px 40px 40px 0px;">
                    <img src="{{ asset('public/images/offers/KG.png') }}" class="promo-imaged" alt="Premium BBQ Box"
                        style=" ">
                </div>

            </div>
        </div>
    </div>
    <!-- Section -->
    <!-- Section HMC -->
    <div class="section row" style="padding-left: 5%; display: none;  ">
        <div class="col-md-3" style="text-align:center;">
            <img src="{{ asset('public/img/Logo-HMC.png') }}" id="nav-logo"
                style="width: 100%;    max-width: 417px; " alt="THB">
        </div>
        <div class="col-md-9 hmc" style="text-align: left;padding: 2%;">
            <div>
                <h4 style="">
                    HMC - HALAL MONITORING COMITTEE
                </h4>
                <hr style="width:30%; float: left;"><br>
            </div>
            <div style="    min-height: 180px;    display: inline-block;">
                <p style="width:90%;text-size: 18px; margin-top: 2%;">HMC is our Halal Certifier. HMC wishes for all
                    consumers to be confident and assured that the Halal meat and products they purchase are genuinely
                    Halal. Consuming Halal foods is one of the most important tenets of ones faith without which our
                    actions and our prayers are at risk and said to remain unanswered.<br>

            </div>
            <br>
            <div style="">
                <form action="https://thehalalbutchery.com/product/43">
                    <button class="white_color carousel_order_now" type="submit" style=" ">Shop Now</button>
                </form>
            </div>
        </div>


    </div>
    <!-- Section -->
    @endif

    @yield('content')


    <!-- FOOTER -->
    <footer id="footer">
        <!-- top footer -->
        <div class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->


                <div class="row">
                    <div class="col-md-8 col-xs-12">
                        <div class="newsletter">
                            <p style="color:white;text-align:left;"><strong>GET SPECIAL OFFERS & WEEKLY
                                    RECIPES</strong></p>
                            <form method="Post" style="margin: 20px 0px 20px 0px;"
                                action="{{ route('subscribeNow') }}">
                                {{ csrf_field() }}
                                <input class="input" type="email" name="email" required
                                    placeholder="Enter Your Email">
                                <button class="newsletter-btn"><i class="fa fa-envelope"></i> Subscribe</button>
                            </form>
                            <ul class="newsletter-follow" style="text-align:left;">
                                <li>
                                    <a href="https://www.instagram.com/thehalalbutcheryuk/"
                                        style="background: none; border: none">
                                        <img width="32px" height="32px" title="THB Instagram" alt="THB Instagram"
                                            src="{{ asset('public/images/socials/Icon-ins.png') }}" />
                                    </a>
                                </li>

                                <li>
                                    <a href="https://www.facebook.com/thehalalbutcheryuk"
                                        style="background: none; border: none">
                                        <img width="32px" height="32px" title="THB FaceBook" alt="THB FaceBook"
                                            src="{{ asset('public/images/socials/Icon-fb.png') }}" />
                                    </a>
                                </li>

                                <li>
                                    <a href="https://twitter.com/halalbutcheryuk?s=11"
                                        style="background: none; border: none">
                                        <img width="32px" height="32px" title="THB Twitter" alt="THB Twitter"
                                            src="{{ asset('public/images/socials/Icon-twi.png') }}" />
                                    </a>
                                </li>

                                <li>
                                    <a href="https://vm.tiktok.com/ZMd3fC2To/" style="background: none; border: none">
                                        <img width="32px" height="32px" title="THB TikTok" alt="THB TikTok"
                                            src="{{ asset('public/images/socials/Icon-tik.png') }}" />
                                    </a>
                                </li>

                                <li>
                                    <a href="https://wa.me/message/RWVS64NEC6GTB1"
                                        style="background: none; border: none">
                                        <img width="32px" height="32px" title="THB Whatsapp" alt="THB Whatsapp"
                                            src="{{ asset('public/images/socials/Icon-wapp.png') }}" />

                                    </a>
                                </li>

                            </ul>
                        </div>
                    </div>



                    <div class="clearfix visible-xs"></div>

                    <div class="col-md-2 col-xs-12">
                        <div class="footer" style="    text-align: left;">
                            <h3 class="footer-title" style="color:#BEA004">Support</h3>
                            <ul class="footer-links" style="color:white;">
                                <li><a href="{{ route('aboutUs') }}"> Account</a></li>
                                <li><a href="{{ route('deliveryProcess') }}"> FAQs</a></li>
                                <li><a href="{{ route('deliveryProcess') }}"> Blog</a></li>
                                <li><a href="{{ route('deliveryProcess') }}"> Recipes</a></li>
                                <li><a href="{{ route('deliveryProcess') }}"> Wholesale</a></li>



                            </ul>
                        </div>
                    </div>

                    <div class="col-md-2 col-xs-12">
                        <div class="footer" style="    text-align: left;">
                            <h3 class="footer-title" style="color:#BEA004">Infomation</h3>
                            <ul class="footer-links" style="color:white;">

                                <li><a href="{{ route('aboutUs') }}"> About Us</a></li>
                                <li><a href="{{ route('deliveryProcess') }}"> Delivery Process</a></li>
                                <li><a href="{{ route('privacyPolicy') }}"> Privacy Policy</a></li>
                                <li><a href="{{ route('refundPolicy') }}"> Refund Policy</a></li>
                                <li><a href="{{ route('termsConditions') }}"> Terms & Conditions</a></li>
                                <li><a href="{{ route('siteMap') }}"> Site Map</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /top footer -->


        <!-- copyRight footer -->
        <div id="copyRight-footer" class="section" style="    padding-top: 10px;padding-bottom: 0px;">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p style="color: white;text-align: left;font-size: 15px;">THE HALAL BUTCHERY LIMITED Company
                            Number 12762518 T/A <span style="color: #BEA004;">THE HALAL BUTCHERY.</span> <span
                                style="float: right;"> <span>We accept all major credit and debit cards</span>
                                <a href="#"><i class="fa fa-cc-visa" style="color:white;"></i></a>
                                <a href="#"><i class="fa fa-credit-card" style="color:white;"></i></a>
                                <a href="#"><i class="fa fa-cc-paypal" style="color:white;"></i></a>
                                <a href="#"><i class="fa fa-cc-mastercard" style="color:white;"></i></a>
                                <a href="#"><i class="fa fa-cc-discover" style="color:white;"></i></a>
                                <a href="#"><i class="fa fa-cc-amex" style="color:white;"></i></a>
                            </span>
                        </p>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /copyRight footer -->
        <!-- Cockie Bar -->
        <div class="cookie-container">
            <div class="row">
                <p class="col-md-10">
                    We use cookies in this website to give you the best experience on our
                    site and show you relevant ads. To find out more, read our
                    <a href="#">privacy policy</a> and <a href="#">cookie policy</a>.
                </p>
            </div>

            <button class="cookie-btn col-md-2">
                Okay
            </button>
        </div>

    </footer>
    <!-- /FOOTER -->
    <!-- jQuery Plugins -->

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="{{ asset('public/js/slick.min.js') }}"></script>
    <script src="{{ asset('public/js/nouislider.min.js') }}"></script>
    <script src="{{ asset('public/js/jquery.zoom.min.js') }}"></script>
    <script src="{{ asset('public/js/main.js') }}"></script>
    <script src="{{ asset('public/js/lib/jquery.js') }}"></script>
    <script src="{{ asset('public/js/dist/jquery.validate.js') }}"></script>

    <script type="text/javascript">
        $(function() {
            setTimeout(function() {
                $('.fade-message').slideUp();
            }, 5000);
        });
        /*search bar code*/
        $(".search-display").click(function() {
            $(".search-bar").toggle(1000);
            $(".search-field").focus();
        });
        $(".search-field").focusout(function() {
            $(".search-bar").toggle(1000);
        });
        /*/ search bar code*/

        window.onscroll = function() {
            myFunction()
        };

        var header = document.getElementById("navigation");
        var sticky = header.offsetTop;


        function myFunction() {

            if (window.pageYOffset > sticky) {
                header.classList.add("sticky");
                // $('.mobile-logo').show();
            } else {
                header.classList.remove("sticky");
                // $('.mobile-logo').hide();
            }
        }

        $(".megamenu").on("click", function(e) {
            e.stopPropagation();
        });

        $(".second_level").click(function() {
            $(".second_child_level").toggle();
            $(".third_child_level").hide();

        });
        $(".third_level").click(function() {
            $(".third_child_level").toggle();

        });
        /*Menu Toggle*/
        $(".nav-tog").click(function() {
            $('#responsive-nav').removeClass('active');
        });


        /*Favrioutes ajax call*/
        function favourite(id) {
            var url = "https://thehalalbutchery.com/add_favourite/" + id;
            document.getElementById(id).style.color = "red";
            $.get(url);

        }
        /* /Favrioutes ajax call*/


        /*Cockie JS*/
        const cookieContainer = document.querySelector(".cookie-container");
        const cookieButton = document.querySelector(".cookie-btn");

        cookieButton.addEventListener("click", () => {
            cookieContainer.classList.remove("active");
            localStorage.setItem("cookieBannerDisplayed", "true");
        });

        setTimeout(() => {
            if (!localStorage.getItem("cookieBannerDisplayed")) {
                cookieContainer.classList.add("active");
            }
        }, 2000);
        /* /Cockie JS*/
        // Popup form

        function openForm() {
            document.getElementById("socials_popup").style.display = "block";
        }

        function closeForm() {
            document.getElementById("socials_popup").style.display = "none";
        }
    </script>

</body>

</html>
