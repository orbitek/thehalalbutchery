@extends('store.storeLayout')
@section('content')
<style>
	.col-md-4 {
		text-align: left;
	}
</style>
<!-- SECTION -->
<div class="section">
	<div class="container">
		<div class="row" style="margin-left: 10px; border-bottom: 1px solid #9c9c9c73; margin-bottom: 50px">
			<div class="col-md-4">
			<h2>THB Site Map</h2>				
			</div>
		</div>
		<div class="row">
		
		
		
		<div class="col-md-4">
			<h3>Services</h3>
			  <ul class="sitemap-links">
                                
                                <li><a href="{{route('aboutUs')}}"> About Us</a></li>
                                <li><a href="{{route('deliveryProcess')}}"> Delivery Process</a></li>
                                <li><a href="{{route('privacyPolicy')}}"> Privacy Policy</a></li>
                                <li><a href="{{route('refundPolicy')}}"> Refund Policy</a></li>
                                <li><a href="{{route('termsConditions')}}"> Terms & Conditions</a></li>
                              
               </ul>
		</div>
		<div class="col-md-4">
			<h3>Catagories</h3>
			  <ul class="sitemap-links">
                                
                        @foreach($cat as $c)
                        <li ><a href="https://thehalalbutchery.com/searchCategory/{{ $c->slug }}" class="">{{$c->name}}</a></li>
                        @endforeach
                        <li ><a href="search" class="">Browse All</a></li>
                              
               </ul>
		</div>

		<div class="col-md-4">
			<h3>Products</h3>
			  <ul class="sitemap-links">
                                
                        @foreach($products as $p)
                        <li ><a href="https://thehalalbutchery.com/productName/{{ $p->slug }}" class="">{{$p->name}}</a></li>
                        @endforeach
                              
               </ul>
		</div>

		</div>
	</div>
</div>
@endsection
<!-- /SECTION -->
