@extends('store.storeLayout')
@section('content')
<script src="{{asset('public/js/lib/jquery.js')}}"></script>
<script src="{{asset('public/js/dist/jquery.validate.js')}}"></script>

<link type="text/css" rel="stylesheet" href="{{asset('public/css/style_for_quantity.css')}}" />
<style>
     @media only screen and (max-width: 767px){
            .billing-details {
                display: inline-block;

            }
            #quantity{
                width: 50%;
            }
        }
label.error {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
  padding:1px 20px 1px 20px;
}

    .rTable {

    display: block;
    width:100%;

}
.rTableHeading, .rTableBody, .rTableFoot, .rTableRow{
    clear: both;
}
.rTableRow{
    border: 1px solid black;
}
.rTableHead, .rTableFoot{
    background-color: #DDD;
    font-weight: bold;

}
.rTableCell, .rTableHead {

    float: left;
    overflow: hidden;
    padding: 3px 1.8%;
    width:20%;

}
.rTableCell
{
    height: auto;
}
.rTableCell button
{
    border-radius: 50px;
    height: 30px;
    width: 30px;
}
.rTable:after {
    visibility: hidden;
    display: block;
    font-size: 0;
    content: " ";
    clear: both;
    height: 0;
}

.order-summary {
    margin: 0px 0px;
}
 .shipping{
    border-radius: 40px;
   /* background-color: gray;
    color: white;*/
    }
.shipping:focus {
  background-color: #ad9802;
}
hr.horizental {
  border-top: 1px solid #ffffff00;
}
.order-summary .order-col{
	 border-bottom: 1px solid #e6dede8c;
}
button{
    color: white;
}
#signupform{
	display: inline-block;

}
</style>
<?php $que = explode(',', Session::get('questions'));?>
<!-- SECTION -->
<div class="section">
	@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
    <!-- container -->
    <div class="container">
        <!-- row -->


            <!-- Order Details -->
            <div class="col-md-12 order-details" style="width: 100%;">
                <div class="section-title text-center">
                    <h3 class="title">Your Order</h3>
                </div>
                <div id="order_summary" class="order-summary">



                    @if($all != null)
                    <div class="rTable col-md-8">
                        <div class="rTableRow">
                            <div class="rTableHead"><strong>REMOVE</strong></div>
                            <div class="rTableHead"><strong>PRODUCT</strong></div>
                            <div class="rTableHead"><strong>SIZE</strong></div>
                            <div class="rTableHead"><strong>QUANTITY</strong></div>
                            <div class="rTableHead" hidden="hidden"><strong></strong></div>
                            <div class="rTableHead"><strong>PRICE</strong></div>

                        </div>
					@foreach($all as $c)
					@foreach($prod as $p)
					@if($c[0]==$p->id && $c[5] == $p->order)
                        <div  class="rTableRow" id="deleteItem_{{$c[5]}}">

                          <div class="rTableCell">  <button type="button" id="delete_item"  value={{$c[5]}} name="delete_item"  class="delete_item">X</button></div>
							<div class="rTableCell"><img src="public/uploads/products/{{$p->id}}/{{$p->image_name}}" height="50px" width="50px" hidden> {{$p->name}}

                                @if(Session::has('questions'))
                                <br>
                                 {{-- <span style="font-weight: bold;">{{$p['questions'][1]}}</span>
                                <span>{{$p['questions'][2]}}</span> --}}

                                 @foreach($que  as $ques)
                                 <?php $q = explode(':', $ques);?>
                                 @if(!empty($q[3]))
                                @if($p->order == $q[3])
                                <span style="font-weight: bold;">{{$q[1]}}</span>
                                <span>{{$q[2]}}</span><br>
                                @endif

                                @endif
                                @endforeach
                                @endif
                                </div>
                                <div class="rTableCell"> {{$p->size}} </div>


                            <!--quantity-->
                                                                <!--c[1] is pid and c[3] is order serial-->
                            <div class="rTableCell">
                           <button type="button" id="sub" style="border: 1px solid;" value={{$p->id}} data-rel={{$c[5]}} data-rel2={{$p->price}} data-rel3={{$p->size}} class="sub">-</button>
                        <input type="number"  id="quantity" style=" border-radius: 40px; text-align: center; background: white;" name={{$p->id}} value={{$c[1]}} min="1" max="100" readonly/>
                        <button type="button" id="add" style="border: 1px solid;" value={{$p->id}} data-rel={{$c[5]}} data-rel2={{$p->price}} data-rel3={{$p->size}}  class="add">+</button></div>

<!--                            -->
							<div class="rTableCell"  hidden="hidden"><div style="height:25px;width:25px;display:inline-block;background-color: #0000"></div></div>

							<div class="rTableCell"><div id="individualPrice_{{$c[5]}}">
                                £
                                @php
                                $tot =$p->price* $c[1];
                                echo $tot;
                                @endphp

                                </div></div>

						</div>

						@break
					@endif
					@endforeach
					@endforeach

                    </div>
                    <hr class="horizental">
                    <form method="post" name="cart" class="col-md-4" style="float: right;border-left: 1px solid #e6dede8c;">
                    <div class="section-title text-center">
                    <h3 class="title">Your Bill</h3>
                </div>
                    <div class="order-col">
                        	<div><strong>Subtotal</strong></div>
                        	<div style="float: right;">£ <strong class="order-total" id="order_total"  >{{Session::get('price')}} </strong></div>
                        	<input type="text" name="sub_total" hidden id="sub_total" value="{{Session::get('price')}}">
                    	</div>
                    <div class="order-col " >



                        	 <div><strong> Delivery</strong></div>
                           <div style="width: 50%">
                            <select name="shipping"  onchange="shipping_prices();" id="shipping" class="form-control shipping" style="width: 100%;">
                            @foreach($shipping as $ship)
                            <option value="{{$ship->id}}" {{($ship->id == 4 && Session::get('price') <40)?'disabled':''}} data-price="{{$ship->price}}">{{$ship->type}}</option>
                            @endforeach
                        </select>
                    		</div>
                        <div style=" text-align: end;">£ <strong id="shipping_price_v" class="order-total" >0</strong></div>
                        <input type="text" class="form-control" name="shipping_price" hidden id="shipping_price" value="">
                    </div>

                    <div class="order-col" id="dedicated_time">
                    	<label for="dedicated_time">Enter Date & Time</label> <input type="datetime-local" class="form-control"  name="dedicated_time_i" id="dedicated_time_i" placeholder="DD/MM - HH:MM" />

                    </div>


                    <div class="order-col">
                        <div><strong>Total</strong></div>
                        <div style=" ">£ <strong class="order-total" id="totalCost"> </strong></div>
                    </div>
                     <div class="order-col">
                        <div style="width: 30%;"><strong>Notes</strong></div>
                        <div style="width: 90%;"><textarea style="height:100px" class="form-control" name="notes" id="notes"></textarea> </div>
                    </div>
                    <div class="order-col" style="border-bottom: 0px">
                    	 {{csrf_field()}}
                        <input type="submit" id="confirm_order"  name="order" class="primary-btn order-submit" value="Confirm order" style="float: right;">
                   </center> </form>
                    </div>
                    @else
                    <div class="order-col">
                        <h1>Your Cart is Empty</h1>
                    </div>
                    @endif

                </div>

                @if(session('user'))
                    @if($all != null)
                   <center>
                       <!--  {{csrf_field()}}
                        <input type="submit" id="confirm_order"  name="order" class="primary-btn order-submit" value="Confirm order">
                   </center> </form> -->

                    @else
                        <a href="{{route('user.home')}}"><input type="button"  class="primary-btn order-submit" value="Order Now"></a>
                    @endif

                @else
                 @if(!session('user'))
        <div class="row" style="display: inline-block; width: inherit;">
        <form id="signupForm"  method="post" action="{{route('userSave')}}">
            {{csrf_field()}}
            <div class="col-md-6">
                <!-- Billing Details -->
                <div class="billing-details">
                    <div class="section-title">
                        <h3 class="title">SIGN UP</h3>
                    </div>

                    <div class="form-group ">
                        <input class="input" type="text" name="name" id="name" placeholder="Full Name">
                    </div>
                   {!! $errors->first('name', '<label class="error">:message</label>') !!}

                    <div class="form-group">
                        <input class="input" type="email" name="email" id="email" placeholder="Email" onkeyup="myFunction()">
                    </div>
                    <div id="for_duplicate-email"></div>
                     {!! $errors->first('email', '<label class="error">:message</label>') !!}
                    <div class="form-group">
                        <input class="input" type="text" name="address" id="address" placeholder="Address">
                    </div>
                     {!! $errors->first('address', '<label class="error">:message</label>') !!}
                    <div class="form-group">
                        <input class="input" type="text" name="city" id="city" placeholder="City">
                    </div>
                     {!! $errors->first('city', '<label class="error">:message</label>') !!}
                    <div class="form-group">
                         <input class="input" type="text" name="postcode" style="text-transform:uppercase" id="postcode" onblur="check_postcode()" placeholder="Post Code">
                    </div>
                     {!! $errors->first('post', '<label class="error">:message</label>') !!}
                    <div class="form-group">
                         <input class="input" type="text" name="tel" id="tel" placeholder="Mobile" pattern="[789][0-9]{9}" value="44 ---- ------">
                    </div>
                     {!! $errors->first('tel', '<label class="error">:message</label>') !!}
                    <div class="form-group">
                        <input class="input" type="password" name="pass" id="pass" placeholder="Enter Your Password">
                    </div>
                     {!! $errors->first('pass', '<label class="error">:message</label>') !!}
                    <div class="form-group">
                        <input class="input" type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password">
                    </div>
                    {!! $errors->first('confirm_password', '<label class="error">:message</label>') !!}


                    <br>

                        <input type="submit"  name="signup" class="primary-btn order-submit" value="Sign Up">
                </form>
               </div>

            @endif

                @endif




            <!-- /Order Details -->

        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>
<!-- /SECTION -->

<script>
$("#tel").keydown(function(e) {
    var oldvalue=$(this).val();
    var field=this;
    setTimeout(function () {
        if(field.value.indexOf('44') !== 0) {
            $(field).val(oldvalue);
        }
    }, 1);
});

function check_postcode() {
    var str =document.getElementById("postcode").value;
    str = str.toUpperCase();
    var n = str.startsWith("M") || str.startsWith("SK") || str.startsWith("WA");
    if (n != true)
    {
        var str =document.getElementById("postcode").value = '';
        Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Invalid Postcode!',
        footer: 'We are serving only in Manchester for now!'
    })
  }
  else{
    IsPostcode(str);
  }
  //alert(n);
}
function IsPostcode(postcode)
    {
     postcode = postcode.toString().replace(/\s/g, "");
    // alert(postcode);
    var regex = /^[A-Z]{1,2}[0-9]{1,2} ?[0-9][A-Z]{2}$/i;
    //alert(regex.test(postcode));
    if (regex.test(postcode) != true)
    {
      var str =document.getElementById("postcode").value = '';
        Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Invalid Postcode!',
        footer: 'We are serving only in Manchester for now!'
    })

    }
}
   //TO DO: ajax will take place

    $('.add').click(function () {

    var url="{{route('user.editCart')}}";
    var product_id= $(this).val();
    $(this).prev().val(+$(this).prev().val() + 1);
    var x=$(this).prev().val();
    var token=$("input[name=_token]").val();
    var order_serial=this.getAttribute('data-rel');
    var product_price=this.getAttribute('data-rel2');
    var size = this.getAttribute('data-rel3');
    var nPrice = x*product_price;


    $.ajax({
            type:'post',
            url:url,
            dataType: "JSON",
            async: false,
            data:{pid: product_id, newQ:x, oSerial:order_serial,size:size,nPrice:nPrice, _token: token},
            success:function(msg)
            {
                document.getElementById("individualPrice_"+order_serial).innerHTML=" £"+(x*product_price).toFixed(2);
                document.getElementById("order_total").innerHTML = (msg[2].toFixed(2));
                $('#sub_total').val(msg[2].toFixed(2));
                shipping_prices();
            }
            });


    });
    $('.sub').click(function () {

        var url="{{route('user.editCart')}}";
        var product_id= $(this).val();
        var order_serial=this.getAttribute('data-rel');
        var product_price=this.getAttribute('data-rel2');
        var size = this.getAttribute('data-rel3');
        var nPrice = x*product_price;

        if ($(this).next().val() > 1)
        {
            $(this).next().val(+$(this).next().val() - 1);
            var x=$(this).next().val();
            var token=$("input[name=_token]").val();


            $.ajax({
            type:'post',
            url:url,
            dataType: "JSON",
            async: false,
            data:{pid: product_id, newQ:x, oSerial:order_serial,size:size,nPrice:nPrice, _token: token},
            success:function(msg)
            {
                document.getElementById("individualPrice_"+order_serial).innerHTML=" £"+(x*product_price).toFixed(2);
                document.getElementById("order_total").innerHTML = (msg[2].toFixed(2));
                $('#sub_total').val(msg[2].toFixed(2));
                shipping_prices();

            }
            });


        }
    });

    $('.delete_item').click(function () {
        var url="{{route('user.deleteCartItem')}}";
        var serial= $(this).val();   //serial is the forth element of sale coloumn
        var token=$("input[name=_token]").val();
        var id_holder="deleteItem_"+serial;
        $.ajax({
                type:'post',
                url:url,
                dataType: "JSON",
                async: false,
                data:{serial:serial, _token: token},
                success:function(msg)
                {
                    if(msg=="Empty")
                        {
                        document.getElementById("order_summary").innerHTML = "<div class='order-col'><h1>Your Cart is Empty</h1></div>";
                        document.getElementById("confirm_order").style.visibility = "hidden";
                        }

                    //$("#deleteItem_".$p->id").load(location.href+" #refresh_div","");
                    document.getElementById(id_holder).innerHTML  = "";
                    document.getElementById("order_total").innerHTML = (msg[2].toFixed(2));
                    $('#sub_total').val(msg[2].toFixed(2));
                    shipping_prices();
                }
                });


    });





    //validation
    $(document).ready(function() {
        // validate the comment form when it is submitted
        //$("#commentForm").validate();

        // validate signup form on keyup and submit
        $("#signupForm").validate({
            rules: {
                name: "required",
                email: {
                    required: true,
                    email: true
                },
                address: "required",
                city: "required",
                postcode: {
                    required: true,
                },
                tel: "required",
                pass: {
                    required: true,
                    minlength: 5
                },
                confirm_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#pass"
                }



            },
            messages: {
                name: "Please enter your Fullname",
                email: "Please enter a valid email address",
                address: "Please enter your Address",
                city: "Please enter your City",
                address: "Please enter your Address",
                postcode: {
                    required: "Please enter Post code",
                },
                tel: "Please enter your Phone number",
                pass: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                confirm_password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                }


            }



        });


    });


function myFunction() {
    //var token={{ csrf_token() }};
    var email=$("#email").val();
    var token=$("input[name=_token]").val();
    var url="{{route('user.signup.check_email')}}";


            $.ajax({
                type:'post',
                url:url,
                dataType: "JSON",
                async: false,
                data:{email: email, _token: token},
                success:function(msg){


                        if(msg == "1")
                            {
                                document.getElementById("for_duplicate-email").innerHTML = "<label class='error'>This Email Address is Already taken</label>";


                            }
                    else
                        {
                            document.getElementById("for_duplicate-email").innerHTML = "";

                        }
                    }
             });

}
function shipping_prices()
{
	var price = $("#shipping").find(':selected').data('price');
	//alert(price);
	$('#shipping_price_v').html(price);
	$('#shipping_price').val(price);
	var sub_total = $('#sub_total').val();

	var sv = $('#shipping').val();
	if (sv == 2)
	{
		$('#dedicated_time').fadeIn("slow");
	}
	else{
		$('#dedicated_time').fadeOut("slow");
	}

	$('#totalCost').html((parseFloat(price)+parseFloat(sub_total)).toFixed(2));

}

shipping_prices();

function set_date()
{
	var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();
 if(dd<10){
        dd='0'+dd
    }
    if(mm<10){
        mm='0'+mm
    }

today = yyyy+'-'+mm+'-'+dd+'T15:30';
document.getElementById("dedicated_time_i").setAttribute("min", today);
}
set_date();

 $('#dedicated_time_i').change(function(){

    var datetime = $("#dedicated_time_i").val() ;
    var today = new Date().addHours(3);
    var time =  today.toLocaleTimeString();
  const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  didOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
})

Toast.fire({
  icon: 'success',
  title: 'Your Order Will take atleast 3 Hours To deliver.'
})
    });

 Date.prototype.addHours = function(h) {
  var t = this.setTime(this.getTime() + (h*60*60*1000));
  return this;
}
</script>
@endsection
