@extends('store.storeLayout')
@section('content')
<style type="text/css">
	
</style>
<!-- SECTION -->
<div class="section">
	<div class="container">
		<div class="row">
		<div class="col-md-1"></div>		
		<div class="col-md-10" style="padding-right: 20px;padding-left: 20px; text-align: left">
		<h2>THB Privacy Policy</h2>
		<p>With the introduction of GDPR, we have updated our Privacy Policy which sets out how we use
and protect information you give us when you use our website. Should we ask you to provide
certain information by which you can be identified when using our website; you can be rest
assured that it will only be used in accordance with this privacy statement. We may change this
policy from time to time by updating this page. We encourage all users to check this page from
time to time to ensure you are happy with any changes.</p>
<h4>WHAT WE COLLECT</h4>
<p>We may collect and process the following classes of data about you:</p>
<h5>Information you give us</h5>
<p>This is information about you that you provide to us on our site (when making purchases,
subscribing to our newsletter, subscribing to be kept up to date, reporting a problem, writing a
review, sending us correspondence or any details you provide when registering to use our site).
This information includes your name (first and last name), address, phone number, e-mail
address, financial and credit card information, order details and other information that you
provide us with.</p>
<h5>Information we collect about you</h5>
<p>We collect information automatically about your visit during your time on our website referred to
as &quot;cookies&quot;. The information collected allows us to deliver you with a positive user experience
when you browse the site. It also helps us to identify areas of improvement. For more
information, please see our cookies policy.</p>
<h5>Transactional information</h5>
<p>Our website collects details of transactions you have made through it; however, we do not
collect, process, or store/hold any credit or debit card information. All Payment Processing is
done by our secure payment gateway provider. Our payment gateway provider is Stripe
Payments. You can avail further information on their policies by visiting their
website <a href="https://www.Stripe.com">https://www.stripe.com.</a> </p>
<h5>Information we receive from other sources</h5>
<p>We work closely with third parties (including, for example, business partners, payment and
delivery services, advertising networks, analytics providers, search information providers,
marketing agencies and credit reference agencies) and may receive information about you from
them. We may combine this information with information you give us and information we collect
about you.</p>
<h4>WHAT WE DO WITH YOUR INFORMATION</h4>
<p>The information we collect is important to us as it allows us to better understand your needs and,
in turn, provide you with a better service. Your information is used in a variety of means
including:</p>
<ul>
	<li>To carry out our obligations arising from any contracts entered into between you and us and to
provide you with information, products or services that you request from us.</li>
<li>To process purchases, refunds, exchanges, and other transactions.</li>
<li>To provide you with information about other goods and services.</li>
<li>To allow you to participate in interactive features of our service, when you choose to do so.</li>
<li>To notify you about changes to our service(s).</li>
<li>To analyse for fraud protection and credit risk.</li>
<li>To administer our site and for internal operations, including troubleshooting, data analysis,
testing, research, statistical and survey purposes.</li>
<li>To keep our site safe and secure.</li>
<li>To improve our site to ensure that content is presented in the most effective manner for you
and for your computer.</li>
<li>To make suggestions and recommendations to you and other users of our site about goods or
services that may be of interest to you.</li></ul>

<h4>SOCIAL MEDIA PLATFORMS</h4>
<p>We operate social media platforms. These platforms are, in most cases, operated outside of the
EU and do not comply with current Data Privacy Act and subsequent GDPR provision, although
they may well conform to the U.S Privacy Shield protocol. It is our process and protocol that any
personally identifiable data gathered on these platforms is only in response to users interacting
out of their own volition with our marketing pages. The contact is deemed as a legitimate
business enquiry. The personal contact data is removed from the site once the enquiry is
processed or the user has requested us to do so.</p>

<h4>ANALYTICS</h4>
<p>Our website uses Google Analytics to collect information about how visitors use our website. We
anonymise this data at the point of collection and automatically delete user and event data that is
older than two years.</p>

<h4>SHARING OF INFORMATION</h4>
<p>We do not share, sell, lend or lease any of the information that uniquely identifies a subscriber
(such as email addresses or personal details) with anyone except to the extent it is necessary to
process transactions or provide services that you have requested. In order to fulfil your order,
process your payment details and to provide you with services, we may share your personal

information with our trusted service providers including but not limited to our payment gateway
provider, delivery partners, analytics &amp; search engine providers and credit reference agencies.</p>

<h4>HOW WE STORE YOUR PERSONAL DATA</h4>
<p>All information provided is stored on our secure servers and we take realistic steps to protect
your information in accordance with this privacy policy. Unfortunately, the transmission of
information via the internet is not completely secure. Although we will do our best to protect your
personal data, we cannot guarantee the security of any data transmitted to the website; and any
such transmission is done so at your own risk. Once we have received your information, we will
use strict procedures and security features to maintain the prevention of unauthorised access.</p>

<h4>LINKS TO OTHER WEBSITES</h4>
<p>It is quite possible that our website will include links to other websites of interest. Once you have
used these links and leave our website, it is important to note that we do not have any control
over that website or its policies. We, therefore, cannot be held responsible for the protection and
privacy of any information which you may provide to those websites. Any third-party website
included as a link on our website is not governed in any way by this privacy policy.</p>

<h4>YOUR RIGHTS</h4>
<p>
You have the following rights under law in respect of your personal information:</p>
<ul>
	<li>The right to be informed about the collection and use of your personal information;</li>
	<li>The right of access to your information to verify the legality of our use of it;</li>
	<li>The right to request that inaccurate or incomplete information about you is rectified;</li>
	<li>The right to request the deletion or removal of your information where there is no further
reason for us to use it (such as you have withdrawn your consent);</li>
<li>The right to restrict the use of your information;</li>
<li>The right to obtain and reuse the information that we have about you for your own purposes;</li>
<li>The right to object to certain uses (such as for marketing purposes);</li>
<li> and The right not to be subject to a decision that has a legal effect on you that has been based on
an automated decision.</li>
</ul>
<p>Should you wish to exercise any of these rights or have an enquiry or concern about our privacy
&amp; cookie policy, please contact us at hello@thehalalbutchery.com or you can write in to
</p>
<p><b>Customer Services</b></p>

<p><b>The Halal Butchery</b></p>
<p><b>No1 Spinninfields,</b></p>
<p><b>Quay Street,</b></p>
<p><b>Manchester,</b></p>
<p><b>M3 3JE</b></p>


	</div></div></div>
</div>
@endsection
<!-- /SECTION -->
