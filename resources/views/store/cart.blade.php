<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://cdn.jsdelivr.net/npm/jquery@3.6.1/dist/jquery.slim.min.js"></script>

<!-- Popper JS -->
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>


@extends('store.storeLayout')
@section('content')
    <script src="{{ asset('public/js/lib/jquery.js') }}"></script>
    <script src="{{ asset('public/js/dist/jquery.validate.js') }}"></script>
    <script src="{{ asset('public/js/postcoder-autocomplete.js') }}"></script>
    <link type="text/css" rel="stylesheet" href="{{ asset('public/css/style_for_quantity.css') }}" />
    <style>
        @media only screen and (min-width: 100px) and (max-width: 991px) {
            .billing-details {
                display: inline-block;

            }

            .col-md-12 {
                width: 100% !important;
                float: none;
            }

            #quantity {
                width: 50%;
            }

            .prod-image {
                min-height: 80px !important;
                min-width: 80px !important;
                border-radius: 20px;
                margin-left: 0px;
            }

            .prod-name {
                font-size: 16px;
            }

            .rTableCell button {
                border-radius: 100% !important;
                height: 20px !important;
                width: 20px !important;
                text-align: center;
                font-size: xx-small;
                padding: 0px !important;
            }
        }

        label.error {
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
            padding: 1px 20px 1px 20px;
        }

        .col-md-12 {

            float: none;
        }

        .col-sm-12 {
            padding-left: 0px;
            padding-right: 5px;
        }

        .prod-image {
            min-height: 100px;
            min-width: 100px;
            border-radius: 20px;
            margin-left: 0px;
        }

        .rTable {

            display: block;
            width: 100%;

        }

        .rTableHeading,
        .rTableBody,
        .rTableFoot,
        .rTableRow {
            clear: both;
        }

        .rTableHead,
        .rTableFoot {
            background-color: #DDD;
            font-weight: bold;

        }

        .rTableCell,
        .rTableHead {

            float: left;
            overflow: hidden;
            padding: 3px 0%;
            width: 20%;

        }

        .rTableCell {
            height: auto;
        }

        .rTableCell button {
            border-radius: 50px;
            height: 30px;
            width: 30px;
        }

        .rTable:after {
            visibility: hidden;
            display: block;
            font-size: 0;
            content: " ";
            clear: both;
            height: 0;
        }

        .order-summary {
            margin: 0px 0px;
        }

        .shipping {
            border-radius: 40px;
            /* background-color: gray;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           color: white;*/
        }

        .shipping:focus {
            background-color: #ad9802;
        }

        hr.horizental {
            border-top: 1px solid #ffffff00;
        }

        .order-summary .order-col {
            border-bottom: 1px solid #e6dede8c;
        }

        button {
            color: white;
        }

        #signupform {
            display: inline-block;

        }

        body {
            color: black;
        }

        .order-summary .order-col .order-total {

            color: #fff;
        }

        .order-details {
            border-radius: 20px;
            position: relative;
            background-color: #ffffffb8;
            padding: 0px;
            border-right: 0;
            border-left: 0;
            border-bottom: 0;
        }

        .rTableRow {
            display: flex;
            align-items: center;
        }
    </style>
    <?php $que = explode(',', Session::get('questions')); ?>
    <!-- SECTION -->
    <div class="section steps">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <!-- container -->
        <div class="container">

            <!-- Order Details -->
            <div class="col-md-12 col-sm-12 order-details" style="width: 100%;">

                <div id="order_summary" class="order-summary order-products">
                    @if ($all != null)
                        @foreach ($all as $c)
                            @foreach ($prod as $p)
                                @if ($c[0] == $p->id && $c[5] == $p->order)
                                    <div class="rTable col-md-12 col-sm-12"
                                        style="border-bottom: 3px solid #00000047; padding-bottom: 20px; padding-top: 20px;">
                                        <div class="rTableRow" id="deleteItem_{{ $c[5] }}">


                                            <div class="rTableCell">

                                                <img src="public/uploads/products/{{ $p->id }}/{{ $p->image_name }}"
                                                    alt="{{ $p->image_name }}" class="prod-image">


                                            </div>
                                            <div class="rTableCell" style="width: 30%">
                                                <h2 class="prod-name">{{ $p->name }}<span
                                                        style="font-size:10px;">{{ $p->tag }}</span></h2> <br>
                                                {{ $p->size }}
                                                @if (Session::has('questions'))
                                                    |
                                                    {{-- <span style="font-weight: bold;">{{$p['questions'][1]}}</span>
							<span>{{$p['questions'][2]}}</span> --}}

                                                    @foreach ($que as $ques)
                                                        <?php $q = explode(':', $ques); ?>
                                                        @if (!empty($q[3]))
                                                            @if ($p->order == $q[3])
                                                                {{-- <span style="font-weight: bold;">| {{$q[1]}} |</span> --}}
                                                                <span> {{ $q[2] }} </span><br>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </div>


                                            <!--quantity-->
                                            <!--c[1] is pid and c[3] is order serial-->
                                            <div class="rTableCell" style="width: 30%">
                                                <button type="button" id="sub" style="border: 1px solid;"
                                                    value={{ $p->id }} data-rel={{ $c[5] }}
                                                    data-rel2={{ $p->price }} data-rel3={{ $p->size }}
                                                    class="sub">-</button>
                                                <input type="number" id="quantity"
                                                    style=" border-radius: 40px; text-align: center;"
                                                    name={{ $p->id }} value={{ $c[1] }} min="1"
                                                    max="100" readonly />
                                                <button type="button" id="add" style="border: 1px solid;"
                                                    value={{ $p->id }} data-rel={{ $c[5] }}
                                                    data-rel2={{ $p->price }} data-rel3={{ $p->size }}
                                                    class="add">+</button>
                                            </div>


                                            <div class="rTableCell" hidden="hidden">
                                                <div
                                                    style="height:25px;width:25px;display:inline-block;background-color: #0000">
                                                </div>
                                            </div>

                                            <div class="rTableCell" style="width: 10%">
                                                <div id="individualPrice_{{ $c[5] }}">
                                                    £
                                                    @php
                                                        $tot = $p->price * $c[1];
                                                        echo number_format((float) $tot, 2, '.', '');
                                                    @endphp

                                                </div>
                                            </div>
                                            <div class="rTableCell" style="width: 10%">
                                                <button type="button" id="delete_item" value={{ $c[5] }}
                                                    name="delete_item" style="float: right;" class="delete_item">X</button>
                                            </div>
                                        </div>
                                    </div>
                                @break
                            @endif
                        @endforeach
                    @endforeach

            </div>
            <!-- Checkout and Delivery -->

            <form method="post" name="cart" class="col-md-6"
                style="float: right;border-left: 1px solid #e6dede8c;background: #ffffffb8; padding: 10px 5px;    border-radius: 40px;margin-top: 30px;line-height: 3; ">

                <!-- Order Summary End -->

                <!-- Address Collapse -->
                <div class="col-sm-12 col-md-12 " style="padding: 4px 10%;">
                    <button type="button" class="collapsible ">Shipping Address</button>
                    <input type="checkbox" name="sameAddress" id="same_address" checked="true"
                        onchange="shipping_address_change();"> Same as Primary Address?
                    <div class="content">
                        <!-- Address section -->

                        <div class="address-finder" id="address_finder">
                            <div class="form-group" style="text-align: center;">
                                <label for="postcoder_autocomplete" id="postcoder_autocomplete_label">Enter
                                    Address!</label>
                                <input id="postcoder_autocomplete" type="text" class="form-control input"
                                    onblur="check_postcode()">
                            </div>
                        </div>


                        <div class="form-group">
                            <input class="input shipping_address" type="text" name=" shipping_address" id="address"
                                placeholder="Address">
                        </div>
                        {!! $errors->first('address', '<label class="error">:message</label>') !!}
                        <div class="form-group">
                            <input class="input shipping_city" type="text" name="shipping_city" id="city"
                                placeholder="City">
                        </div>
                        {!! $errors->first('city', '<label class="error">:message</label>') !!}
                        <div class="form-group">
                            <input class="input shipping_postcode" type="text" name=" shipping_postcode"
                                style="text-transform:uppercase" id="postcode" onblur="check_postcode()"
                                placeholder="Post Code">
                        </div>
                        {!! $errors->first('post', '<label class="error">:message</label>') !!}
                        <!-- Address Section End -->
                    </div>
                </div>

                <!-- Payment Method -->
                <div class="col-sm-12 col-md-12 " style="padding: 4px 10%;">
                    <button type="button" class="collapsible ">Payment Method</button>
                    <span>
                        <input type="radio" name="payment_method" value="1" checked="checked"> Bank
                        Card</span><br>
                    <span>
                        <input type="radio" name="payment_method" value="2"> Cash on Delivery</span>

                </div>
                <!-- End Payment Method -->

                <div class="section-title text-center">
                    <h3 class="title" style="color: black">Your Bill</h3>
                </div>
                <div class="order-col row">
                    <div class="col-md-6 col-sm-6" style="text-align: left;">
                        <strong>Subtotal</strong>
                    </div>
                    <div class="col-md-6 col-sm-6" style="text-align: right;">£
                        <strong class="order-total" id="order_total">{{ Session::get('price') }} </strong>
                        <input type="text" name="sub_total" hidden id="sub_total"
                            value="{{ Session::get('price') }}">
                    </div>

                </div>

                <div class="order-col row">
                    <div class="col-md-3 col-sm-3" style="text-align: left;">
                        <strong> Delivery</strong>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <select name="shipping" onchange="shipping_prices();" id="shipping"
                            class="form-control shipping" style="width: 100%;">
                            @foreach ($shipping as $ship)
                                <option value="{{ $ship->id }}" id="{{ $ship->slug }}"
                                    data-id={{ $ship->id }}
                                    {{ $ship->id == 4 || ($ship->id == 5 && Session::get('price') < 40) ? 'disabled="true"' : '' }}
                                    data-price="{{ $ship->price }}">{{ $ship->type }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-3 col-sm-3" style="text-align: right;">£ <strong id="shipping_price_v"
                            class="order-total">0</strong></div>
                    <input type="text" class="form-control" name="shipping_price" hidden id="shipping_price"
                        value="">
                </div>

                <div class="order-col" id="dedicated_time">
                    <label for="dedicated_time">Enter Date & Time</label> <input type="datetime-local"
                        class="form-control" name="dedicated_time_i" id="dedicated_time_i"
                        placeholder="DD/MM - HH:MM" />

                </div>


                <div class="order-col row">
                    <div class="col-md-6 col-sm-6" style="text-align: left;"><strong>Total</strong></div>
                    <div class="col-md-6 col-sm-6" style="text-align: right;">£ <strong class="order-total"
                            id="totalCost"> </strong> </div>

                </div>

                <div class="order-col row">
                    <div class="col-md-6 col-sm-6" style="text-align: left;"><strong>Discount</strong></div>
                    <div class="col-md-6 col-sm-6" style="text-align: right;"><strong class="order-total"
                            id="totalDiscount"> </strong> </div>

                </div>

                <div class="order-col row" style="display: none;" id="a_d">
                    <div class="col-md-6 col-sm-6" style="text-align: left;"><strong style="color: green;">Promo
                            Accepted</strong></div>
                    <div class="col-md-6 col-sm-6" style="text-align: right;">£ <strong class="order-total"
                            id="after_discount"> </strong> </div>
                </div>

                <div class="order-col row">
                    <div class="col-md-4 col-sm-4" style="text-align: left;">
                        <strong>Promo</strong>
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <input type="text" name="promo" id="promo"
                            style="width:100%;height: 35px; padding: 10px; color: black;">
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <button type="button" style="border-radius: 30px;width: 75%;color: black;"
                            onclick="checkPromo()">Apply</button>
                    </div>

                </div>

                <div class="order-col">
                    <div style="width: 30%;"><strong>Notes</strong></div>
                    <div style="width: 100%;">
                        <textarea style="height:100px" class="form-control" name="notes" id="notes"></textarea>
                    </div>
                </div>

                @if (session('user'))
                    <div class="order-col" style="border-bottom: 0px">
                        {{ csrf_field() }}
                        <input type="submit" id="confirm_order" name="order" class="primary-btn order-submit"
                            value="Confirm order" style="float: right;">
                    </div>
                @else
                    <div class="order-col" style="border-bottom: 0px">
                        {{ csrf_field() }}

                        {{-- <h1>hello</h1> --}}
                        <!-- Button trigger modal -->
                        <button type="button" class="primary-btn order-submit" data-toggle="modal"
                            data-target="#mymodal" style="float: right; margin-left:2px;">
                            Login
                        </button>

                        <a href="{{ route('google-auth') }}" class="primary-btn order-submit" style="float: right;">
                            <i class="fa fa-google fa-fw" style="padding-right: 2rem"></i> Login with Google
                        </a>

                    </div>
                @endif



            </form>

            <!-- Modal -->
            <div class="modal" id="mymodal">
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Login</h5>
                            <button type="button" class="close" data-dismiss="modal"
                                style="background-color: white">
                                &times;</button>
                        </div>

                        <div class="modal-body">
                            <div class="backgroundimg">
                                <img src="{{ asset('public/images/slides/main.png') }}" class="modalbg"
                                    style="width: 100%;height:50%">
                            </div>
                            <form method="post" action="{{ route('user_login') }}" id="modalLoginForm">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <input class="input" type="email" id="email" name="email"
                                        class="form-control" style="border: 1px solid black; border-radius: 40px"
                                        placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                    <input class="input" type="password" name="pass" class="form-control"
                                        id="pass" style="border: 1px solid black; border-radius: 40px"
                                        placeholder="Password" required>
                                </div>


                                {{-- <div class="modal-footer"> --}}
                                <input type="submit" class="primary-btn order-submit"
                                    style="font-size: 17px; margin: 0px 2.5px 2.5px; padding: 5px 20px; width:100%"
                                    value="Login">

                                <button type="button" class="btn order-submit" data-dismiss="modal"
                                    style="font-size: 15px; margin:auto; width:30%; margin-top:1rem; background-color:silver; border-radius: 40px;">Close</button>

                                {{-- </div> --}}
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <style>
                /* .modal-content {
                    height: 46.5rem;
                    width: 100%;
                } */

                .modal-header {
                    background-color: rgba(0, 0, 0, 0.8);
                    padding: 16px 16px;
                    color: #FFF;
                    border-bottom: 2px solid #337AB7;
                }

                .modalbg {
                    height: 100%;
                    width: 100%;
                    padding-bottom: 2.5rem;
                }

                .modal-title {
                    width: 100%;
                    font-size: 2.5rem;
                    color: white;
                    text-align: center;
                    align-items: center;
                    justify-content: center;
                }
            </style>
        @else
            <div class="order-col">
                <h1>Your Cart is Empty</h1>
            </div>
            @endif

        </div>

        @if (session('user'))
            @if ($all != null)
            @else
                <a href="{{ route('user.home') }}"><input type="button" class="primary-btn order-submit"
                        value="Order Now"></a>
            @endif
        @endif




        <!-- /Order Details -->
    </div>
    <!-- /container -->

</div>

<!-- /SECTION -->

<script>
    $(document).ready(function() {
        // validate the comment form when it is submitted
        //$("#commentForm").validate();

        // validate signup form on keyup and submit
        $("#loginForm").validate({
            rules: {

                email: {
                    required: true,
                    email: true
                },
                pass: {
                    required: true,
                    minlength: 5
                }
            },
            messages: {

                email: "Please enter a valid email address",


                pass: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                }


            }
        });


    });


    $("#tel").keydown(function(e) {
        var oldvalue = $(this).val();
        var field = this;
        setTimeout(function() {
            if (field.value.indexOf('44') !== 0) {
                $(field).val(oldvalue);
            }
        }, 1);
    });

    function check_postcode() {
        var str = document.getElementById("postcode").value;
        str = str.toUpperCase();
        var n = str.startsWith("M") || str.startsWith("SK") || str.startsWith("WA") || str.startsWith("OL");
        if (n != true) {
            var str = document.getElementById("postcode").value = '';
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Invalid Postcode!',
                footer: 'We are serving only in Manchester for now!'
            })
        } else {
            IsPostcode(str);
        }
        //alert(n);
    }

    function IsPostcode(postcode) {
        postcode = postcode.toString().replace(/\s/g, "");
        // alert(postcode);
        var regex = /^[A-Z]{1,2}[0-9]{1,2} ?[0-9][A-Z]{2}$/i;
        //alert(regex.test(postcode));
        if (regex.test(postcode) != true) {
            var str = document.getElementById("postcode").value = '';
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Invalid Postcode!',
                footer: 'We are serving only in Manchester for now!'
            })

        }
    }
    //TO DO: ajax will take place

    $('.add').click(function() {

        var url = "{{ route('user.editCart') }}";
        var product_id = $(this).val();
        $(this).prev().val(+$(this).prev().val() + 1);
        var x = $(this).prev().val();
        var token = $("input[name=_token]").val();
        var order_serial = this.getAttribute('data-rel');
        var product_price = this.getAttribute('data-rel2');
        var size = this.getAttribute('data-rel3');
        var nPrice = x * product_price;


        $.ajax({
            type: 'post',
            url: url,
            dataType: "JSON",
            async: false,
            data: {
                pid: product_id,
                newQ: x,
                oSerial: order_serial,
                size: size,
                nPrice: nPrice,
                _token: token
            },
            success: function(msg) {
                document.getElementById("individualPrice_" + order_serial).innerHTML = " £" + (x *
                    product_price).toFixed(2);
                document.getElementById("order_total").innerHTML = (msg[2].toFixed(2));
                $('#sub_total').val(msg[2].toFixed(2));

                shipping_prices();
            }
        });


    });
    $('.sub').click(function() {

        var url = "{{ route('user.editCart') }}";
        var product_id = $(this).val();
        var order_serial = this.getAttribute('data-rel');
        var product_price = this.getAttribute('data-rel2');
        var size = this.getAttribute('data-rel3');
        var nPrice = x * product_price;

        if ($(this).next().val() > 1) {
            $(this).next().val(+$(this).next().val() - 1);
            var x = $(this).next().val();
            var token = $("input[name=_token]").val();


            $.ajax({
                type: 'post',
                url: url,
                dataType: "JSON",
                async: false,
                data: {
                    pid: product_id,
                    newQ: x,
                    oSerial: order_serial,
                    size: size,
                    nPrice: nPrice,
                    _token: token
                },
                success: function(msg) {
                    document.getElementById("individualPrice_" + order_serial).innerHTML = " £" + (
                        x * product_price).toFixed(2);
                    document.getElementById("order_total").innerHTML = (msg[2].toFixed(2));
                    $('#sub_total').val(msg[2].toFixed(2));
                    shipping_prices();

                }
            });


        }
    });

    $('.delete_item').click(function() {
        var url = "{{ route('user.deleteCartItem') }}";
        var serial = $(this).val(); //serial is the forth element of sale coloumn
        var token = $("input[name=_token]").val();
        var id_holder = "deleteItem_" + serial;
        $.ajax({
            type: 'post',
            url: url,
            dataType: "JSON",
            async: false,
            data: {
                serial: serial,
                _token: token
            },
            success: function(msg) {
                if (msg == "Empty") {
                    document.getElementById("order_summary").innerHTML =
                        "<div class='order-col'><h1>Your Cart is Empty</h1></div>";
                    document.getElementById("confirm_order").style.visibility = "hidden";
                }

                //$("#deleteItem_".$p->id").load(location.href+" #refresh_div","");
                document.getElementById(id_holder).innerHTML = "";
                document.getElementById("order_total").innerHTML = (msg[2].toFixed(2));
                $('#sub_total').val(msg[2].toFixed(2));
                shipping_prices();
            }
        });


    });





    //validation
    $(document).ready(function() {
        // validate the comment form when it is submitted
        //$("#commentForm").validate();

        // validate signup form on keyup and submit
        $("#signupForm").validate({
            rules: {
                name: "required",
                email: {
                    required: true,
                    email: true
                },
                address: "required",
                city: "required",
                postcode: {
                    required: true,
                },
                tel: "required",
                pass: {
                    required: true,
                    minlength: 5
                },
                confirm_password: {
                    required: true,
                    minlength: 5,
                    equalTo: "#pass"
                }



            },
            messages: {
                name: "Please enter your Fullname",
                email: "Please enter a valid email address",
                address: "Please enter your Address",
                city: "Please enter your City",
                address: "Please enter your Address",
                postcode: {
                    required: "Please enter Post code",
                },
                tel: "Please enter your Phone number",
                pass: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long"
                },
                confirm_password: {
                    required: "Please provide a password",
                    minlength: "Your password must be at least 5 characters long",
                    equalTo: "Please enter the same password as above"
                }


            }



        });


    });


    function myFunction() {
        //var token={{ csrf_token() }};
        var email = $("#email").val();
        var token = $("input[name=_token]").val();
        var url = "{{ route('user.signup.check_email') }}";


        $.ajax({
            type: 'post',
            url: url,
            dataType: "JSON",
            async: false,
            data: {
                email: email,
                _token: token
            },
            success: function(msg) {


                if (msg == "1") {
                    document.getElementById("for_duplicate-email").innerHTML =
                        "<label class='error'>This Email Address is Already taken</label>";


                } else {
                    document.getElementById("for_duplicate-email").innerHTML = "";

                }
            }
        });

    }

    function shipping_prices() {
        var price = $("#shipping").find(':selected').data('price');
        //alert(price);
        $('#shipping_price_v').html(price);
        $('#shipping_price').val(price);
        var sub_total = $('#sub_total').val();

        var sv = $('#shipping').val();
        if (sv == 2) {
            $('#dedicated_time').fadeIn("slow");
        } else {
            $('#dedicated_time').fadeOut("slow");
        }
        if ($('#sub_total').val() > 39.99) {
            $('#NDA').prop('disabled', false);
            $('#FATT').prop('disabled', false);

        } else {
            $('#NDA').prop('disabled', true);
            $('#FATT').prop('disabled', true);
        }
        $('#totalCost').html((parseFloat(price) + parseFloat(sub_total)).toFixed(2));
        checkPromo();

    }

    shipping_prices();

    function set_date() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }

        today = yyyy + '-' + mm + '-' + dd + 'T15:30';
        document.getElementById("dedicated_time_i").setAttribute("min", today);
    }
    set_date();

    $('#dedicated_time_i').change(function() {

        var datetime = $("#dedicated_time_i").val();
        var today = new Date().addHours(3);
        var time = today.toLocaleTimeString();
        Swal.fire({
            title: 'Your Order Will take atleast 3 Hours To deliver!',
            showClass: {
                popup: 'animate__animated animate__fadeInDown'
            },
            hideClass: {
                popup: 'animate__animated animate__fadeOutUp'
            }
        })

    });

    Date.prototype.addHours = function(h) {
        var t = this.setTime(this.getTime() + (h * 60 * 60 * 1000));
        return this;
    }

    function checkPromo() {
        var delivery_charges = $('#shipping_price').val();

        var promo = $('#promo').val();
        if (promo != '') {
            var token = $("input[name=_token]").val();

            var url = "{{ route('promo.check') }}";
            $.ajax({
                type: 'post',
                url: url,
                dataType: "JSON",
                async: false,
                data: {
                    promo: promo,
                    _token: token
                },
                success: function(response) {
                    if (response != null) {
                        var discount = response.value;

                        var promo_type = response.type_id;
                        var tc = $("#totalCost").html();

                        if (promo_type == 1) {
                            var disc = (parseFloat(discount) / 100) * parseFloat(tc);
                            tc = parseFloat(tc) - parseFloat(disc);
                            tc = tc.toFixed(2);
                            $('#totalDiscount').html(discount + "%");
                        } else if (promo_type == 2) {
                            var id = $("#shipping").find(':selected').data('id');
                            if (id == "1" || id == "2") {
                                var price = $("#shipping").find(':selected').data('price');
                                var sub_total = $('#sub_total').val();
                                $('#totalCost').html((parseFloat(price) + parseFloat(sub_total)).toFixed(
                                    2));
                                $('#totalCost').css('text-decoration', '');
                                $('#promo').css('border', '2px solid red');
                                $('#totalDiscount').html(0);
                                $('#a_d').hide();
                                return;
                            }
                            tc = parseFloat(tc) - parseFloat(delivery_charges);
                            tc = tc.toFixed(2);
                            $('#totalDiscount').html("£ " + delivery_charges);
                        } else if (promo_type == 3) {
                            var disc = (parseFloat(discount));
                            tc = parseFloat(tc) - parseFloat(disc);
                            tc = tc.toFixed(2);
                            $('#totalDiscount').html("£" + discount);
                        } else if (promo_type == 4) {
                            var id = $("#shipping").find(':selected').data('id');
                            if (id == "1") {
                                tc = parseFloat(tc) - parseFloat(delivery_charges);
                                tc = tc.toFixed(2);
                                $('#totalDiscount').html("£ " + delivery_charges);
                            } else {
                                var price = $("#shipping").find(':selected').data('price');
                                var sub_total = $('#sub_total').val();
                                $('#totalCost').html((parseFloat(price) + parseFloat(sub_total)).toFixed(
                                    2));
                                $('#totalCost').css('text-decoration', '');
                                $('#promo').css('border', '2px solid red');
                                $('#totalDiscount').html(0);
                                $('#a_d').hide();
                                return;
                            }

                        } else if (promo_type == 5) {
                            var id = $("#shipping").find(':selected').data('id');
                            if (id == "1" && tc > 49.99) {
                                var td = parseFloat(delivery_charges) + parseFloat(discount);
                                tc = parseFloat(tc) - td;
                                tc = tc.toFixed(2);
                                $('#totalDiscount').html("£ " + td.toFixed(2));
                            } else {
                                var price = $("#shipping").find(':selected').data('price');
                                var sub_total = $('#sub_total').val();
                                $('#totalCost').html((parseFloat(price) + parseFloat(sub_total)).toFixed(
                                    2));
                                $('#totalCost').css('text-decoration', '');
                                $('#promo').css('border', '2px solid red');
                                $('#totalDiscount').html(0);
                                $('#a_d').hide();
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'Invalid Promocode!',
                                    footer: 'Please check delivery option or Promocode!'
                                })
                                return;
                            }

                        }

                        $('#totalCost').css('text-decoration', 'line-through');
                        $('#after_discount').html(tc);
                        $('#promo').css('border', '1px solid black');
                        $('#a_d').show();
                    } else {

                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'Invalid Promocode!',
                            footer: 'Please check delivery option or Promocode!'
                        })
                        var price = $("#shipping").find(':selected').data('price');
                        var sub_total = $('#sub_total').val();
                        $('#totalCost').html((parseFloat(price) + parseFloat(sub_total)).toFixed(2));
                        $('#totalCost').css('text-decoration', '');
                        $('#promo').css('border', '2px solid red');
                        $('#totalDiscount').html(0);
                        $('#a_d').hide();
                    }
                }

            });
        } else {

            var price = $("#shipping").find(':selected').data('price');
            var sub_total = $('#sub_total').val();
            $('#totalCost').html((parseFloat(price) + parseFloat(sub_total)).toFixed(2));
            $('#totalCost').css('text-decoration', '');
            $('#totalDiscount').html(0);
            $('#promo').css('border', '1px solid black');
            $('#a_d').hide();

        }
    }
    /*Collapse*/
    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
        coll[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var content = this.nextElementSibling;
            if (content.style.display === "block") {
                content.style.display = "none";
            } else {
                content.style.display = "block";
            }
        });
    }

    // Chicking shipping address
    shipping_address_change();

    function shipping_address_change() {
        if ($('#same_address').is(':checked')) {
            $(".shipping_postcode").val('');
            $(".shipping_address").val('');
            $(".shipping_city").val('');
        }
        $(".content").toggle(1000);
    }
</script>
<!-- Postcoder -->
<script type="text/javascript">
    // Choose the element we will use as the search box
    var autocomplete_input = document.getElementById("postcoder_autocomplete");
    var autocomplete_label = document.getElementById("postcoder_autocomplete_label");
    var autocomplete_wrapper = document.getElementById("address_finder");

    // Attach autocomplete to search box, with our settings
    // To get your free trial API key visit https://www.alliescomputing.com/postcoder/sign-up
    var postcoder_complete = new AlliesComplete(autocomplete_input, {
        apiKey: "PCWZL-J8MCL-3VZBS-F9RP3", // Change this to your own API key
        autocompleteLabel: autocomplete_label,
        autocompleteWrapper: autocomplete_wrapper
    });

    // This event is fired by library when user selects an item in the list
    autocomplete_input.addEventListener("postcoder-complete-selectcomplete", function(e) {

        auto_address_select(e.address);

    });

    // Demo function to populate form fields with address fields from chosen address
    function auto_address_select(the_address) {


        document.getElementById("address").value = the_address.addressline1 + " " + the_address.addressline2 || "";
        // document.getElementById("address_line_2_auto").value = the_address.addressline2 || "";
        document.getElementById("city").value = the_address.posttown;

        document.getElementById("postcode").value = the_address.postcode;

        autocomplete_input.value = "";
        autocomplete_input.blur();

        return true;

    }
</script>
@endsection
