@extends('store.storeLayout')
@section('content')
    <script src="{{ asset('public/js/lib/jquery.js') }}"></script>
    <script src="{{ asset('public/js/dist/jquery.validate.js') }}"></script>
    <script src="{{ asset('public/js/postcoder-autocomplete.js') }}"></script>

    <style>
        label.error {
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
            padding: 1px 20px 1px 20px;
        }
    </style>
    <!-- SECTION -->
    <div class="section steps">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row"
                style="display: flex;flex-wrap: wrap; background:#ffffffe0; border-radius:150px 0px 150px 0px;">
                <div class="col-md-6"
                    style="background: white;padding-left: 0px;padding-right: 0px;background: black;border-radius: 150px 0px 0px 0px;">
                    <img src="{{ asset('public/images/slides/main.png') }}" alt="Premium BBQ Box"
                        style="width: 100%; border-radius: 150px 0px 0px 0px; ">
                </div>
                <div class="col-md-6">
                    <form id="signupForm" method="post" action="{{ route('updateProfile') }}">
                        {{ csrf_field() }}

                        <!-- Billing Details -->
                        <div class="billing-details">
                            <div class="section-title">
                                <h1 class="title" style="font-size: 33px;color: #2b2d42;">Edit Profile</h1>
                            </div>

                            <div class="form-group ">
                                <input class="input" type="text" name="name" id="name"
                                    value="{{ $user->full_name }}" placeholder="Full Name">
                            </div>
                            {!! $errors->first('name', '<label class="error">:message</label>') !!}

                            <div class="form-group">
                                <input class="input" type="email" name="email" id="email" placeholder="Email"
                                    readonly="true" value="{{ $user->email }}" onkeypress="check_email()">
                            </div>
                            <div id="for_duplicate-email"></div>
                            {!! $errors->first('email', '<label class="error">:message</label>') !!}

                            <div class="address-finder" id="address_finder">
                                <div class="form-group">
                                    <label for="postcoder_autocomplete" id="postcoder_autocomplete_label">Type your address
                                        or postcode</label>
                                    <input id="postcoder_autocomplete" type="text" class="form-control input">
                                </div>
                            </div>
                            <div class="form-group">
                                <input class="input" type="text" name="address" id="address"
                                    value="{{ $user->addresses->area }}" placeholder="Address" readonly>
                            </div>
                            {!! $errors->first('address', '<label class="error">:message</label>') !!}
                            <div class="form-group">
                                <input class="input" type="text" name="city" id="city"
                                    value="{{ $user->addresses->city }}" placeholder="City" readonly>
                            </div>
                            {!! $errors->first('city', '<label class="error">:message</label>') !!}
                            <div class="form-group">
                                <input class="input" type="text" name="postcode" style="text-transform:uppercase"
                                    id="postcode" value="{{ $user->addresses->postcode }}" onblur="check_postcode()"
                                    placeholder="Post Code" readonly>
                            </div>
                            {!! $errors->first('post', '<label class="error">:message</label>') !!}
                            <div class="form-group">
                                <input class="input" type="tel" name="tel" id="tel" placeholder="Mobile"
                                    value="{{ $user->phone }}">
                            </div>
                            {!! $errors->first('tel', '<label class="error">:message</label>') !!}
                            <div class="form-group">
                                <input class="input" type="password" name="pass" id="pass"
                                    value="{{ $user->password }}" placeholder="Enter Your Password">
                            </div>
                            {!! $errors->first('pass', '<label class="error">:message</label>') !!}
                            <div class="form-group">
                                <div style="float:left;    margin-top: -10px;">
                                    <input type="checkbox" onclick="showPassword()">&nbsp&nbsp&nbsp<label
                                        style="margin-top: 3px;color: white;">Show Password</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <input class="input" type="password" name="confirm_password" id="confirm_password"
                                    value="{{ $user->password }}" placeholder="Confirm Password">
                            </div>
                            {!! $errors->first('confirm_password', '<label class="error">:message</label>') !!}


                            <br>

                            <input type="submit" name="signup" class="primary-btn order-submit" value="Update Profile">
                    </form>


                </div>
                <!-- /Billing Details -->
            </div>

        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
    </div>

    <!--JQUERY Validation-->
    <script>
        $(document).ready(function() {
            // validate the comment form when it is submitted
            //$("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#signupForm").validate({
                rules: {
                    name: "required",
                    email: {
                        required: true,
                        email: true
                    },
                    address: "required",
                    city: "required",
                    postcode: {
                        required: true,
                    },
                    tel: "required",
                    pass: {
                        required: true,
                        minlength: 5
                    },
                    confirm_password: {
                        required: true,
                        minlength: 5,
                        equalTo: "#pass"
                    }



                },
                messages: {
                    name: "Please enter your Fullname",
                    email: "Please enter a valid email address",
                    address: "Please enter your Address",
                    city: "Please enter your City",
                    address: "Please enter your Address",
                    postcode: {
                        required: "Please enter Post code",
                    },
                    tel: "Please enter your Phone number",
                    pass: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 5 characters long"
                    },
                    confirm_password: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 5 characters long",
                        equalTo: "Please enter the same password as above"
                    }


                }



            });


        });

        function check_postcode() {
            var str = document.getElementById("postcode").value;
            str = str.toUpperCase();
            var n = str.startsWith("M") || str.startsWith("SK") || str.startsWith("WA");
            if (n != true) {
                var str = document.getElementById("postcode").value = '';
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Invalid Postcode!',
                    footer: 'We are serving only in Manchester for now!'
                })
            } else {
                IsPostcode(str);
            }
            //alert(n);
        }

        function IsPostcode(postcode) {
            postcode = postcode.toString().replace(/\s/g, "");
            // alert(postcode);
            var regex = /^[A-Z]{1,2}[0-9]{1,2} ?[0-9][A-Z]{2}$/i;
            //alert(regex.test(postcode));
            if (regex.test(postcode) != true) {
                var str = document.getElementById("postcode").value = '';
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Invalid Postcode!',
                    footer: 'We are serving only in Manchester for now!'
                })

            }
        }
        $("#tel").keydown(function(e) {
            var oldvalue = $(this).val();
            var field = this;
            setTimeout(function() {
                if (field.value.indexOf('') !== 0) {
                    $(field).val(oldvalue);
                }
            }, 1);
        });

        function check_email() {

            //var token={{ csrf_token() }};
            var email = $("#email").val();
            var token = $("input[name=_token]").val();
            var url = "{{ route('user.signup.check_email') }}";

            $.ajax({
                type: 'post',
                url: url,
                dataType: "JSON",
                async: false,
                data: {
                    email: email,
                    _token: token
                },
                success: function(msg) {


                    if (msg == "1") {
                        document.getElementById("for_duplicate-email").innerHTML =
                            "<label class='error'>This Email Address is Already taken</label>";
                    } else {
                        document.getElementById("for_duplicate-email").innerHTML = "";
                    }
                }
            });

        }

        function showPassword() {
            var x = document.getElementById("pass");
            var y = document.getElementById("confirm_password");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }

            if (y.type === "password") {
                y.type = "text";
            } else {
                y.type = "password";
            }
        }
    </script>
    <!--/Duplicate Email Validation-->
    <!-- Postcoder -->
    <script type="text/javascript">
        // Choose the element we will use as the search box
        var autocomplete_input = document.getElementById("postcoder_autocomplete");
        var autocomplete_label = document.getElementById("postcoder_autocomplete_label");
        var autocomplete_wrapper = document.getElementById("address_finder");

        // Attach autocomplete to search box, with our settings
        // To get your free trial API key visit https://www.alliescomputing.com/postcoder/sign-up
        var postcoder_complete = new AlliesComplete(autocomplete_input, {
            apiKey: "PCWZL-J8MCL-3VZBS-F9RP3", // Change this to your own API key
            autocompleteLabel: autocomplete_label,
            autocompleteWrapper: autocomplete_wrapper
        });

        // This event is fired by library when user selects an item in the list
        autocomplete_input.addEventListener("postcoder-complete-selectcomplete", function(e) {

            auto_address_select(e.address);

        });

        // Demo function to populate form fields with address fields from chosen address
        function auto_address_select(the_address) {


            document.getElementById("address").value = the_address.addressline1 + " " + the_address.addressline2 || "";
            // document.getElementById("address_line_2_auto").value = the_address.addressline2 || "";
            document.getElementById("city").value = the_address.posttown;

            document.getElementById("postcode").value = the_address.postcode;

            autocomplete_input.value = "";
            autocomplete_input.blur();
            check_postcode();
            return true;

        }
    </script>
    <!-- /SECTION -->
@endsection
