@extends('store.storeLayout')
@section('content')
<head><title>THB Site Map</title></head>

{{ <?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>}}
<style>
	.col-md-4 {
		text-align: left;
	}
</style>

<!-- SECTION -->
<div class="section">
	<div class="container">
		<div class="row" style="margin-left: 10px; border-bottom: 1px solid #9c9c9c73; margin-bottom: 50px">
			<div class="col-md-4">
			<h2>THB Site Map</h2>				
			</div>
		</div>
		<div class="row">
		
		
		
		<div class="col-md-4">
			<h3>Services</h3>
			  
                            <urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">
                                <url>
									<loc href="{{route('aboutUs')}}"> 
										<p><a href="{{route('aboutUs')}}">
											About Us
										</a></p>
									</loc>
								</url>
                                <url><loc href="{{route('deliveryProcess')}}">
										<p><a href="{{route('deliveryProcess')}}">
											Delivery Process
										</a>
										</p>
									</loc>
								</url>
                                <url>
									<loc href="{{route('privacyPolicy')}}"> 
									 <p>
										<a href="{{route('privacyPolicy')}}">
											Privacy Policy
										</a>
									 </p>
									</loc>
								</url>
                                <url>
									<loc href="{{route('refundPolicy')}}">
										<a href="{{route('refundPolicy')}}"> 
											Refund Policy
										</a>
									</loc>
								</url>
                                <url>
									<loc href="{{route('termsConditions')}}"> 
										<p><a href="{{route('termsConditions')}}">
											Terms & Conditions
										</a>
									</p>
										</loc>
									</url>
							</urlset>
               
		</div>
		<div class="col-md-4">
			<h3>Catagories</h3>
			  		<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">  
                        @foreach($cat as $c)
                        
						<url>
							<loc>		
								https://thehalalbutchery.com/searchCategory/{{ $c->slug }}
							</loc>
						</url>
                        @endforeach
                        <url>
							<loc>
								https://thehalalbutchery.com/searchCategory/search
							</loc>
						</url>
					</urlset>          
              
		</div>

		<div class="col-md-4">
			<h3>Products</h3>
			  		<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">        
                        @foreach($products as $p)
                        <url >
							<loc>
								https://thehalalbutchery.com/productName/{{ $p->slug }}
								<br>
							</loc>
						</url>
                        @endforeach
					</urlset>
		</div>

		</div>
	</div>
</div>
@endsection
<!-- /SECTION -->
