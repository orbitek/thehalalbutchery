@extends('store.storeLayout')
@section('content')
    <style type="text/css">
        .img-container {
            position: relative;
            text-align: center;
            color: white;
        }

        /* Bottom left text */
        .bottom-left {
            position: absolute;
            bottom: 8px;
            left: 16px;
        }

        /* Top left text */
        .top-left {
            position: absolute;
            top: 8px;
            left: 16px;
        }

        /* Top right text */
        .top-right {
            position: absolute;
            top: 8px;
            right: 16px;
        }
    </style>
    <div class="section steps">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">

                <!-- section title -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h1 class="title">Trending Now</h1>

                    </div>
                </div>
                <!-- /section title -->
                <!-- Products tab & slick -->
                <div class="col-md-12">
                    <div class="row">
                        @foreach ($products as $product)
                            <!-- product -->
                            <div class="col-md-4">
                                <div class="product">
                                    <div class="product-img">
                                        <a href="{{ route('product.viewByName', ['slug' => $product->slug]) }}">
                                            <div class="img-container">
                                                <img class="img-size"
                                                    src="public/uploads/products/{{ $product->id }}/{{ $product->image_name }}"
                                                    alt="{{ $product->name }}">
                                        </a>

                                        <div class="top-right"
                                            style="width: 50%;height: 15%; background: #bea004;border-radius: 40px;display: flex;align-items: center;justify-content: center;">
                                            <strong> {{ mt_rand(20, 100) }} </strong> &nbsp Sold in Last 24 Hours
                                        </div>
                                    </div>
                                    <div class="product-label">

                                        <span class="new" style="display: none;">
                                            {{ $product->active == 1 ? $product->tag : 'Out Of Stock' }}</span>
                                        <span class="sale" style="display: none;">£ {{ $product->discount }}</span>
                                    </div>
                                </div>
                                <div class="product-body">
                                    <p class="product-category" style="display:none;">{{ $product->category->name }}</p>

                                    <h3 class="product-name">
                                        <a href="{{ route('product.viewByName', ['slug' => $product->slug]) }}"
                                            style="font-size: 20px; color: #BEA004">{{ $product->name }}<span
                                                style="font-size:10px;">{{ $product->tag }}</span></a>
                                    </h3>
                                    <span style="display: none">
                                        {{ $p = \App\Variation::where(['product_id' => $product->id])->min('price') }}</span>
                                    <div style="display:flex;align-items: flex-end;">
                                        <h6 style="margin-right: 10px">From</h6>
                                        <h4
                                            style="margin-right:20px;text-decoration: line-through;text-decoration-color: red;color: red;margin-right:10px; display:none;">
                                            £ {{ round($p + $p * (20 / 100), 2) }}</h4>
                                        <h4 class="product-price">£ {{ $p }}</h4>
                                    </div>
                                    @if ($product->active == 1)
                                        <div>
                                            <form action="{{ route('product.viewByName', ['slug' => $product->slug]) }}"
                                                style="    display: flex;margin: 0 auto;align-items: center;"
                                                class="row">

                                                <button class="add-to-cart-btn carousel_order_now col-xs-8" type="submit"
                                                    style="width: 80%; border: 0px;"><i class="fa fa-shopping-cart"
                                                        style="margin-right:15px"></i> <span>Shop Now</span></button>
                                                <!-- <i class="fa fa-heart col-xs-2 favourite"  id="{{ $product->id }}"  onclick="favourite(this.id)" style="font-size: 35px; text-align: right; padding-right: 0px;"></i> -->
                                            </form>

                                        </div>
                                    @endif
                                    @if ($product->active == 0)
                                        <div>
                                            <button clbuttonss="add-to-cart-btn carousel_order_now"
                                                style="background-color: #ab1c05;"><i class="fa fa-shopping-cart"></i>Out of
                                                Stock</button>
                                            <!-- <i class="fa fa-heart col-xs-2 favourite" id="{{ $product->id }}" onclick="favourite(this.id)" style="font-size: 35px; text-align: right; padding-right: 0px;"></i> -->
                                        </div>
                                    @endif
                                    @if ($product->active == 2)
                                        <div>
                                            <button class="add-to-cart-btn carousel_order_now"><i
                                                    class="fa fa-shopping-cart"></i>Coming Soon</button>
                                        </div>
                                    @endif
                                </div>
                            </div>
                    </div>
                    <!-- /product -->
                    @endforeach
                </div>
            </div>

            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    </div>
@endsection
