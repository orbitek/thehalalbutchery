@extends('store.storeLayout')
@section('content')
    <script src="{{ asset('public/js/lib/jquery.js') }}"></script>
    <script src="{{ asset('public/js/dist/jquery.validate.js') }}"></script>

    <style>
        label.error {
            color: #a94442;
            background-color: #f2dede;
            border-color: #ebccd1;
            padding: 1px 20px 1px 20px;
        }

        #loginForm {
            margin-left: auto;
            margin-right: auto;
        }
    </style>
    <!-- SECTION -->
    <div class="section steps">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row"
                style="display: flex;flex-wrap: wrap; background:#ffffffe0;; border-radius:150px 0px 150px 0px;">
                <div class="col-md-6"
                    style="background: white;padding-left: 0px;padding-right: 0px;background: black;border-radius: 150px 0px 0px 0px;">
                    <img src="{{ asset('public/images/slides/main.png') }}" alt="Premium BBQ Box"
                        style="width: 100%; border-radius: 150px 0px 0px 0px; ">
                </div>

                <div class="col-md-6">

                    <form method="post" id="loginForm">
                        {{ csrf_field() }}
                        <div class="">
                            <!-- Billing Details -->
                            <div class="billing-details">
                                <div class="section-title">
                                    <h3 class="title" style="font-size: 33px;color: #2b2d42;">User Login</h3>
                                </div>
                                <div class="form-group">
                                    <input class="input" type="email" name="email" id="email" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <input class="input" type="password" name="pass" id="pass"
                                        placeholder="Password" placeholder="Password">
                                </div>
                                <br>
                                <input type="submit" name="signin" class="primary-btn order-submit"
                                    style="width: 75%;margin-bottom:22px ;" value="Sign In">


                                <a href="{{ route('google-auth') }}" class="primary-btn order-submit"
                                    style="width: 75%;margin-bottom:22px ;">
                                    <i class="fa fa-google" style="padding-right:2rem"></i>
                                    Login with Google
                                </a>


                                <div class="alreadyin">
                                    <span class="" style="font-size:18px;font-weight: 300;margin-bottom: 14px">New
                                        to
                                        THB? <a href="{{ route('user.signup') }}" style="color:#1EE92C;font-weight: 300">
                                            Sign Up </a> </span>
                                </div>

                                <div class="alreadyin">
                                    <span class="" style="font-size:18px;font-weight: 300; "> <a
                                            href="{{ route('forgetPassword') }}"
                                            style="font-weight: 300;color: #BEA004">Forgotten Password? </a></span>
                                </div>
                    </form>

                </div>

            </div>
            <!-- /Billing Details -->
        </div>

    </div>
    <!-- /row -->
    </div>
    <!-- /container -->
    </div>
    <!--JQUERY Validation-->
    <script>
        $(document).ready(function() {
            // validate the comment form when it is submitted
            //$("#commentForm").validate();

            // validate signup form on keyup and submit
            $("#loginForm").validate({
                rules: {

                    email: {
                        required: true,
                        email: true
                    },
                    pass: {
                        required: true,
                        minlength: 5
                    }
                },
                messages: {

                    email: "Please enter a valid email address",


                    pass: {
                        required: "Please provide a password",
                        minlength: "Your password must be at least 5 characters long"
                    }


                }
            });


        });
    </script>
    <!--/JQUERY Validation-->
    <!-- /SECTION -->
@endsection
