@extends('store.storeLayout') 
@section('content')
<style type="text/css">
	.img-container {
  position: relative;
  text-align: center;
  color: white;
}

/* Bottom left text */
.bottom-left {
  position: absolute;
  bottom: 8px;
  left: 16px;
}

/* Top left text */
.top-left {
  position: absolute;
  top: 8px;
  left: 16px;
}

/* Top right text */
.top-right {
  position: absolute;
  top: 8px;
  right: 16px;
}

</style>
<div class="section steps">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">

			<!-- Products tab & slick -->
			<div class="col-md-12">
				<div class="row">
					@foreach($blogs as $blog)
					<!-- product -->
					<div class="col-md-4">
						<div class="product">
							<div class="product-img">
								<a href="{{route('blog.view')}}"> 
									 <div class="img-container">
									<img class="img-size" src="public/uploads/products/{{$blog->id}}/{{$blog->photo}}" alt="{{$blog->title}}">{{$blog->title}}</a>
                                      
                                      <div class="top-right" style="width: 50%;height: 15%; background: #bea004;border-radius: 40px;display: flex;align-items: center;justify-content: center;" > <strong> {{$blog->created_at}} </strong> </div>
                                    </div>
								
							</div>
							<div class="product-body">
								
								<h3 class="product-name" ><a href="{{route('blog.view',['id'=>$blog->id])}}" style="font-size: 20px; color: #BEA004">{{$blog->title}}</a></h3>
								
							   
							   <div >
								<form action="{{route('blog.view',['id'=>$blog->id])}}" style="    display: flex;margin: 0 auto;align-items: center;" class="row">

									<button class="add-to-cart-btn carousel_order_now col-xs-8" type="submit" style="width: 80%; border: 0px;" ><i class="fa fa-shopping-cart" style="margin-right:15px"></i> <span>Read Now</span></button>
								</form>

							</div>
							
											
						</div>
					</div>
				</div>
				<!-- /product -->
				@endforeach
				</div>
			</div>
		
		<!-- /row -->
		</div>
	<!-- /container -->
	</div>
</div>
@endsection