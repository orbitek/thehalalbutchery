@extends('store.storeLayout')
@section('content')
<style type="text/css">
	
</style>
<!-- SECTION -->
<div class="section">
	<div class="container">
		<div class="row">
		<div class="col-md-1"></div>		
		<div class="col-md-10" style="padding-right: 20px;padding-left: 20px; text-align: left">
		<h1>Terms and Conditions</h1>
		At The Halal Butchery, we aim to give you a seamless online meat shopping experience. Please
ensure that you have read the following terms and conditions prior to using The Halal Butchery;
they set out what you can expect from us and the terms on which you agree to be bound to as
and when you place your order.
<h5>BECOMING A CUSTOMER</h5> 
<p>
1.1 To place an order with us, you must be over 16 years of age, require delivery in our coverage
area, and have a credit or debit card acceptable to us in your name.<br>
1.2 We reserve the right to decline any order or to suspend your Account with us at our
discretion.<br>
1.3 Our terms and conditions will apply when you place your order.
</p>
<h5>YOUR ACCOUNT</h5>
<p>
2.1 If you wish, you may set up an online account with us to make each visit to our website
easier. If you do this, you will be given a username and a password to use each time you place
an order with us.<br>
2.2 You are responsible for all activity on your account. You should ensure that your username
and password are known only to you and that access to your computer is restricted to prevent
any unauthorised use of your account. You must inform us immediately if you believe your
password has become known to anybody else or has been or may be used in an unauthorised
manner.<br>
2.3 Please ensure that the information you provide us with is correct and up to date and notify us
as soon as possible of any changes.<br>
2.4 Please ensure that you have read our statement on our privacy policy.<br>
2.5 We store your personal information as long as you remain registered customer on the
website.<br></p>
<h5> CONTRACT</h5>
<p>
3.1 This website is operated by The Halal Butchery. Once you have placed an order on the
website, we will confirm your order by sending an email to the email address you have provided.
This email will list all products you have ordered, the price and delivery details. This is not an
acceptance by us. A legally binding contract will only arise once we have completed delivery of
the goods to you.<br>
3.2 If you have entered an incorrect email address and receive no email purchase confirmation
and/or any other details are incorrect then the onus is on you to contact us and amend your
details. We cannot be held responsible for the entering of incorrect information or your email
service provider spam filtration.<br>
</p>
<h5> AMENDING OR CANCELLING THE CONTRACT</h5>
<p>
4.1 As we dispatch your items same day or next day dependant on when you place your order,
we give our customers a 1 hour window from the time the order is placed to either amend or
cancel your order.<br>
4.2 Should you wish to cancel your order after the cut off time but prior to delivery, we may
charge you in full for an amount equal to the value of your order, including any delivery cost.<br>
4.3 Any cancellation must be sent to us by email at liveorders@thehalalbutchery.com within the
1 hour cut-off period with your order number and delivery address.<br>
4.4 In the instance that you have ordered goods which are unavailable for any reason, we shall
notify you and we will either send a replacement product with your order, or organise a full refund
or voucher.<br>
</p>
<h5> PRICE AND DELIVERY CHARGES</h5>
<p>5.1 The price of the goods will be as quoted on the website at the time you confirm your order by
clicking “Checkout” and will be inclusive of VAT (if applicable) or any other tax and, subject to
paragraphs 5.2 and 6 below, delivery.
<br>5.2 &quot;No delivery charge shall be applied to orders with a value above £40 on next day delivery;
orders placed to be received within 3 Hours, Anytime Today and Designated Time &amp; Day shall be
subject to a delivery charge of £6.99, £4.99 &amp; £9.99.&quot;<br>
5.3 If you amend your order prior to the cut off time to increase the quantity of items already
ordered, the price charged for the additional items will be the price quoted at the time you made
the original order. If, however, you are adding new items to your order, the price charged will be
the price quoted at the time you amend the order. The delivery charge shall be amended and
applied in accordance with paragraph 5.2.<br>

5.4 We will debit the total cost of your order, including any delivery charge or packaging cost that
may be applied, from your debit or credit card at the time you place your order.<br></p>

<h5> DELIVERY</h5>
<p>
6.1 We will always aim to deliver your order within the stated timeframe given to you when
placing your order.<br>
6.2 As the items we supply are Perishable, the responsibility lies with the customer to ensure that
the parcel is accepted on the day of delivery. If an attempt was made to deliver and failed due to
no one being present at the primary delivery address, then the responsibility and liability for the
loss of goods lies with the customer, in this respect no claim can be made against The Halal
Butchery.<br>
6.3 If nobody is available at the primary delivery address (nominated by you at the time you place
your order) to accept delivery of the order, we will attempt to call you on the number provided. If
the call is unanswered, we will leave your goods unattended at a secure place (E.g porch, shed
or neighbour). If these options are not available, we will place a re-order once you have been in
contact. There will be a £4.99 fee for any orders which have been unsuccessful in being retrieved
and require a re-order.<br>
6.4 Our deliveries are completed by a photo being taken of the package arriving at the delivery
address. In the event that no one is available to take delivery of the order at your primary delivery
address or you ask us to leave your goods unattended at a secure place, The Halal Bucthery
expressly disclaims all liability that may arise in consequence of the delivery being left
unattended, including, without limitation, theft, tampering or contamination, however caused.
<br>6.5 If your delivery is refused or not accepted at a recipient&#39;s address, we cannot take
responsibility for this occurrence and cannot offer a free replacement box.<br>
6.6 Events outside our control, for example, extreme weather conditions, may occasionally mean
we are unable to deliver your goods at the time agreed. In these circumstances, we will contact
you as early as possible to rearrange the delivery time. In such an event, our liability to you shall
be limited to the price of the goods ordered and the cost of delivery.<br></p>

<h5> IF YOU ARE DISSATISFIED</h5>
<p>7.1 We are committed to offering you the very best service and the very best produce. We
guarantee the quality of all goods and produce you order from us. You must inspect the goods
immediately upon delivery and, in the event that you are dissatisfied in any way with either our
service or the quality of the goods you receive, let us know promptly.
<br>
7.2 Complaints in respect of service will be reviewed by our customer services team and we
commit to providing you with a response within 1 working day, whenever possible.<br>
7.3 Complaints in respect of the quality of the goods you receive will be forwarded to our Head
Butcher for review. Please, therefore, retain produce for review. We commit to providing you with
a response within 1 working day, whenever possible, with our findings.<br>
7.4 Unfortunately, as our products are perishable, we are unable to accept the return of any
produce delivered to you. However, if the goods you have ordered do not meet your reasonable
satisfaction, we may, in our absolute discretion, send you a The Halal Butchery voucher to
compensate for the items which did not met your expectations within 3 working days of a
complaint being received.<br>
7.5 You can also delete your account, which will delete all personal information about you.<br>
</p>
<h5> WHAT WE WILL NOT BE LIABLE FOR</h5>
<p>
8.1 We will not be liable for any indirect or consequential loss or damage or loss of profits arising
out of our supply or failure to supply the goods to you.<br>
8.2 Nothing in this Agreement shall in any way limit our liability for death or personal injury
resulting from our breach of contract, tort or negligence nor limit any legal rights you have as a
consumer.<br>
8.3 Some of our products may contain traces of nuts and other allergens. We will ensure that the
contents of each box are clearly listed on our website, but it is your responsibility to ensure that
you have read the information. If you require further information on allergens, please contact us.<br>
8.4 The goods are sold to you on the basis that you are a consumer, therefore we will not be
liable for any special losses that you might suffer using, distributing or reselling the goods as part
of a business.<br>
8.5 We shall not be liable for goods once they have been delivered to the address or secure
place stated in your order.<br>
8.6 Our packaging is designed to ensure our produce reaches you in perfect condition, but it is
your responsibility to ensure that all perishable goods are refrigerated as soon as possible upon
receipt. Further, if you or a third party moves, transports, or delivers the goods to any other
address following our delivery, we shall not be liable for anything that said third party might do to
the goods or any subsequent deterioration in the produce.
<br>
8.7 Due to the nature of certain artisan products, the minimum and maximum weight cannot
always be guaranteed. We will not accept liability for under or overweight products, the published
price is the unit price. We will, however, always endeavour to meet weight expectations.<br>
8.8 Please note that we will do our best to ensure that access to our website is uninterrupted and
that transmissions are error-free but, given the nature of the internet, this is not something that
we are able to guarantee. There may be occasions where access is suspended or restricted so
that we can undertake repairs, maintenance, or improvements. We will keep such occasions to a
minimum and endeavour to restore full access as soon as we can.<br></p>
<h5> ACCOUNT DELETION</h5>
<p>9.1 You can delete your account anytime by clicking on the delete button in the account
dashboard.<br>
9.2 After you click on ‘delete account’, all your personal details except your email are deleted.
But, for the orders placed from your account, all the details required to process that order will
remain as historical records of payment. It will take a period of one month for your personal
information to be completely removed from our backup data on the website.<br></p>
<h5> GENERAL</h5>
<p>10.1 These terms and conditions set out the entire agreement between us. Any failure by us to
enforce any of our rights under this Agreement shall not prejudice our entitlement to rely on any
of our rights in the future.<br>
 10.2 We shall not be liable for delays or failures to perform our obligations if that delay or failure
arises from events beyond our reasonable control.<br>
10.3 We reserve the right to change our website, policies and terms and conditions at any time,
but this shall not affect the terms and conditions prevailing at the time you make a purchase.<br>
10.4 Whilst we take the utmost care to ensure that all product descriptions, images, information,
and prices are accurate, we do not accept any liability for any inaccuracies, errors, or omissions.
Colour and shape may vary from those shown on the website.<br>
10.5 This Agreement is governed by the laws of England and Wales and both parties hereby
submit to the exclusive jurisdiction of the English courts.<br></p>
	</div></div></div>
</div>
@endsection
<!-- /SECTION -->
