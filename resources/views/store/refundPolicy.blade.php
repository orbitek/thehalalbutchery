@extends('store.storeLayout')
@section('content')
<style type="text/css">
	
</style>
<!-- SECTION -->
<div class="section">
	<div class="container">
		<div class="row">
		<div class="col-md-1"></div>		
		<div class="col-md-10" style="padding-right: 20px;padding-left: 20px; text-align: left">
		<h2>Refund policy</h2>
		<p>We have a 1-Hour return policy, which means you have 1 hour after receiving your item to
request a return only on unlikely quality issue, not for change of mind. 
To be eligible for a return, your item must be in the same condition that you received it,, have
photographic evidence and unused, with tags, and in its original packaging. You’ll also need the
receipt or proof of purchase.
To start a return, you can contact us at liveorders@thehalalbutchery.com. If your return is
accepted, we will send you a collection time slot, in which a driver will return and collect the item.</p>
<h4>Damages and issues
</h4>
<p>Please inspect your order upon reception and contact us immediately if the item is defective,
damaged or if you receive the wrong item, so that we can evaluate the issue and make it right.

</p>
<h4>Exchanges 
</h4>
<p>The fastest way to ensure you get what you want is to return the item you have, and once the
return is accepted, make a separate purchase for the new item.</p>
<h4>Refunds</h4>
<p>We will notify you once we’ve received and inspected your return, and let you know if the refund
was approved or not. If approved, you’ll be automatically refunded on your original payment
method. Please remember it can take some time for your bank or credit card company to process
and post the refund too.</p>
	</div></div></div>
</div>
@endsection
<!-- /SECTION -->
