@extends('store.storeLayout')
@section('content')
<!-- SECTION -->
<style type="text/css">
    th {
        text-align: center;
    }
    thead{
        background: #DDD;
    }

</style>
<div class="section steps">
    <!-- container -->
    <div class="container" style="color: white !important;">

        <!-- row -->
        <div class="row">
            <div class="col-md-12">
                <div class="section-title text-center">
                    <h3 class="title">Your Orders</h3>
                </div>
                <table class="table">
                    <thead>
                        <th>Invoice Id</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th>Payment</th>
                        <th>Action</th>

                    </thead>
                    <tbody  style="color: white">
                        @foreach($invoice as $s)
                            <tr>
                            <td><a href="invoice/{{$s->id}}"  style="color: white">{{$s->id}}</a></td>
                            <td>£{{$s->total_price}}</td>
                            <td>{{$s->status}}</td>
                            <td>{{ ($s->payment_method == 1)? (($s->paid == 0)? "Unpaid":"Paid") : "Cash on delivery"}}</td>
                            <td><a href="invoice/{{$s->id}}" class="btn btn-primary">View</a>
                                @if($s->paid == 0)
                                <a href="https://thehalalbutchery.com/checkout/{{$s->id}}" class="btn btn-warning">Pay Now</a>
                                @endif
                              </td>
                            </tr>
                          
                        @endforeach
                        </tbody>
                </table>
            </div>
        </div>
        <!-- /Billing Details -->
    </div>

</div>

@endsection
