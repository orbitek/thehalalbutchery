@extends('store.storeLayout')
@section('content')

<!-- SECTION -->
<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-4"><img src="{{asset('public/images/delivery.jpeg')}}" alt="Delivery_process" style="width: 290px;border-radius:40px;margin-top: 55px;border: 3px solid #ad9802;" alt=""></div>
		<div class="col-md-8" style="padding-right: 20px; text-align: left">
		<h1>Delivery</h1>
		<p>The Halal Butchery promises deliveries across all 7 days in the week between 10am to 9pm.<br>
		Each delivery will arrive with your meat individually packed and delivered in a refrigerated van, ensuring your meat to arrive as fresh as possible!<br>
		*Please note – Orders made for same day delivery must be ordered no later than 7pm (4pm on Sundays.)<br>
		For any requests or amendments, please email liveorders@thehalalbutchery.com.
		</p>
		<h1>Delivery Charges</h1>

	Below are our delivery charges:
<ul>	
<li>- Within 3 hours – £6.99</li>
<li>- Anytime Today - £4.99</li>
<li>- Designated Time &amp; Day– £9.99</li>
<li>- We offer Free Delivery for any order made above £40.00!</li>
</ul>
<br>
<h1>Delivery Complaints</h1>
<p>The customer must ensure the order is accepted at the time of the delivery. If an order was made and has failed to deliver due to there being no one present at home, the responsibility falls with the customer. No claims can be made against The Halal Butchery, respectfully. Additionally, any complaints regarding the quality of meat must be raised within one hour of the delivery being made.</p>
	</div></div></div>
</div>
@endsection
<!-- /SECTION -->
