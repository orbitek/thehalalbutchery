@extends('store.storeLayout')
@section('content')

<!-- SECTION -->
<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-4"><img src="{{asset('public/images/Aboutus.jpeg')}}" alt="About_us" style="width: 290px;border-radius:40px;margin-top: 0px;border: 3px solid #ad9802;" alt=""></div>
		<div class="col-md-8" style="padding-right: 20px; text-align: left">
		<h1>About Us</h1>
		<p>About Us

The Halal Butchery is an online meat store and more. To fulfil expanding customer expectations and the evolution of consumer culture, The Halal Butchery has incorporated technology with the meat industry. As a result, the maximum convenience of ordering products in the contemporary world is now available to you wherever you are, anytime you want.<br><br> 
Need your meat by this evening? … Not a problem.<br><br>
The Halal Butchery was founded in order to make halal and Tayyab accessible. Providing a dynamic experience when selecting your meats of choice; The Halal Butchery seeks to bring more options and preparation styles to the consumer from the comfort and safety of their own home. The THB menu ranges from Sirloin & Tomahawk steaks, right down to Peri Peri marinated chicken legs ready to throw on the grill. 
Beyond the extensive range of products available, we pride ourselves on being the only same day delivery service; even providing 3-hour delivery slots for those who are in need of an emergency care package! Operating in a multi-drop fashion, our deliveries are made across Greater Manchester throughout the day in our refrigerated vans.<br><br> 
We consider our customers to be family, thus high-quality meat and reliable suppliers are crucial to us. We have strict controls in place to guarantee that our halal guidelines are followed from farm to table. We are committed to providing our customers with high-quality halal meat in a timely and convenient manner. Our goal is to make our customer’s life simpler, and we appreciate their continued support and contributions within the THB Community.<br><br>
</p>
	</div></div></div>
</div>
@endsection
<!-- /SECTION -->
