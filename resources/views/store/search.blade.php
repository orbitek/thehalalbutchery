@extends('store.storeLayout')
@section('content')
<div class="section steps">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">

            <!-- STORE -->
            <div id="store" class="col-md-12">
                <!-- store products -->
                <div class="row">
                    @foreach(collect($products)->sortByDESC('price') as $product)
                      <!-- product -->
                         <!-- product -->
                        <div class="col-md-4">
                            <div class="product">
                                <div class="product-img">
                                     @if($product->active == 1 )
                                    <a href="{{route('product.viewByName',['slug'=>$product->slug])}}"> <img class="img-size" src="https://thehalalbutchery.com/public/uploads/products/{{$product->id}}/{{$product->image_name}}" alt="{{$product->name}}"></a>
                                    @else
                                    <a href="#"> <img class="img-size" src="https://thehalalbutchery.com/public/uploads/products/{{$product->id}}/{{$product->image_name}}" alt="{{$product->name}}"></a>
                                    @endif
                                    
                                </div>
                                <div class="product-body">
                                    <h3 class="product-name" ><a href="{{route('product.viewByName',['slug'=>$product->slug])}}" style="font-size: 20px; color: #BEA004">{{$product->name}} <span style="font-size:10px;" >{{ $product->tag }}</span></a></h3>
                                    <span style="display: none"> {{ $p = \App\Variation::where(['product_id' => $product->id])->min('price')}}</span>
                                    <div style="display:flex;align-items: flex-end;">
                                        <h6 style="margin-right: 10px">From</h6>
                                        <h4 style="margin-right:20px;text-decoration: line-through;text-decoration-color: red;color: red;margin-right:10px; display: none;" >£ {{ number_format((float)(($p)+($p)*(20/100)), 2, '.', '')  }}</h4>
								        <h4 class="product-price">£ {{ number_format((float)($p), 2, '.', '') }}</h4>
                                    </div>
                                   

                                    @if($product->active == 1 )
                                <div >
                                    <form action="{{route('product.viewByName',['slug'=>$product->slug])}}" style="display: flex;margin: 0 auto; align-items: center;" class="row">

                                        <button class="add-to-cart-btn carousel_order_now col-xs-8" type="submit" style="width: 80%; border: 0px;" ><i class="fa fa-shopping-cart" style="margin-right:15px"></i> <span>Shop Now</span></button>
                                        <!-- <i class="fa fa-heart col-xs-2 favourite" id="{{$product->id}}" onclick="favourite(this.id)" style="font-size: 35px; text-align: right; padding-right: 0px;"></i> -->

                                    </form>

                                </div>
                                @endif
                                @if($product->active == 0 )
                                <div  >
                                     <form action="#" style="display: flex;margin: 0 auto; align-items: center;" class="row">
                                        <button class="add-to-cart-btn carousel_order_now col-xs-8" style="background-color: #ab1c05;width: 80%; border: 0px;" ><i class="fa fa-shopping-cart"></i>Out of Stock</button>
                                        <!-- <i class="fa fa-heart col-xs-2 favourite" id="{{$product->id}}" onclick="favourite(this.id)" style="font-size: 35px; text-align: right; padding-right: 0px;"></i> -->

                                    </form>
                                </div>
                                @endif
                                @if($product->active == 2 )
                                <div >
                                    <button class="add-to-cart-btn carousel_order_now col-xs-8"  ><i class="fa fa-shopping-cart"></i>Coming Soon</button>
                                    <!-- <i class="fa fa-heart col-xs-2 favourite" id="{{$product->id}}" onclick="favourite(this.id)" style="font-size: 35px; text-align: right; padding-right: 0px;"></i> -->
                                   
                                </div>
                                @endif


                                    
                                </div>
                               
                            </div>
                        </div>
                        <!-- /product -->
                    @endforeach
                </div>
                <!-- /STORE -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
</div>

    @endsection
