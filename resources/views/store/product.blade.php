    @extends('store.storeLayout')
@section('content')
<script src="{{asset('public/js/lib/jquery.js')}}"></script>
<script src="{{asset('public/js/dist/jquery.validate.js')}}"></script>
 <script data-require="jquery@3.1.1" data-semver="3.1.1" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

<link type="text/css" rel="stylesheet" href="{{asset('public/css/style_for_quantity.css')}}" />

<style>
label.error {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
  padding:1px 20px 1px 20px;
}
.product-preview img {
    border: 1px solid #ad9802;
   border-radius: 40px 0px 40px 0px;
}
.product-details .product-rating>i.fa-star{
    color: #ad9802;
    -webkit-text-stroke: 1px black;
}
.product-details .product-rating>i {
    -webkit-text-stroke: 1px black;
}
.product-available{
    color: #ad9802;
}
.in{
    margin: 0px 0px 5px 0px;
}
.product-details .product-name {
    text-transform: uppercase;
    font-size: 40px;
    margin-top: 30px;
}
.question{
    border-radius: 40px;
}


#quantity{
    border-radius: 40px;
    -ms-box-sizing:content-box;
    -moz-box-sizing:content-box;
    box-sizing:content-box;
    -webkit-box-sizing:content-box; 
}
select{
 
    -ms-box-sizing:content-box;
    -moz-box-sizing:content-box;
    box-sizing:content-box;
    -webkit-box-sizing:content-box; 
    
}
.add-to-cart .add-to-cart-btn {
    position: relative;
    border: 2px solid transparent;
    height: 40px;
    padding: 0 30px;
    background-color:#ad9802;
    color: #FFF;
    text-transform: uppercase;
    font-weight: 700;
    border-radius: 40px;
    -webkit-transition: 0.2s all;
    transition: 0.2s all;
    margin-bottom: 30px;}
    label{
        text-align: left;
    }
    .add-to-cart-btn{
        margin: 10px 0px 10px 0px;
    }
    h1, h2, h3, h4, h5, h6 {
    color: #fff;
}

   
</style>

<!-- SECTION -->
<div class="section steps" style="background: none;padding-bottom: 0px;">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row in">
            <!-- Product main img -->
            <div class="col-md-6 " style="padding: 15px 15px 15px">
                <div id="product-main-img">
                    <div class="product-preview">
                        <img src="../public/uploads/products/{{$product->id}}/{{$product->image_name}}" alt="{{$product->name}}">
                    </div>
                </div>
            </div>
            <!-- /Product main img -->


            <!-- Product details -->
            <div class="col-md-6">
                <div class="product-details">
                    <h1 class="product-name" style="color: black">{{$product->name}}<span style="font-size:14px;" >{{ $product->tag }}</span></h1>
                    <ul class="product-links">
                        <li><a href="{{route('user.search')}}?c={{$product->category->id}}" style="color: black">{{$product->category->name}}</a></li>
                    </ul>
                    <div>
                        <div class="product-rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                        </div>
                    </div>
                    <div>
                        <h3 class="product-price"><span id="old-price" style="margin-right:20px;text-decoration: line-through;text-decoration-color: red;color: red;display:none"></span> £ <span id="price"></span> </h3>
                        <span class="product-available">In Stock</span>
                    </div>
                    <div class="row in">
                        <div class="col-md-12">
                        <p style="color: black">{!!$product->description!!}</p>
                        </div>
                    </div>
                    
                    <form method="post" id="order_form" action="{{route('user.postcart',$product->id)}}">
                    {{csrf_field()}}
                    <input type="hidden" class="form-control" name="sizeI" id="sizeI">
                    <input type="hidden" class="form-control" name="priceI" id="priceI">
                    <div class="product-options row in" >
                        <input type="hidden" id="discount_price_holder" name="discount_price_holder" value={{$product->discount}}>
                    </div>
                    	<div class="row in">
                        <label class="col-md-6" style="color: black"> {{ ($product->category_id == 6)?'Box':'Weight'}} </label></div>
                        <div class="row in">
                            @if(isset($var))
 						<select class="form-control col-md-10 question" onchange="priceManage()" name="size" id="size" style="">
 							@foreach($var as $v)
                            <option value="{{$v->price}}" data-size="{{$v->variation}}" >{{$v->variation}}</option>
 							@endforeach
 						</select>
                        @endif
                        </div>
                        @foreach($colors as $c)
                        <input type="radio" name="color" hidden checked="checked" value="#00000">
                        <div style="height:25px;width:25px;margin:5px;display:inline-block;background-color: #000000" hidden></div>
                        @endforeach
                          
                    </div>
                   		<span hidden="hidden">{{$Q = 0}}</span>
                        @foreach($questions as $q)
                        <div class="row in" >
                            <label class="col-md-6" style="color: black"> {{ $question = \App\Question::where(['id' => $q->question_id])->value('question')}}</label>
                            <label hidden="hidden" > {{$options = \App\Option::where(['question_id' => $q->question_id])->get()}}</label>
                            <input type="text" name="tq" value="{{$Q = $Q+1}}" hidden="hidden">
                            </div>
                            <div class="row in">
                            <input type="text" name="questions[]" value="{{$question}}" hidden="hidden">
                            <select class="form-control col-md-10 question" name="questionA[]" style="">
                            @foreach($options as $o)
                            <option>
                                {{$o->option}}
                            </option>
                            @endforeach
                            </select>
                        </div>
                       @endforeach
                 
                        <div id="for_error"></div>

                    <div class="add-to-cart row in" style="margin-top: 30px">
                       
                        <label class="col-md-2" style="font-weight: bold; margin-top: 15px; color: black">QUANTITY:</label>
                       
                   
                        <input type="number" id="quantity" inputmode="numeric" name="quantity" class="quantity form-control col-md-3" value="1" style="margin-right: 60px;"  />
                   
                        
                        <button type="submit" name="myButton" id="myButton" class=" add-to-cart-btn col-md-4"><i class="fa fa-shopping-cart"></i> add to cart</button>
                         
                    </div>
                    </form>
                    
                </div>
            </div>
            <!-- /Product details -->

        </div>
        <!-- /row -->

    <div class="section steps" style="">
    <!-- container -->
    <div class="container">
        <div class="row">

            <!-- section title -->
            <div class="col-md-12">
                <div class="section-title">
                    <h1 class="title">Related Items</h1>

                </div>
            </div>
            <!-- /section title -->
            <!-- Products tab & slick -->
            <div class="col-md-12">
                <div class="row">
                    @foreach($products as $product)
                    <!-- product -->
                    <div class="col-md-4">
                        <div class="product">
                            <div class="product-img">
                                <a href="{{route('product.view',['id'=>$product->id])}}"> 
                                     <div class="img-container">
                                    <img class="img-size" src="../public/uploads/products/{{$product->id}}/{{$product->image_name}}" alt="{{$product->name}}">
                                </a>
                                      
                                      
                                    </div>
                                <div class="product-label">

                                    <span class="new" style="display: none;"> {{ ($product->active==1)?$product->tag:"Out Of Stock"}}</span>
                                    <span class="sale" style="display: none;">£ {{$product->discount}}</span>
                                </div>
                            </div>
                            <div class="product-body">
                                <p class="product-category" style="display:none;">{{$product->category->name}}</p>
                                
                                <h3 class="product-name" ><a href="{{route('product.view',['id'=>$product->id])}}" style="font-size: 20px; color: #BEA004">{{$product->name}}<span style="font-size:10px;" >{{ $product->tag }}</span></a></h3>
                                <span style="display: none"> {{ $p = \App\Variation::where(['product_id' => $product->id])->min('price')}}</span>
                                <div style="display:flex;align-items: flex-end;">
                                   <h6 style="margin-right: 10px">From</h6> 
                                   <h4 style="margin-right:20px;text-decoration: line-through;text-decoration-color: red;color: red;margin-right:10px" >£ {{ round((($p)+($p)*(20/100)),2) }}</h4>
                                   <h4 class="product-price">£ {{ number_format((float)($p), 2, '.', '') }}</h4>
                               </div>
                               @if($product->active == 1 )
                               <div >
                                <form action="{{route('product.view',['id'=>$product->id])}}" style="    display: flex;margin: 0 auto;align-items: center;" class="row">

                                    <button class="add-to-cart-btn carousel_order_now col-xs-8" type="submit" style="width: 80%; border: 0px;" ><i class="fa fa-shopping-cart" style="margin-right:15px"></i> <span>Shop Now</span></button>
                                    <i class="fa fa-heart col-xs-2 favourite"  id="{{$product->id}}"  onclick="favourite(this.id)" style="font-size: 35px; text-align: right; padding-right: 0px;"></i>
                                </form>

                            </div>
                            @endif
                            @if($product->active == 0 )
                            <div  >
                                <button clbuttonss="add-to-cart-btn carousel_order_now" style="background-color: #ab1c05;" ><i class="fa fa-shopping-cart"></i>Out of Stock</button>
                                <i class="fa fa-heart col-xs-2 favourite" id="{{$product->id}}" onclick="favourite(this.id)" style="font-size: 35px; text-align: right; padding-right: 0px;"></i>
                            </div>
                            @endif
                            @if($product->active == 2 )
                            <div >
                                <button class="add-to-cart-btn carousel_order_now"  ><i class="fa fa-shopping-cart"></i>Coming Soon</button>
                            </div>
                            @endif              
                        </div>
                    </div>
                </div>
                <!-- /product -->
                @endforeach
                </div>
            </div>
    </div>
</a>
</div>
</div>
    <!-- /container -->
</div>

<!--JQUERY Validation-->
<script>
	
    //////////////////////////////////////
    $(document).ready(function() {
		
		$("#order_form").validate({
			
            submitHandler: function (form) {
            if($('input[name=color]:checked').val()==undefined)
            {
                
            document.getElementById("for_error").innerHTML = "<label class='error' style=' '>Invalid Variation Input</label>";

            }
                else
                    {
                        return true;
                    }
                
         }
		});

		
	});

    function priceManage(){
        var price = $("#size").val();
        var size = $("#size").find(':selected').attr('data-size');
        $("#priceI").val(price);
        $("#sizeI").val(size);
        $("#price").text(Number(price).toFixed(2));
        var percent = (20/100) * price;
        $("#old-price").text('£'+ Number(parseFloat(price)+parseFloat(percent)).toFixed(2));
        
    }
    priceManage();
	
    $('.add').click(function () {
        
        $(this).prev().val(+$(this).prev().val() + 1);
        
    });
    $('.sub').click(function () {
            if ($(this).next().val() > 1) {
            $(this).next().val(+$(this).next().val() - 1);
            }
    });
    
	
   
	</script>
<!--/JQUERY Validation-->
<!-- /SECTION -->
@endsection
