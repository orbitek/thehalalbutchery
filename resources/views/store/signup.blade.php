@extends('store.storeLayout')
@section('content')
<script src="{{asset('public/js/lib/jquery.js')}}"></script>
<script src="{{asset('public/js/dist/jquery.validate.js')}}"></script>
 <script src="{{asset('public/js/postcoder-autocomplete.js')}}"></script>

<style>
label.error {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1;
  padding:1px 20px 1px 20px;
}


</style>
   <!-- SECTION -->
<div class="section steps">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row" style="display: flex;flex-wrap: wrap; background:#ffffffe0; border-radius:150px 0px 150px 0px;">
        <div class="col-md-6" style="background: white;padding-left: 0px;padding-right: 0px;background: black;border-radius: 150px 0px 0px 0px;"> 
            <img src="{{asset('public/images/slides/main.png')}}" alt="Premium BBQ Box" style="width: 100%; border-radius: 150px 0px 0px 0px; ">
         </div>

        <div class="col-md-6">

        <form id="signupForm"  method="post" >
            {{csrf_field()}}
            <div class="">
                <!-- Billing Details -->
                <div class="billing-details">
                    <div class="section-title">
                        <h3 class="title" style="font-size: 33px;color: #2b2d42;">SIGN UP</h3>
                    </div>
                    
                    <div class="form-group ">
                        <input class="input" type="text" name="name" id="name" placeholder="Full Name">
                    </div>
                   {!! $errors->first('name', '<label class="error">:message</label>') !!}
                    
                    <div class="form-group">
                        <input class="input" type="email" name="email" id="email" placeholder="Email" onkeypress="check_email()">
                    </div>
                    <div id="for_duplicate-email"></div>
                     {!! $errors->first('email', '<label class="error">:message</label>') !!}
                     <!-- Address section -->
                      <div class="address-finder" id="address_finder">
                        <div class="form-group">
                            <label for="postcoder_autocomplete" id="postcoder_autocomplete_label">Type your address or postcode</label>
                            <input id="postcoder_autocomplete" type="text" class="form-control input">
                        </div>
                    </div>


                    <div class="form-group">
                        <input class="input" type="text" name="address" id="address" placeholder="Address">
                    </div>
                     {!! $errors->first('address', '<label class="error">:message</label>') !!}
                    <div class="form-group">
                        <input class="input" type="text" name="city" id="city" placeholder="City">
                    </div>
                     {!! $errors->first('city', '<label class="error">:message</label>') !!}
                    <div class="form-group">
                        <input class="input" type="text" name="postcode" style="text-transform:uppercase" id="postcode" onblur="check_postcode()" placeholder="Post Code">
                    </div>
                     {!! $errors->first('post', '<label class="error">:message</label>') !!}
                     <!-- Address Section End -->
                    <div class="form-group">
                        <input class="input" type="tel" maxlength="12" minlength="11" name="tel" id="tel" pattern="^(\+44\s?7\d{3}|\(?07\d{3}\)?)\s?\d{3}\s?\d{3}$" title="Mobile number should be in formated like, 44711122233, 0711122233" placeholder="Mobile" required="required">
                        <span style="float: left;margin: 15px;color: #7b7b7b;font-size: 12px;">E.g. "44711122233 or 0711122233 "</span>
                    </div>
                     {!! $errors->first('tel', '<label class="error">:message</label>') !!}
                    <div class="form-group">
                        <input class="input" type="password" name="pass" id="pass" placeholder="Enter Your Password">
                    </div>
                     {!! $errors->first('pass', '<label class="error">:message</label>') !!}
                    <div class="form-group">
                        <input class="input" type="password" name="confirm_password" id="confirm_password" placeholder="Confirm Password">
                    </div>
                    {!! $errors->first('confirm_password', '<label class="error">:message</label>') !!}


                    <br>
                        
                        <input type="submit"  name="signup" class="primary-btn order-submit" value="Sign Up" style="width: 75%;margin-bottom:22px ;">

                        <div class="alreadyin">
                            <span class="" style="font-size:18px;font-weight: 300;margin-bottom: 14px">Already Registerd? <a href="{{route('user.login')}}" style="color:#1EE92C;font-weight: 300"> Login </a> </span>
                        </div>

                        <div class="alreadyin">
                            <span class="" style="font-size:18px;font-weight: 300"><input type="checkbox" name="tc" id="tc" checked> I agree to   <a href="{{route('termsConditions')}}" style="color:#E91E1E;font-weight: 300">Terms & Conditions </a> of THB</span>
                        </div>
                </form>
                
                </div>
                    
                </div>
                <!-- /Billing Details -->
            </div>

        </div>
        <!-- /row -->
    </div>
    <!-- /container -->
</div>

<!--JQUERY Validation-->
<script>
    
    
   
    
    
    
	$(document).ready(function() {
		// validate the comment form when it is submitted
		//$("#commentForm").validate();

		// validate signup form on keyup and submit
		$("#signupForm").validate({
			rules: {
				name: "required",
				email: {
					required: true,
					email: true
				},
                address: "required",
                city: "required",
                postcode: {
					required: true,
				},
                tel: "required",
				pass: {
					required: true,
					minlength: 5
				},
				confirm_password: {
					required: true,
					minlength: 5,
					equalTo: "#pass"
				}
				
				
				
			},
			messages: {
				name: "Please enter your Fullname",
				email: "Please enter a valid email address",
                address: "Please enter your Address",
                city: "Please enter your City",
                address: "Please enter your Address",
				postcode: {
					required: "Please enter Post code",
				},
                tel: "Please enter your Phone number",
				pass: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long"
				},
				confirm_password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long",
					equalTo: "Please enter the same password as above"
				}
				
				
			}
            
            
        
		});

		
	});
    
    function check_postcode() {
    var str =document.getElementById("postcode").value;
    str = str.toUpperCase();
    var n = str.startsWith("M") || str.startsWith("SK") || str.startsWith("WA") || str.startsWith("OL");
    if (n != true)
    {
        var str =document.getElementById("postcode").value = '';
        Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Invalid Postcode!',

        footer: 'We are serving only in Manchester for now!'
        
    })
  }
  else{
    IsPostcode(str);
  }
  //alert(n);
}   
function IsPostcode(postcode)
    {
     postcode = postcode.toString().replace(/\s/g, "");
    // alert(postcode);
    var regex = /^[A-Z]{1,2}[0-9]{1,2} ?[0-9][A-Z]{2}$/i;
    //alert(regex.test(postcode));
    if (regex.test(postcode) != true) 
    {
      var str =document.getElementById("postcode").value = '';
        Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Invalid Postcode!',
        footer: 'We are serving only in Manchester for now!'
    })
    
    }
}
//     $("#tel").keydown(function(e) {
//     var oldvalue=$(this).val();
//     var field=this;
//     setTimeout(function () {
//         if(field.value.indexOf('44') !== 0) {
//             $(field).val(oldvalue);
//         } 
//     }, 1);
// });

function check_email() {
    
    //var token={{ csrf_token() }};
    var email=$("#email").val();
    var token=$("input[name=_token]").val();
    var url="{{route('user.signup.check_email')}}";
    
            $.ajax({
                type:'post',
                url:url,
                dataType: "JSON",
                async: false,
                data:{email: email, _token: token},
                success:function(msg){
                        
                         
                        if(msg == "1")
                            {
                                document.getElementById("for_duplicate-email").innerHTML = "<label class='error'>This Email Address is Already taken</label>";
                                                    

                            }
                    else
                        {
                                                document.getElementById("for_duplicate-email").innerHTML = "";

                        }
                    }
             });
    
}
</script>
<!--/Duplicate Email Validation-->

<!-- Postcoder -->
<script type="text/javascript">
     // Choose the element we will use as the search box
            var autocomplete_input = document.getElementById("postcoder_autocomplete");
            var autocomplete_label = document.getElementById("postcoder_autocomplete_label");
            var autocomplete_wrapper = document.getElementById("address_finder");

            // Attach autocomplete to search box, with our settings
            // To get your free trial API key visit https://www.alliescomputing.com/postcoder/sign-up
            var postcoder_complete = new AlliesComplete(autocomplete_input, {
                apiKey: "PCWZL-J8MCL-3VZBS-F9RP3", // Change this to your own API key
                autocompleteLabel: autocomplete_label,
                autocompleteWrapper: autocomplete_wrapper
            });

            // This event is fired by library when user selects an item in the list
            autocomplete_input.addEventListener("postcoder-complete-selectcomplete", function(e) {

                auto_address_select(e.address);

            });

            // Demo function to populate form fields with address fields from chosen address
            function auto_address_select(the_address) {

                
                document.getElementById("address").value = the_address.addressline1 + " " +the_address.addressline2 || "";
                // document.getElementById("address_line_2_auto").value = the_address.addressline2 || "";
                document.getElementById("city").value = the_address.posttown;
               
                document.getElementById("postcode").value = the_address.postcode;

                autocomplete_input.value = "";
                autocomplete_input.blur();
                check_postcode();
                return true;

            }
</script>
@endsection

