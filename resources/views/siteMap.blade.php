@extends('store.storeLayout')
@section('content')

<!-- SECTION -->
<div class="section">
	<div class="container">
		<div class="row">
	
		
		<h1>THB Site Map</h1>
		<div class="col-md-4">
			<h3>Services</h3>
			  <ul class="footer-links">
                                
                                <li><a href="{{route('aboutUs')}}"> About Us</a></li>
                                <li><a href="{{route('deliveryProcess')}}"> Delivery Process</a></li>
                                <li><a href="{{route('privacyPolicy')}}"> Privacy Policy</a></li>
                                <li><a href="{{route('refundPolicy')}}"> Refund Policy</a></li>
                                <li><a href="{{route('termsConditions')}}"> Terms & Conditions</a></li>
                              
               </ul>
		</div>
		</div>
	</div>
</div>
@endsection
<!-- /SECTION -->
