<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForgetPassword extends Model
{
     protected $fillable = [
        'user_id',
        'email',
        'link',
        'active',
    ];

}
