<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postcode_admin extends Model
{
     protected $fillable = [
        'postcode_id',
        'admin_id',
        'assign_by',
    ];

}
