<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Blog;
use App\User;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Blogs=Blog::getAllBlog();
        // return $Blogs;
        return view('backend.Blog.index')->with('Blogs',$Blogs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        die;
        // return $request->all();
        $this->validate($request,[
            'title'=>'string|required',
            'quote'=>'string|nullable',
            'summary'=>'string|required',
            'description'=>'string|nullable',
            'photo'=>'string|nullable',
            'tags'=>'nullable',
            'added_by'=>'nullable',
            'Blog_cat_id'=>'required',
            'status'=>'required|in:active,inactive'
        ]);
        $img = explode('|', $request->img);
            
            for ($i = 0; $i < count($img) - 1; $i++) {
                
                if (strpos($img[$i], 'data:image/jpeg;base64,') === 0) {
                    $img[$i] = str_replace('data:image/jpeg;base64,', '', $img[$i]);  
                    $ext = '.jpg';
                }
                if (strpos($img[$i], 'data:image/png;base64,') === 0) { 
                    $img[$i] = str_replace('data:image/png;base64,', '', $img[$i]); 
                    $ext = '.png';
                }
            }
        $data=$request->all();

        $slug=Str::slug($request->title);
        $count=Blog::where('slug',$slug)->count();
        if($count>0){
            $slug=$slug.'-'.date('ymdis').'-'.rand(0,999);
        }
        $data['slug']=$slug;

        $tags=$request->input('tags');
        if($tags){
            $data['tags']=implode(',',$tags);
        }
        else{
            $data['tags']='';
        }
        $data['photo'] = "1".$ext;


        $status=Blog::create($data);

        // Saving Image
        $img[$i] = str_replace(' ', '+', $img[$i]);
                $data = base64_decode($img[$i]);
                
                $temp_string='/uploads/blogs/'.$prd->id;
                $temp_string2='/uploads/blogs/'.$prd->id;
                
                if (!file_exists(public_path().$temp_string)) {
                    mkdir( public_path().$temp_string, 0777, true);
                    
                    $file = public_path().$temp_string2.'/1'.$ext;
                    
                    if (file_put_contents($file, $data)) {
                        echo "<p>Image $i was saved as $file.</p>";
                    } else {
                        echo '<p>Image $i could not be saved.</p>';
                    } 
                }
                // End Saving image
        if($status){
            request()->session()->flash('success','Blog Successfully added');
        }
        else{
            request()->session()->flash('error','Please try again!!');
        }
        return redirect()->route('Blog.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Blog=Blog::findOrFail($id);
        return view('backend.blog.edit')->with('blog',$Blog);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Blog=Blog::findOrFail($id);
         // return $request->all();
         $this->validate($request,[
            'title'=>'string|required',
            'quote'=>'string|nullable',
            'summary'=>'string|required',
            'description'=>'string|nullable',
            'photo'=>'string|nullable',
            'tags'=>'nullable',
            'added_by'=>'nullable',
            'Blog_cat_id'=>'required',
            'status'=>'required|in:active,inactive'
        ]);

        $data=$request->all();
        $tags=$request->input('tags');
        // return $tags;
        if($tags){
            $data['tags']=implode(',',$tags);
        }
        else{
            $data['tags']='';
        }
        // return $data;

        $status=$Blog->fill($data)->save();
        if($status){
            request()->session()->flash('success','Blog Successfully updated');
        }
        else{
            request()->session()->flash('error','Please try again!!');
        }
        return redirect()->route('blog.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Blog=Blog::findOrFail($id);
       
        $status=$Blog->delete();
        
        if($status){
            request()->session()->flash('success','Blog successfully deleted');
        }
        else{
            request()->session()->flash('error','Error while deleting Blog ');
        }
        return redirect()->route('Blog.index');
    }
}
