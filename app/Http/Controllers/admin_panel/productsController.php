<?php

namespace App\Http\Controllers\admin_panel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductVerifyRequest;
use App\Http\Requests\ProductEditVerifyRequest;

use Illuminate\Support\Facades\DB;
use App\Product;
use App\Category;
use App\Question;
use App\Variation;

class productsController extends Controller
{
    public function index()
    {
        $products = Product::all()->sortByDesc("category_id");
        
        return view('admin_panel.products.index')
        ->with('prdlist', $products);
        
    }
    
    public function create()
    {
        $catlist = Category::all();
        $data['questions'] = Question::all();
        return view('admin_panel.products.create',compact('data','catlist'));
        
    }
    
    public function view($id)
    {
        
        $res = Product::find($id);
        return redirect()->route('product.viewByName',$res->slug);
        $res1 = Product::inRandomOrder()->limit(6)->get();
        $cat=Category::find($res->category_id);
        $colorList = explode(',',$res->colors);
        $cat = Category::all();
        $questions = DB::table('product_question')->where('product_id', $id)->get(); 
        $var = Variation::where('product_id',$id)->get();
        //dd($questions);
        $title = $res->name;
        return view('store.product')
        ->with('title', $title)
        ->with('product', $res)
        ->with('products', $res1)
        ->with('cat', $cat)
        ->with('colors',$colorList)
        ->with('var', $var)
        ->with('questions',$questions); 
        
    }
    
    
    public function store(ProductVerifyRequest $request)
    { 
        $delimiter = '-';
        try {
            $img = explode('|', $request->img);
            
            for ($i = 0; $i < count($img) - 1; $i++) {
                
                if (strpos($img[$i], 'data:image/jpeg;base64,') === 0) {
                    $img[$i] = str_replace('data:image/jpeg;base64,', '', $img[$i]);  
                    $ext = '.jpg';
                }
                if (strpos($img[$i], 'data:image/png;base64,') === 0) { 
                    $img[$i] = str_replace('data:image/png;base64,', '', $img[$i]); 
                    $ext = '.png';
                }
                
                
                $prd = new Product();
                $prd->image_name = "1".$ext;
                $prd->name = $request->Name;
                $prd->description = $request->Description;
                $prd->category_id = $request->Category;
                $prd->price = $request->Price;
                $prd->discount = $request->Discounted_Price;
                $prd->slug =  strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $request->Name))))), $delimiter));
                //$prd->colors = $request->Colors;
                $prd->tag = $request->Tags;
                $prd->save();
                
                $var = new Variation();
                $var->product_id = $prd->id;
                $var->variation = $request->var1;
                $var->price = $request->var_price1;
                $var->save();
                
                if ($request->var2 != -1) {
                    
                    
                    $var = new Variation();
                    $var->product_id = $prd->id;
                    $var->variation = $request->var2;
                    $var->price = $request->var_price2;
                    $var->save();
                }
                if ($request['questions']) {
                    foreach ($request['questions'] as $value) {
                        $questions = Question::where('id',$value)->first();
                        $prd->question()->attach($questions);
                        
                    }
                }
                
                
                
                $img[$i] = str_replace(' ', '+', $img[$i]);
                $data = base64_decode($img[$i]);
                
                $temp_string='/uploads/products/'.$prd->id;
                $temp_string2='/uploads/products/'.$prd->id;
                
                if (!file_exists(public_path().$temp_string)) {
                    mkdir( public_path().$temp_string, 0777, true);
                    
                    $file = public_path().$temp_string2.'/1'.$ext;
                    
                    if (file_put_contents($file, $data)) {
                        echo "<p>Image $i was saved as $file.</p>";
                    } else {
                        echo '<p>Image $i could not be saved.</p>';
                    } 
                }
                
                
                
            }
            
            return redirect()->route('admin.products');
        } catch (\Throwable $th) {
            dd($th->getMessage());
        }
        
    }
    

    public function viewByName($slug)
    {
        
        $res = Product::where('slug',$slug)->first();

        $res1 = Product::inRandomOrder()->limit(6)->get();
        $cat=Category::find($res->category_id);
        $colorList = explode(',',$res->colors);
        $cat = Category::all();
        $questions = DB::table('product_question')->where('product_id', $res->id)->get(); 
        $var = Variation::where('product_id',$res->id)->get();
        //dd($questions);
        $title = $res->name;
        return view('store.product')
        ->with('title', $title)
        ->with('product', $res)
        ->with('products', $res1)
        ->with('cat', $cat)
        ->with('colors',$colorList)
        ->with('var', $var)
        ->with('questions',$questions); 
        
    }
    public function createFolder()
    {
        $id = 91;
        echo public_path();
        // while($id <= 200) {
            
            //  $temp_string='public/uploads/products/'.$id;
            // mkdir( public_path().$temp_string, 0777, true);
            // $id++;
            // echo $id;
            // }
        }
        
        
        
        public function edit($id)
        {
            $cat = Category::all();
            $questions = Question::all();
            
            
            $prd = Product::find($id);
            $p_questions = DB::table('product_question')->where('product_id', $prd->id)->get(); 
            $variations = Variation::where('product_id', $id)->get();
            return view('admin_panel.products.edit')
            ->with('product', $prd)
            ->with('catlist', $cat)
            ->with('questions', $questions)
            ->with('pq', $p_questions)
            ->with('variations', $variations)
            ->with('select_attribute', '');
            
            
        }
        
        public function update(ProductEditVerifyRequest $request, $id)
        {
            
            
            
            $prdToUpdate = Product::find($request->id);
            $prdToUpdate->name = $request->Name;
            $prdToUpdate->description = $request->Description;
            $prdToUpdate->price = $request->Price;
            $prdToUpdate->discount= $request->Discounted_Price;
            $prdToUpdate->category_id = $request->Category;
            $prdToUpdate->categories = json_encode($request->categories);
            
            $prdToUpdate->tag= $request->Tags;
            
            //NEW FILE UPLOADED
            if($request->img!="")
            {      
                
                $img = explode('|', $request->img);
                
                for ($i = 0; $i < count($img) - 1; $i++) {
                    
                    if (strpos($img[$i], 'data:image/jpeg;base64,') === 0) {
                        $img[$i] = str_replace('data:image/jpeg;base64,', '', $img[$i]);  
                        $ext = '.jpg';
                    }
                    if (strpos($img[$i], 'data:image/png;base64,') === 0) { 
                        $img[$i] = str_replace('data:image/png;base64,', '', $img[$i]); 
                        $ext = '.png';
                    }
                    
                    
                    
                    $prdToUpdate->image_name = "1".$ext;
                    $prdToUpdate->save();
                    $sync = $prdToUpdate->question()->sync($request->questions);
                    // dd($sync);
                    
                    
                    $img[$i] = str_replace(' ', '+', $img[$i]);
                    $data = base64_decode($img[$i]);
                    
                    
                    $temp_string2='public/uploads/products/'.$prdToUpdate->id;
                    $file = $temp_string2.'/1'.$ext;
                    
                    if (file_put_contents($file, $data)) {
                        echo "<p>Image $i was saved as $file.</p>";
                    } else {
                        echo '<p>Image $i could not be saved.</p>';
                    } 
                    
                    
                    
                    
                }
                
                return redirect()->route('admin.variation',$request->id);
                
            }
            else
            {
                
                $prdToUpdate->save();
                $sync = $prdToUpdate->question()->sync($request->questions);
                return redirect()->route('admin.variation',$request->id);
            }    
        }
        
        public function delete($id)
        {
            $prd = Product::find($id);
            return view('admin_panel.products.delete')
            ->with('product', $prd);
        }
        
        public function destroy(Request $request)
        {
            
            $prdToDelete = Product::find($request->id);   
            try{
                $src='uploads/products/'.$prdToDelete->id.'/';
                $dir = opendir($src);
                while(false !== ( $file = readdir($dir)) ) {
                    if (( $file != '.' ) && ( $file != '..' )) {
                        $full = $src . '/' . $file;
                        if ( is_dir($full) ) {
                            rrmdir($full);
                        }
                        else {
                            unlink($full);
                        }
                    }
                }
                closedir($dir);
                rmdir($src);
            }
            catch(\Exception $e){
                
            }
            $prdToDelete->delete();
            return redirect()->route('admin.products');
        }
        
        public function inStock($id, $active)
        {
            if ($active == 1) {
                $active = 0;
            }
            else{
                $active = 1;
            }
            DB::table('products')
            ->where('id', $id)
            ->update(['active' => $active]);
            return back();
        }
        
        public function variation($id)
        {
            $prd = Product::find($id);
            $vlist = Variation::where('product_id',$id)->get();
            return view('admin_panel.variation.index')
            ->with('product', $prd)
            ->with('vlist', $vlist)
            ->with('id', $id);
        }
        public function allVariation()
        {
           
            $vlist = Variation::all();

            return view('admin_panel.variation.all')->with('vlist', $vlist);
        }
        
        public function addVariation(Request $request)
        {
            $var = new Variation();
            $var->product_id = $request->id;
            $var->variation = $request->variation;
            $var->price = $request->price;
            $var->save();
            return back();   
        }
        
        public function variationDelete($id)
        {
            
            $var = Variation::find($id);
            $var->delete();
            return back();  
        }
        
        public function updateVariation(Request $request)
        {
            DB::table('variations')
            ->where('id', $request->id)
            ->update(['price' => $request->priceE, 'variation' => $request->variationE]);
            return back();
        }
        public function ajaxUpdateVariation(Request $request)
        {
            $id = $request->id;
            $price = $request->price;
            DB::table('variations')
            ->where('id', $id)
            ->update(['price' => $price]);
            
        }
        /* Cron jobs for products are starting from here 
        1. update_product_price() is to update products price according to the requirement i.e increase 20% in all prices
        */
        
        public function update_product_price()
        {
            die;
            $products = Product::all();
            foreach($products as $product)
            {
                //echo $product->id,$product->price,$product->discount."<br>";
                
                echo $product->name.'<br>';
                
                $prod_var = $variations = Variation::where('product_id', $product->id)->get();
                
                $percentage = 20;
                $prod_var_new = $product->price;
                $prod_var_new += round(($product->price)*($percentage/100),2);
                
                                
                $prod_var_disc = $product->discount;
                $prod_var_disc += round(($product->discount)*($percentage/100),2);
                
                DB::table('products')
                ->where('id', $product->id)
                ->update(['price' => $prod_var_new, 'discount' => $prod_var_disc]);

                
                foreach($prod_var as $variation)
                {
                    $new_var = $variation->price;
                    $new_var += round(($variation->price)*($percentage/100),2); 
                
                    echo $new_var;

                    DB::table('variations')
                    ->where('id', $variation->id)
                    ->update(['price' => $new_var]);
                }
                
            }

        }
        public function update_product_category()
        {
            die;
            $products = Product::all();
            foreach ($products as $prod) {

                $catA = array($prod->category->slug);
                $cat = json_encode($catA);

                $upd = DB::table('products')
                ->where('id', $prod->id)
                ->update(['categories' => $cat]);
                // dd($upd);
            }
        }

        public function update_product_price_by_var()
        {
            // die;
            $products = Product::all();
            foreach($products as $product)
            {
                //echo $product->id,$product->price,$product->discount."<br>";
                
                echo $product->name.'<br>';
                
                $prod_var = Variation::where('product_id', $product->id)->min('price');
                
                
                // dd($prod_var);         
                DB::table('products')
                ->where('id', $product->id)
                ->update(['price' => $prod_var, 'discount' => $prod_var]);    
            }
                
        }
    }
    