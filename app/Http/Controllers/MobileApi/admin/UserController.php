<?php

namespace App\Http\Controllers\MobileApi\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Admin;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function adminLoginMobile(Request $request)
    {  
        //dd($request->all());
       

        $admin = Admin::where('username',$request->username)->first();
        
        if($admin==null)
        {
            
             $response = ["message"=>"User dose not exist","error"=>true,'user' => null];
            return json_encode($response);
        }
        
        else
        {
            if($request->password==$admin->password)
            {
               
                 $response = ["message"=>"Pass","error"=>false,'user' => $admin];
                return json_encode($response);
            }
            
            else if($request->Password!=$admin->password)
            {
                $response = ["message"=>"Wrong password","error"=>true,'user' => null];
                return json_encode($response);
            }
        }
        
        
        
    }

}
