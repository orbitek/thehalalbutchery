<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;
use DB;
use App\Events\NewOrder;
use DataTables;
use App\Category;
use Mailgun\Mailgun;
use Session;
use Sentinel;
use Reminder;
use App\User;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    public function index()
    {
        $arrayName = array('user' => "Usman");
         broadcast(new NewOrder($arrayName));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice = Invoice::find($id);
        if(Session::has('user')){
        
        if (session('user')->id == $invoice->user->id) {
            return view('Email.confermationEmail',compact('invoice'));
        }
        else{
            return redirect()->route('user.history');
            }
        }
        else{
            return redirect()->route('user.home');
        }
        
    }

    public function showInvoice($id)
    {
        $invoice = Invoice::find($id);
        if(Session::has('user')){
        
            if (session('user')->id == $invoice->user->id) {
                return view('Email.confermationEmail',compact('invoice'));
            }
            else{
                if(session::has('admin')){
                    return view('Email.confermationEmail',compact('invoice'));
                }
                else
                {
                    return redirect()->route('user.home');
                }
            }
        }
        else
        {
            if(session::has('admin')){
                    return view('Email.confermationEmail',compact('invoice'));
                }
            return redirect()->route('user.home');
        }
    }

    public function print($id)
    {
      
        $invoice = Invoice::find($id);
     
        return view('admin_panel.orders.print_invoice',compact('invoice'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function dashboard_ajax($page)
    {
        $model = Invoice::with('user', 'shipping')->orderBy('id', 'DESC');
        if ($page == "Dashboard") {
            $model = Invoice::with('user', 'shipping')->orderBy('id', 'DESC');
        }
        else if ($page == "New") {
            $model = Invoice::with('user', 'shipping')->where('status',"Placed")->orderBy('id', 'DESC');
        }
         else if ($page == "Processing") {
            $model = Invoice::with('user', 'shipping')->where('status',"Processing")->orderBy('id', 'DESC');
        }
         else if ($page == "Dispatched") {
            $model = Invoice::with('user', 'shipping')->where('status',"Dispatched")->orderBy('id', 'DESC');
        }
        else if ($page == "Delivered") {
            $model = Invoice::with('user', 'shipping')->where('status',"Delivered")->orderBy('id', 'DESC');
        }
        //$model = Invoice::with('user', 'shipping')->orderBy('id', 'DESC');
                return DataTables::eloquent($model)
                ->addColumn('name', function (Invoice $invoice) {
                    return $invoice->user->full_name;
                })
                ->addColumn('shipping', function (Invoice $invoice) {
                    return $invoice->shipping->type;
                })
               ->addColumn('id', '<a href="https://thehalalbutchery.com/showInvoice/{{$model->id}}">{{$model->id}}</a>')
              
                ->addColumn('action', function (Invoice $invoice) {
                    if ($invoice->status == "Placed" && $invoice->paid == "1") {
                        return '<a href="https://thehalalbutchery.com/invoice_update/'.$invoice->id.'/Processing'.'" class="btn" style="background:white; border-radius:50px;width:40px" ><i class="menu-icon mdi mdi-thumb-up" alt="Accept"></i></a>'.'<a href="https://thehalalbutchery.com/invoice_print/'.$invoice->id.'" class="btn btn-warning" style="border-radius:50px;width:40px;margin-left: 5px;" ><i class="menu-icon mdi mdi-printer" alt="Accept"></i></a>'.'<a href="https://thehalalbutchery.com/showInvoice/'.$invoice->id.'" class="btn btn-info" style="border-radius:50px;width:40px;margin-left: 5px;" ><i class="menu-icon mdi mdi-eye" alt="Accept"></i></a>';
                    }
                    else if ($invoice->status == "Processing") {
                        return '<a href="https://thehalalbutchery.com/invoice_update/'.$invoice->id.'/Dispatched'.'" class="btn btn-primary" style=" border-radius:50px;width:40px"><i class="menu-icon mdi mdi-truck"></i></a>'.'<a href="https://thehalalbutchery.com/invoice_print/'.$invoice->id.'" class="btn btn-warning" style="border-radius:50px;width:40px;margin-left: 5px;" ><i class="menu-icon mdi mdi-printer" alt="Accept"></i></a>'.'<a href="https://thehalalbutchery.com/showInvoice/'.$invoice->id.'" class="btn btn-info" style="border-radius:50px;width:40px;margin-left: 5px;" ><i class="menu-icon mdi mdi-eye" alt="Accept"></i></a>';
                    }
                     else if($invoice->status == "Dispatched") {
                        return '<a href="https://thehalalbutchery.com/invoice_update/'.$invoice->id.'/Delivered'.'" class="btn btn-success" style=" border-radius:50px;width:40px"><i class="menu-icon mdi mdi-shopping"></i></a>'.'<a href="https://thehalalbutchery.com/invoice_print/'.$invoice->id.'" class="btn btn-warning" style="border-radius:50px;width:40px;margin-left: 5px;" ><i class="menu-icon mdi mdi-printer" alt="Accept"></i></a>'.'<a href="https://thehalalbutchery.com/showInvoice/'.$invoice->id.'" class="btn btn-info" style="border-radius:50px;width:40px;margin-left: 5px;" ><i class="menu-icon mdi mdi-eye" alt="Accept"></i></a>';
                    }
                     else {
                        return '<a href="https://thehalalbutchery.com/invoice_print/'.$invoice->id.'" class="btn btn-warning" style="border-radius:50px;width:40px;margin-left: 5px;" ><i class="menu-icon mdi mdi-printer" alt="Accept"></i></a>'.'<a href="https://thehalalbutchery.com/showInvoice/'.$invoice->id.'" class="btn btn-info" style="border-radius:50px;width:40px;margin-left: 5px;" ><i class="menu-icon mdi mdi-eye" alt="Accept"></i></a>';
                    }
                    
                })
                ->addColumn('paid', function (Invoice $invoice) {
                    return $invoice->paid;
                })
                ->rawColumns(['action','id'])
                ->toJson();
    }

    public function mail($id,$email)
    {
        
        $invoice = Invoice::find($id);
        $contents = view('Email.confermationEmail',compact('invoice'))->render();
        
        $mgClient = new Mailgun('key-01af769af3a925c6efaaa4e703f3a808');
        $domain = "thehalalbutchery.com";
        
        $admin_mail = "thehalalbutcheryuk@gmail.com";
        $liverOrder_mail = "Thbliveorders@gmail.com";
        $to = array($email,$admin_mail,$liverOrder_mail);
        
        # Make the call to the client.
        $result = $mgClient->sendMessage($domain, array(
            'from'  => 'THB@thehalalbutchery.com',
            'to'    => $to,
            'subject' => 'Order Confirmation',
            'html'  => $contents
        ));
       
        return $result;
    }

    public function confermationEmail($id)
    {
        
        $invoice = Invoice::find($id);
         
        $email = "asim.yaqub@me.com";
        
        $contents = view('Email.confermationEmail',compact('invoice'))->render();
      
        $mgClient = new Mailgun('key-01af769af3a925c6efaaa4e703f3a808');
        $domain = "thehalalbutchery.com";
        $result = $mgClient->sendMessage($domain, array(
            'from'  => 'THB@thehalalbutchery.com',
            'to'    => $email,
            'subject' => 'Order Confirmation',
            'html'  => $contents
        ));
    }

     public function dispatchedEmail($email)
    {
         
        $contents = view('Email.dispatchedEmail');
        $mgClient = new Mailgun('key-01af769af3a925c6efaaa4e703f3a808');
        $domain = "thehalalbutchery.com";
        $result = $mgClient->sendMessage($domain, array(
            'from'  => 'THB@thehalalbutchery.com',
            'to'    => $email,
            'subject' => 'Order Dispatched',
            'html'  => $contents
        ));
    }

    public function forgetPasswordLinkEmail($link,$email)
    {
        $contents = view('Email.ForgetPasswordEmail',compact('link'))->render();
      
        $mgClient = new Mailgun('key-01af769af3a925c6efaaa4e703f3a808');
        $domain = "thehalalbutchery.com";
        
        # Make the call to the client.
        $result = $mgClient->sendMessage($domain, array(
            'from'  => 'THB@thehalalbutchery.com',
            'to'    => $email,
            'subject' => 'Forget Password',
            'html'  => $contents
        ));
    }

    public function enq_mail($name,$phone,$typeOfEvent,$email)
    {
                
        $mgClient = new Mailgun('key-01af769af3a925c6efaaa4e703f3a808');
        $domain = "thehalalbutchery.com";
        
        $admin_mail = "umkhan93@gmail.com";
        $info = "info@asianspiceevents.co.uk";

        $to = array($info);
        $enq = "Hi, I am ".$name." my contact details are <br> Phone: ".$phone." <br>Email: ".$email."<br> I want to arange ".$typeOfEvent; 
        # Make the call to the client.
        $result = $mgClient->sendMessage($domain, array(
            'from'  => 'info@asianspiceevents.co.uk',
            'to'    => $to,
            'subject' => 'New Enquiry ',
            'html'  => $enq
        ));
       
        return 1;
    }

    public function forgotMail($user,$reminder)
    {
        $mgClient = new Mailgun('key-01af769af3a925c6efaaa4e703f3a808');
        $domain = "thehalalbutchery.com";

        $contents = view('Email.ForgetPasswordEmail',compact('user','reminder'))->render();
        
        # Make the call to the client.
        $result = $mgClient->sendMessage($domain, array(
            'from'  => 'THB@thehalalbutchery.com',
            'to'    => $email,
            'subject' => 'Reset Your Password',
            'html'  => $contents
        ));
    }

    

    public function orderConfirmSMS($phone)
    {
        $service_plan_id = "15b1e84793b449f9ab946da97fc38d56";
        $bearer_token = "a385386a19ae4f6dbbb13a40c2c85bfe";

        //Any phone number assigned to your API
        $send_from = "THB";
        //May be several, separate with a comma ,
        $recipient_phone_numbers = $phone; 
        $message = "Congratulations, The Halal Butchery has received your order. You will be notified when your order has been dispatched!";

        // Check recipient_phone_numbers for multiple numbers and make it an array.
        if(stristr($recipient_phone_numbers, ',')){
        $recipient_phone_numbers = explode(',', $recipient_phone_numbers);
        }else{
        $recipient_phone_numbers = [$recipient_phone_numbers];
        }

        // Set necessary fields to be JSON encoded
        $content = [
        'to' => array_values($recipient_phone_numbers),
        'from' => $send_from,
        'body' => $message
        ];

        $data = json_encode($content);

        $ch = curl_init("https://us.sms.api.sinch.com/xms/v1/{$service_plan_id}/batches");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BEARER);
        curl_setopt($ch, CURLOPT_XOAUTH2_BEARER, $bearer_token);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);

        if(curl_errno($ch)) {
            // echo 'Curl error: ' . curl_error($ch);
            return 0;
        } else {
            // echo $result;
            return 1;
        }
        curl_close($ch);

    }

    public function orderConfirmSMS_old()
    {
        $nKey = $_ENV['NEXMO_KEY'];
        $nSecret = $_ENV['NEXMO_SECRET'];

        $basic  = new \Vonage\Client\Credentials\Basic($nKey, $nSecret);
        $client = new \Vonage\Client($basic);
        
        $response = $client->sms()->send(
            new \Vonage\SMS\Message\SMS("07852614649", "THB", 'Congratulations, The Halal Butchery has received your order. You will be notified when your order has been dispatched! 🚚')
        );
        //return back();
         echo "Sent";
    }
    public function orderDispatchedSMS($phone)
    {
        $nKey = $_ENV['NEXMO_KEY'];
        $nSecret = $_ENV['NEXMO_SECRET'];
        $basic  = new \Vonage\Client\Credentials\Basic($nKey, $nSecret);
        $client = new \Vonage\Client($basic);
        
        $response = $client->sms()->send(
            new \Vonage\SMS\Message\SMS($phone, "THB", 'Congratulations, your order has been dispatched! Have your apron ready and an eye out for your THB driver who will be arriving with your goods!')
        );
        
    }

     public function newOrderSMS()
    {
         $nKey = $_ENV['NEXMO_KEY'];
        $nSecret = $_ENV['NEXMO_SECRET'];

        $phone = "447847757161";
        $basic  = new \Vonage\Client\Credentials\Basic($nKey, $nSecret);
        $client = new \Vonage\Client($basic);
        
        $response = $client->sms()->send(
            new \Vonage\SMS\Message\SMS($phone, "THB", 'New Order Received!')
        );
        
    }

    public function checkSMS()
    {
        $nKey = $_ENV['NEXMO_KEY'];
        $nSecret = $_ENV['NEXMO_SECRET'];

        $phone = "447852614649";
        $basic  = new \Vonage\Client\Credentials\Basic($nKey, $nSecret);
        $client = new \Vonage\Client($basic);
        
        $response = $client->sms()->send(
            new \Vonage\SMS\Message\SMS($phone, "THB", 'Congratulations, your order has been dispatched! Have your apron ready and an eye out for your THB driver who will be arriving with your goods!')
        );
        
    }
    

    public function invoice_update($id, $status)
    {
        if ($status == 'Dispatched') {
            $user = Invoice::with('user')->find($id);
            // $mail =
             $this->dispatchedEmail($user->user->email);
            // $sms = 
            //$this->orderDispatchedSMS($user->user->phone);
        }
        DB::table('invoices')
            ->where('id', $id)
            ->update(['status' => $status]);
            return back();
    }

    public function Checkout($id){
        $title = 'Checkout';
        if (Session::has('invoice_id')) {
            $id = Session::get('invoice_id');
        }
        $cat = Category::all();
        $invoice = Invoice::find($id);
        
        $payment_method = $invoice->payment_method;
        if($payment_method == 1)
        {
            $cost = $invoice->total_price;
       
            \Stripe\Stripe::setApiKey('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');   
        
              
        $amount = $cost;
        $amount *= 100;
        $amount = (int) $amount;
        //dd($amount);
        $payment_intent = \Stripe\PaymentIntent::create([
            'description' => 'Stripe Test Payment',
            'amount' => $amount,
            'currency' => 'GBP',
            'description' => 'Payment From Codehunger',
            'payment_method_types' => ['card'],
        ]);
        $intent = $payment_intent->client_secret;
         return view('store.checkout',compact('intent','cost', 'cat','id','title'));
        }
        else if($payment_method == 2){
            return redirect('/checkout_cod/'.$id);
        }    
    }
    
    public function Checkout_response(Request $request)
    {
        DB::table('invoices')
            ->where('id', $request->id)
            ->update(['paid' => 1]);


        $user = User::find(session('user')->id);
        // $admin_mail = "thehalalbutcheryuk@gmail.com";
        // $mail1 = $this->mail($request->id,$admin_mail);
        // $liverOrder_mail = "Thbliveorders@gmail.com";
        // $mail2 = $this->mail($request->id,$liverOrder_mail);
         $email = session('user')->email;
         $mail = $this->mail($request->id,$email); 
          $sms = $this->orderConfirmSMS($user->phone);
        // Clearing Sessions
        Session::forget('cart');
        Session::forget('price');
        Session::forget('orderCounter');
        Session::forget('questions');
        Session::forget('invoice_id');
        broadcast(new NewOrder("New Order"));
        return redirect()->route('user.history')->with('status', 'Congratulations, Your Order has been confirmed. View its status in Your Orders!');
    
    }

    public function Checkout_cod($id)
    {
        DB::table('invoices')
            ->where('id', $id)
            ->update(['paid' => 1]);
        
        $user = User::find(session('user')->id);
        // $admin_mail = "thehalalbutcheryuk@gmail.com";
        //  $mail1 = $this->mail($id,$admin_mail);
        // $liverOrder_mail = "Thbliveorders@gmail.com";
        //  $mail2 = $this->mail($id,$liverOrder_mail);
        $email = session('user')->email;
        $mail = $this->mail($id,$email);   
         $sms = $this->orderConfirmSMS($user->phone);
        // Clearing Sessions
        Session::forget('cart');
        Session::forget('price');
        Session::forget('orderCounter');
        Session::forget('questions');
        Session::forget('invoice_id');
        broadcast(new NewOrder("New Order"));
        return redirect()->route('user.history')->with('status', 'Congratulations, Your Order has been confirmed. View its status in Your Orders!');
    
    }

    public function dashboardMobile()
    {
        $model = Invoice::with('user', 'shipping')->orderBy('id', 'DESC')->take(5)->get();
        $response = ["message"=>"pass","error"=>false,'order' => $model];
        return json_encode($response);
    }

    public function test()
    {
        $service_plan_id = "15b1e84793b449f9ab946da97fc38d56";
        $bearer_token = "a385386a19ae4f6dbbb13a40c2c85bfe";
        
        //Any phone number assigned to your API
        $send_from = "THB";
        //May be several, separate with a comma ,
        $recipient_phone_numbers = "447852614649"; 
        $message = "Test message to {$recipient_phone_numbers} from {$send_from}";
        
        // Check recipient_phone_numbers for multiple numbers and make it an array.
        if(stristr($recipient_phone_numbers, ',')){
          $recipient_phone_numbers = explode(',', $recipient_phone_numbers);
        }else{
          $recipient_phone_numbers = [$recipient_phone_numbers];
        }
        
        // Set necessary fields to be JSON encoded
        $content = [
          'to' => array_values($recipient_phone_numbers),
          'from' => $send_from,
          'body' => $message
        ];
        
        $data = json_encode($content);
        
        $ch = curl_init("https://us.sms.api.sinch.com/xms/v1/{$service_plan_id}/batches");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BEARER);
        curl_setopt($ch, CURLOPT_XOAUTH2_BEARER, $bearer_token);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        $result = curl_exec($ch);
        
        if(curl_errno($ch)) {
            echo 'Curl error: ' . curl_error($ch);
        } else {
            echo $result;
        }
        curl_close($ch);
        
    }

    function test_verification()
    {
        $content = [
            "identity"=> [ "type"=> "number", "endpoint"=> "+447852614649" ], "method"=> "sms"
          ];

        //   print_r($content);
        //   return;
          
          $data = json_encode($content);
        $ch = curl_init("https://verificationapi-v1.sinch.com/verification/v1/verifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', "Authorization: Basic MWJmZjAxOGQtYzViMy00NGMxLWJiOTktYTFiODU3ODBkNjRhOmtnNDNLTnFiU0VxZXRGNGhSalRYK0E9PQ==" ));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        $result = curl_exec($ch);
        if(curl_errno($ch)) {
            echo 'Curl error: ' . curl_error($ch);
        } else {
            echo $result;
        }
        curl_close($ch);
    }
    
    function test_verification_code()
    {
        $content = [
            "sms"=> [ "code"=> "257"], "method"=> "sms"
          ];

        //   print_r($content);
        //   return;
          
        $data = json_encode($content);
        $ch = curl_init("https://verificationapi-v1.sinch.com/verification/v1/verifications/number/+447852614649");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', "Authorization: Basic 1bff018d-c5b3-44c1-bb99-a1b85780d64a:kg43KNqbSEqetF4hRjTX+A==" ));
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        
        $result = curl_exec($ch);
        if(curl_errno($ch)) {
            echo 'Curl error: ' . curl_error($ch);
        } else {
            echo $result;
        }
        curl_close($ch);
    }

}
