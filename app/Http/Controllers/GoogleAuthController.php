<?php

namespace App\Http\Controllers;

namespace App\Http\Controllers;

use App\Address;
use App\User;
use Laravel\Socialite\Facades\Socialite;

class GoogleAuthController extends Controller
{
    public function redirect()
    {
        // dd("hi");
        return Socialite::driver('google')->redirect();

    }

    public function callbackGoogle()
    {
        // try {

        $google_user = Socialite::driver('google')->user();
        // dd($google_user);

        $user1 = User::where('email', $google_user->email)->first();
        // dd($user1);
        if (!$user1) {

            $u = new User();
            $add = new Address();

            $add->area = "";
            $add->city = "";
            $add->postcode = "";

            $add->save();

            // dd("m");

            $add_id = $add->id;

            $u->full_name = $google_user->name;
            $u->email = $google_user->email;
            $u->password = "";
            $u->address_id = $add_id;
            $u->phone = "";
            $u->google_id = $google_user->id;
            $u->save();

            $user = User::find($u->id);

            session()->put('user', $user);

            // return view("store.editProfile");
            // return redirect()->route('editProfile');

            // dd('user');
            return redirect()->route('user.home');

        } else {
            $user1->google_id = $google_user->id;
            $user1->update();

            session()->put('user', $user1);
            // dd($user);
            return redirect()->route('user.home');

        }

        // $user = User::where('google_id', $google_user->id)->first();

        // if (!$user) {

        // $new_user = User::create([
        //     'full_name' => $google_user->name,
        //     'email' => $google_user->email,
        //     'google_id' => $google_user->id,
        // ]);
        // dd("gg");

        // yahan se

        // $u = new User();
        // $add = new Address();

        // $add->area = "";
        // $add->city = "";
        // $add->postcode = "";

        // $add->save();

        // // dd("m");

        // $add_id = $add->id;

        // $u->full_name = $google_user->name;
        // $u->email = $google_user->email;
        // $u->password = "";
        // $u->address_id = $add_id;
        // $u->phone = "";
        // $u->google_id = $google_user->id;
        // $u->save();

        // $user = User::find($u->id);

        // session()->put('user', $user);
        // // return view("store.editProfile");
        // // return redirect()->route('editProfile');

        // // dd('user');
        // return redirect()->route('user.home');

        // yahan tak

        // Auth::login($new_user);

        // return redirect()->intended('user.home');
        // } else {

        //     session()->put('user', $user);
        //     // dd($user);
        //     return redirect()->route('user.home');

        // Auth::login($user);

        // return redirect()->intended('user.home');

        // $request->session()->put('user', $user);
        // return redirect()->route('user.home');

        // return redirect()->intended('home');

        // }

        // } catch (\Throwable $th) {
        //     dd('Something went wrong! ' . $th->getMessage());
        // }
    }
}