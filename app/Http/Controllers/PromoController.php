<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;
use DB;
use App\Events\NewOrder;
use DataTables;
use App\Category;
use Mailgun\Mailgun;
use Session;
use App\Promo;
use App\PromoType;

class PromoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $promos = Promo::all();
          $types = PromoType::all();
          return view('admin_panel.Promo.index',compact('promos','types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $promo=new Promo();
        $promo->type_id = $request->type;
        $promo->title = $request->title;
        $promo->value = $request->value;
        $promo->min_price = $request->min_price;
        $promo->limit = $request->limit;
        $promo->description = $request->description;
        $promo->start_date = $request->start_date;
        $promo->end_date = $request->end_date;
        $promo->status = 1;
        
        $promo->save();

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $invoice = Invoice::find($id);
        if(Session::has('user')){
        
        if (session('user')->id == $invoice->user->id) {
            return view('Email.confermationEmail',compact('invoice'));
        }
        else{
            return redirect()->route('user.history');
            }
        }
        else{
            return redirect()->route('user.home');
        }
        
    }

     public function check(Request $request)
    {
        $promo = Promo::where('title',$request->promo)
        ->where('status', 1)
        ->first();

        return json_encode($promo);
        
        
    }

    public function print($id)
    {
      
        $invoice = Invoice::find($id);
     
        return view('admin_panel.orders.print_invoice',compact('invoice'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // Cron Job for Promo Generate
    public function AutoStorePromo()
    {
        $numbers = $this->UniqueRandomNumbersWithinRange($min=0, $max=1000, $quantity=50);
        
        foreach ($numbers as $promo_number)
        {
            $promo=new Promo();
            $promo->type_id = 5;
            $promo->title = "HFFMCR".$promo_number;
            $promo->value = 50.00;
            $promo->min_price = 56.99;
            $promo->limit = 1;
            $promo->description = "Halal food festival promo.The order should be deducing the full £56.99. However they must select 3hr delivery and have a cart of over £50. ";
            $promo->start_date = "2022-08-12";
            $promo->end_date = "2022-08-31";
            $promo->status = 1;
            $promo->save();
            echo $promo->title;
            echo "<br>";
        }
        return back();
    }

    function UniqueRandomNumbersWithinRange($min, $max, $quantity) {
        $numbers = range($min, $max);
        shuffle($numbers);
        return array_slice($numbers, 0, $quantity);
    }

    function simplePrint(){

        $p = Promo::where('id', '>', 89)->get();
       
        foreach($p as $pt)
        {
            echo $pt->title;
            echo "<br>";
        }
        die();
    }
    public function dashboard_ajax($page)
    {
        $model = Invoice::with('user', 'shipping')->orderBy('id', 'DESC');
        if ($page == "Dashboard") {
            $model = Invoice::with('user', 'shipping')->orderBy('id', 'DESC');
        }
        else if ($page == "New") {
            $model = Invoice::with('user', 'shipping')->where('status',"Placed")->orderBy('id', 'DESC');
        }
         else if ($page == "Processing") {
            $model = Invoice::with('user', 'shipping')->where('status',"Processing")->orderBy('id', 'DESC');
        }
         else if ($page == "Dispatched") {
            $model = Invoice::with('user', 'shipping')->where('status',"Dispatched")->orderBy('id', 'DESC');
        }
        else if ($page == "Deliverd") {
            $model = Invoice::with('user', 'shipping')->where('status',"Deliverd")->orderBy('id', 'DESC');
        }
        //$model = Invoice::with('user', 'shipping')->orderBy('id', 'DESC');
                return DataTables::eloquent($model)
                ->addColumn('name', function (Invoice $invoice) {
                    return $invoice->user->full_name;
                })
                ->addColumn('shipping', function (Invoice $invoice) {
                    return $invoice->shipping->type;
                })
               ->addColumn('id', '<a href="invoice/{{$model->id}}">{{$model->id}}</a>')
              
                ->addColumn('action', function (Invoice $invoice) {
                    if ($invoice->status == "Placed") {
                        return '<a href="https://dev.thehalalbutchery.com/invoice_update/'.$invoice->id.'/Processing'.'" class="btn" style="background:white; border-radius:50px;width:40px" ><i class="menu-icon mdi mdi-thumb-up" alt="Accept"></i></a>'.'<a href="https://dev.thehalalbutchery.com/invoice_print/'.$invoice->id.'" class="btn btn-warning" style="border-radius:50px;width:40px;margin-left: 5px;" ><i class="menu-icon mdi mdi-printer" alt="Accept"></i></a>'.'<a href="https://dev.thehalalbutchery.com/invoice/'.$invoice->id.'" class="btn btn-info" style="border-radius:50px;width:40px;margin-left: 5px;" ><i class="menu-icon mdi mdi-eye" alt="Accept"></i></a>';
                    }
                    else if ($invoice->status == "Processing") {
                        return '<a href="https://dev.thehalalbutchery.com/invoice_update/'.$invoice->id.'/Dispatched'.'" class="btn btn-primary" style=" border-radius:50px;width:40px"><i class="menu-icon mdi mdi-truck"></i></a>'.'<a href="https://dev.thehalalbutchery.com/invoice_print/'.$invoice->id.'" class="btn btn-warning" style="border-radius:50px;width:40px;margin-left: 5px;" ><i class="menu-icon mdi mdi-printer" alt="Accept"></i></a>'.'<a href="https://dev.thehalalbutchery.com/invoice/'.$invoice->id.'" class="btn btn-info" style="border-radius:50px;width:40px;margin-left: 5px;" ><i class="menu-icon mdi mdi-eye" alt="Accept"></i></a>';
                    }
                     else if($invoice->status == "Dispatched") {
                        return '<a href="https://dev.thehalalbutchery.com/invoice_update/'.$invoice->id.'/Deliverd'.'" class="btn btn-success" style=" border-radius:50px;width:40px"><i class="menu-icon mdi mdi-shopping"></i></a>'.'<a href="https://dev.thehalalbutchery.com/invoice_print/'.$invoice->id.'" class="btn btn-warning" style="border-radius:50px;width:40px;margin-left: 5px;" ><i class="menu-icon mdi mdi-printer" alt="Accept"></i></a>'.'<a href="https://dev.thehalalbutchery.com/invoice/'.$invoice->id.'" class="btn btn-info" style="border-radius:50px;width:40px;margin-left: 5px;" ><i class="menu-icon mdi mdi-eye" alt="Accept"></i></a>';
                    }
                     else {
                        return '<a href="https://dev.thehalalbutchery.com/invoice_print/'.$invoice->id.'" class="btn btn-warning" style="border-radius:50px;width:40px;margin-left: 5px;" ><i class="menu-icon mdi mdi-printer" alt="Accept"></i></a>'.'<a href="https://dev.thehalalbutchery.com/invoice/'.$invoice->id.'" class="btn btn-info" style="border-radius:50px;width:40px;margin-left: 5px;" ><i class="menu-icon mdi mdi-eye" alt="Accept"></i></a>';
                    }
                    
                })
                ->addColumn('paid', function (Invoice $invoice) {
                    return $invoice->paid;
                })
                ->rawColumns(['action','id'])
                ->toJson();
        //return datatables()->of(Invoice::latest()->get())->toJson();
    }

    public function mail($id,$email)
    {
        
        $invoice = Invoice::find($id);
       
        $contents = "Hi Test mail";//view('Email.confermationEmail',compact('invoice'))->render();
      
        $mgClient = new Mailgun('key-a93b567233af0db66430579590d246dc');
        $domain = "dev.thehalalbutchery.com";
        //dd($mgClient);
        # Make the call to the client.
        $result = $mgClient->sendMessage($domain, array(
            'from'  => 'THB@dev.thehalalbutchery.com',
            'to'    => $email,
            'subject' => 'Order Confermation',
            'html'  => $contents
        ));
        //dd($result);
        return $result;
    }

    public function confermationEmail($id)
    {
        
        $invoice = Invoice::find($id);
         //return view('Email.confermationEmail',compact('invoice'));
        $email = "saifahmed.sa95@gmail.com";
        $contents = view('Email.confermationEmail',compact('invoice'))->render();
      
        $mgClient = new Mailgun('key-a93b567233af0db66430579590d246dc');
        $domain = "dev.thehalalbutchery.com";
        //dd($mgClient);
        # Make the call to the client.
        $result = $mgClient->sendMessage($domain, array(
            'from'  => 'THB@dev.thehalalbutchery.com',
            'to'    => $email,
            'subject' => 'Order Confermation',
            'html'  => $contents
        ));
        //dd($result);
       // return $result;
    }

    public function forgotMail($user,$reminder)
    {
        dd($user);
        $mgClient = new Mailgun('key-a93b567233af0db66430579590d246dc');
        $domain = "dev.thehalalbutchery.com";

        $contents = view('Email.ForgetPasswordEmail',compact('user','reminder'))->render();
        //dd($mgClient);
        # Make the call to the client.
        $result = $mgClient->sendMessage($domain, array(
            'from'  => 'THB@dev.thehalalbutchery.com',
            'to'    => $email,
            'subject' => 'Reset Your Password',
            'html'  => $contents
        ));
    }
    public function orderConfirmSMS()
    {
        $basic  = new \Vonage\Client\Credentials\Basic("6e9ce445", "NZC1L6pmOEFnnx8p");
        $client = new \Vonage\Client($basic);
        
        $response = $client->sms()->send(
            new \Vonage\SMS\Message\SMS("07852614649", "THB", 'Congragulations! Your Order Received Successfully to The Halal Butchery. We will let you know when your order will dispatched and around you about 30 Mins. Thank You!')
        );
        return back();
    }
    public function orderDispatchedSMS()
    {
        $basic  = new \Vonage\Client\Credentials\Basic("6e9ce445", "NZC1L6pmOEFnnx8p");
        $client = new \Vonage\Client($basic);
        
        $response = $client->sms()->send(
            new \Vonage\SMS\Message\SMS("07852614649", "THB", 'Congragulations! Your Order Received Successfully to The Halal Butchery. We will let you know when your order will dispatched and around you about 30 Mins. Thank You!')
        );
        return back();
    }
    

    public function invoice_update($id, $status)
    {
       // dd($status);
        DB::table('invoices')
            ->where('id', $id)
            ->update(['status' => $status]);
            return back();
    }
    public function Checkout($id){
        //Session::put('invoice_id',$id);
        if (Session::has('invoice_id')) {
            $id = Session::get('invoice_id');
        }
        $cat = Category::all();
        $invoice = Invoice::find($id);
     
        $cost = $invoice->total_price;
      
            \Stripe\Stripe::setApiKey('sk_test_51IyFCSGrx18qe3A80lXgf7BbrdhW9q3TzC4CtQOnF2bzdEEy54iJTBijLz1qHll3KqbyIrM9FkOcTSz6Fv1U65GQ00Q02XS3hJ');   
       
              
        $amount = $cost;
        $amount *= 100;
        $amount = (int) $amount;
        //dd($amount);
        $payment_intent = \Stripe\PaymentIntent::create([
            'description' => 'Stripe Test Payment',
            'amount' => $amount,
            'currency' => 'GBP',
            'description' => 'Payment From Codehunger',
            'payment_method_types' => ['card'],
        ]);
        $intent = $payment_intent->client_secret;
         return view('store.checkout',compact('intent','cost', 'cat','id'));

    
    }
    public function Checkout_response(Request $request)
    {
        $email = session('user')->email;
        $mail = $this->mail($invoice->id,$email);   
        $admin_mail = "thehalalbutcheryuk@gmail.com";
        $mail1 = $this->mail($invoice->id,$admin_mail);

        DB::table('invoices')
            ->where('id', $request->id)
            ->update(['paid' => 1]);
        Session::forget('invoice_id');
        return redirect()->route('user.history')->with('status', 'Congratulations, Your Order has been confirmed. View its status in Your Orders!');
    
    }

    public function dashboardMobile()
    {
        $model = Invoice::with('user', 'shipping')->orderBy('id', 'DESC')->take(5)->get();
        $response = ["message"=>"pass","error"=>false,'order' => $model];
        return json_encode($response);
    }

      public function testEmail()
    {
        $mail1 = "umkhan93@gmail.com";
        
        
        
        $mail1 = $this->mail(103,$mail1);

        echo $mail;
    }



}
