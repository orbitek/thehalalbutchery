<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Category;
use App\Http\Requests\AdminLoginVerifyRequest;
use App\Product;
use App\User;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;

class loginController extends Controller
{
    public function adminIndex()
    {
        return view('admin_panel.adminLogin');
    }
    public function adminLogout()
    {
        session()->flush();
        return redirect()->route('admin.login');
    }
    public function adminPosted(AdminLoginVerifyRequest $request)
    {
        $admin = Admin::where('username', $request->Username)->first();

        if ($admin == null) {

            $request->session()->flash('message', 'Invalid Username');

            return redirect(route('admin.login'));
        } else {
            if ($request->Password == $admin->password) {
                session()->put('admin', $admin);
                return redirect()->route('admin.dashboard');
            } else if ($request->Password != $admin->password) {
                $request->session()->flash('message', 'Invalid Password');
                return view('admin_panel.adminLogin');
            }
        }

    }

    // login view only
    public function userIndex()
    {
        // dd("hi");

        if (session()->has('user')) {
            return redirect()->route("user.cart");
        }

        $res = Product::all();
        $cat = Category::all();
        $title = 'Login';
        return view('store.login')
            ->with('title', $title)
            ->with('products', $res)
            ->with("cat", $cat);

    }

    public function userPosted(Request $request)
    {
        // dd("hello");
        // dd($request->pass);

        // Session::has('cart');
        // dd('cart');

        $user = User::where('email', $request->email)
            ->where('password', $request->pass)
            ->first();

        if ($user == null) {
            // dd("if");
            $request->session()->flash('message', 'Invalid User');

            return redirect()->route('user.login');
        } else {
            // dd("else");
            $request->session()->put('user', $user);
            return redirect()->back();
            // return redirect()->route('user.home');
        }
    }
    public function userLogout(Request $r)
    {
        $r->session()->flush();
        return redirect()->route('user.home');
    }

}