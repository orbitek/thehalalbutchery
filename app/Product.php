<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'image_name',
        'description',
        'colors',
        'price',
        'discount',
        'tag',
        'active',
        'category_id'
    ];
    public function question()
    {
        return $this->belongsToMany('App\Question');
    }
    public function variations()
    {
        return $this->belongsToMany('App\Variation');
    }

    public function category()
    {
    	return $this->belongsTo('App\Category','category_id','id');
    }
    
    
}
